package org.dba.test;

public class PID {
	
	public native int getCmdPID();
	
	public native String getPIDName(String imgName);
	
	static {
		System.loadLibrary("libPlugins-C");
	}
	
	public static void main(String[] args){
		System.out.println(new PID().getCmdPID());
		System.out.println(new PID().getPIDName("H:MSG"));
	}

}
