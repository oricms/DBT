package org.dba.util;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class SwingUtils {

	private SwingUtils() {
	}

	/**
	 * 获取保存动作时文件选择器的文件路径, 返回用户所肯定的路径, 否则返回null
	 */
	public static File selectSaveFile() {
		return selectSaveFile(null, null, null);
	}

	public static File selectSaveFile(File file) {
		return selectSaveFile(file, null, null);
	}

	public static File selectSaveFile(File file, Component parent) {
		return selectSaveFile(file, null, parent);
	}

	public static File selectSaveFile(File file, FileFilter[] filters, Component parent) {
		File returnFile = null;
		JFileChooser jfc = file != null ? (new JFileChooser(file)) : (new JFileChooser());
		if (filters != null)
			for (int i = 0; i < filters.length; i++)
				jfc.addChoosableFileFilter(filters[i]);
		jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		int result = jfc.showSaveDialog(parent);
		if (result == JFileChooser.APPROVE_OPTION) {
			file = jfc.getSelectedFile();
			String fileName = file.getName();
			String fileExtName = fileName.indexOf(".") > 0 ? fileName.substring(fileName.lastIndexOf(".") + 1) : "";
			if (jfc.getFileFilter() instanceof FileFilter) {
				String filterExtName = ((FileFilter) jfc.getFileFilter()).getExtName();
				if (!fileExtName.equalsIgnoreCase(filterExtName))
					file = new File(file.getPath()+"/"+Util.getInstance().getNoDateTime(System.currentTimeMillis())+ "." + filterExtName);
			}
			if (file.exists()) {
				result = 0;
				if (result == 0) {
					returnFile = file;
				} else if (result == 1) {
					returnFile = selectSaveFile(file, filters, parent);
				} else {
					returnFile = null;
				}
			} else {
				returnFile = file;
			}
		}
		return returnFile;
	}

	/**
	 * 获取选择文件动作时文件选择器的文件路径, 返回用户所肯定的路径, 否则返回null
	 */
	public static File selectOpenFile() {
		return selectOpenFile(null, null, null);
	}

	public static File selectOpenFile(File file) {
		return selectOpenFile(file, null, null);
	}

	public static File selectOpenFile(File file, Component parent) {
		return selectOpenFile(file, null, parent);
	}

	public static File selectOpenFile(File file, FileFilter[] filters, Component parent) {
		File returnFile = null;
		JFileChooser jfc = file != null ? (new JFileChooser(file)) : (new JFileChooser());
		if (filters != null)
			for (int i = 0; i < filters.length; i++)
				jfc.addChoosableFileFilter(filters[i]);
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int result = jfc.showOpenDialog(parent);
		if (result == JFileChooser.APPROVE_OPTION) {
			file = jfc.getSelectedFile();
			String fileName = file.getName();
			String fileExtName = fileName.indexOf(".") > 0 ? fileName.substring(fileName.lastIndexOf(".") + 1) : "";
			if (jfc.getFileFilter() instanceof FileFilter) {
				String filterExtName = ((FileFilter) jfc.getFileFilter()).getExtName();
				if (!fileExtName.equalsIgnoreCase(filterExtName))
					file = new File(file.getPath() + "." + filterExtName);
			}
			if (!file.exists()) {
				result = JOptionPane.showConfirmDialog(parent, "文件不存在! 确定重新选择吗?", "打开文件", 2);
				if (result == 0) {
					returnFile = selectOpenFile(file.getParentFile(), filters, parent);
				} else {
					returnFile = null;
				}
			} else {
				returnFile = file;
			}
		}
		return returnFile;
	}

	public static File[] selectOpenFiles() {
		return selectOpenFiles(null, null, null);
	}

	public static File[] selectOpenFiles(File file) {
		return selectOpenFiles(file, null, null);
	}

	public static File[] selectOpenFiles(File file, Component parent) {
		return selectOpenFiles(file, null, parent);
	}

	public static File[] selectOpenFiles(File file, FileFilter[] filters, Component parent) {
		File[] returnFiles = null;
		JFileChooser jfc = file != null ? (new JFileChooser(file)) : (new JFileChooser());
		if (filters != null)
			for (int i = 0; i < filters.length; i++)
				jfc.addChoosableFileFilter(filters[i]);
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfc.setMultiSelectionEnabled(true);
		int result = jfc.showOpenDialog(parent);
		if (result == JFileChooser.APPROVE_OPTION) {
			File[] files = jfc.getSelectedFiles();
			boolean ba = true;
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				String fileExtName = fileName.indexOf(".") > 0 ? fileName.substring(fileName.lastIndexOf(".") + 1) : "";
				if (jfc.getFileFilter() instanceof FileFilter) {
					String filterExtName = ((FileFilter) jfc.getFileFilter()).getExtName();
					if (!fileExtName.equalsIgnoreCase(filterExtName))
						files[i] = new File(files[i].getPath() + "." + filterExtName);
				}
				if (!files[i].exists()) {
					ba = false;
					result = JOptionPane.showConfirmDialog(parent, "找不到文件:" + files[i] + "! 确定重新选择吗?", "打开文件", 2);
					if (result == 0) {
						returnFiles = selectOpenFiles(file, filters, parent);
						break;
					} else {
						returnFiles = null;
						break;
					}
				}
			}
			if (ba)
				returnFiles = files;
		}
		return returnFiles;
	}

}
