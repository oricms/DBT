package org.dba.ui;


import java.awt.Color;
import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.IconNode;

public class IconNodeRenderer extends DefaultCheckboxTreeCellRenderer {

	private static final long serialVersionUID = 3070028294132981169L;
	Log log = LogFactory.getLog(getClass());

	String value = "";

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object object, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, object, selected, expanded, leaf, row, hasFocus);
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) object;
		Object userObject = node.getUserObject();
		if (userObject instanceof IconNode) {
			IconNode jtd = (IconNode) userObject;
			setIcon(jtd.getIcon());
			int _row = jtd.getRow();
			value = jtd.getText().toString();
			if (_row > 0) {
				setFColor(Color.RED, value + " [" + _row + "]");
			} else {
				setFColor(Color.BLACK, value);
			}
		}
		return this;
	}
}
