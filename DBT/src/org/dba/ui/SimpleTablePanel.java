package org.dba.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.DynamicBean;
import org.dba.beanunit.IconNode;
import org.dba.dbunit.wf_Db_Connect;
import org.dba.dbunit.wf_Db_Exception;
import org.dba.dbunit.wf_Db_Server;
import org.dba.util.FileFilter;
import org.dba.util.SwingUtils;
import org.dba.util.Util;
import org.dba.util.FrameManager;
import org.dba.util.ProjectContext;

import javax.swing.border.TitledBorder;

import java.awt.Color;

import javax.swing.UIManager;
import javax.swing.JTextField;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.Component;

public class SimpleTablePanel extends JPanel implements ActionListener {

	private JLabel proinfo;
	private JLabel tableSize;
	private String[] options;
	private DynamicBean db = null;

	private static final long serialVersionUID = 6162899017966256858L;
	Log log = LogFactory.getLog(getClass());
	private JTextField s_TF_file;

	public SimpleTablePanel() {
		setLayout(null);

		final CheckboxTree jtree = new CheckboxTree();

		final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(new IconNode(new ImageIcon("image/database_type.png"), "ROOT", 0));
		final DefaultTreeModel rootModel = new DefaultTreeModel(rootNode);
		jtree.setModel(rootModel);
		jtree.setCellRenderer(new IconNodeRenderer());
		jtree.setRootVisible(true);
		jtree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		jtree.setRowHeight(20);
		jtree.setAlignmentY(CENTER_ALIGNMENT);
		jtree.setAlignmentX(Component.LEFT_ALIGNMENT);
		jtree.setVisibleRowCount(20);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 450, 48);
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u8FDE\u63A5\u5C5E\u6027", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("\u8FDE\u63A5\u7684\u670D\u52A1\u5668\uFF1A");
		label.setBounds(10, 21, 96, 15);
		panel.add(label);

		JButton newCon = new JButton("\u65B0\u5EFA");
		newCon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConnectionDialog cd = new ConnectionDialog(FrameManager.sf);
				cd.setVisible(true);
				UIManager.put("ConnectionDialog", cd);
			}
		});
		newCon.setVerticalAlignment(SwingConstants.TOP);
		newCon.setIcon(null);
		newCon.setHorizontalAlignment(SwingConstants.TRAILING);
		newCon.setBounds(255, 17, 57, 23);
		panel.add(newCon);

		JComboBox connectComBox = new JComboBox();
		connectComBox.setModel(new DefaultComboBoxModel(Util.getInstance().readConfig()));
		// String value =
		// Util.getInstance().getSelectConnectItem(String.valueOf(connectComBox.getSelectedItem()));
		// if (value != null) {
		// String[] _s = value.split("@");
		// // 192.168.3.80@1521@BZPM@BZADMIN@BZADMIN@BZADMIN
		// if (_s.length == 6) {
		// db = new DynamicBean(new String[] { "ip:String=" + _s[0],
		// "port:String=" + _s[1], "sid:String=" + _s[2], "user:String=" +
		// _s[3], "msm:String=" + _s[4],
		// "pwd:String=" + _s[5] });
		// final DefaultMutableTreeNode rootNode = new
		// DefaultMutableTreeNode(new IconNode(new
		// ImageIcon("image/database_type.png"), _s[0], 0));
		// final DefaultTreeModel rootModel = new DefaultTreeModel(rootNode);
		// jtree.setModel(rootModel);
		// jtree.setCellRenderer(new IconNodeRenderer());
		// jtree.setRootVisible(true);
		// jtree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		// jtree.setRowHeight(20);
		// jtree.setAlignmentY(CENTER_ALIGNMENT);
		// jtree.setAlignmentX(CENTER_ALIGNMENT);
		// jtree.setVisibleRowCount(20);
		// }
		// }

		connectComBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object key = e.getItem();
					String value = Util.getInstance().getSelectConnectItem(String.valueOf(key));
					if (value != null) {
						String[] _s = value.split("@");
						//192.168.3.80@1521@BZPM@BZADMIN@BZADMIN@BZADMIN
						if (_s.length == 6) {
							db = new DynamicBean(new String[]{"ip:String=" + _s[0], "port:String=" + _s[1], "sid:String=" + _s[2], "user:String=" + _s[3], "msm:String=" + _s[4],
									"pwd:String=" + _s[5]});
							new Thread(new Runnable(){
								public void run() {
									initTree(jtree, db);
								}
							}).start();
						}
					}
				}
			}
			
		});

		UIManager.put("connectComBox", connectComBox);
		connectComBox.setBounds(96, 18, 157, 21);
		panel.add(connectComBox);

		JButton deleCon = new JButton("\u6E05\u9664");
		deleCon.setBounds(383, 17, 57, 23);
		panel.add(deleCon);

		JButton execCon = new JButton("\u67E5\u8BE2");
		execCon.setBounds(316, 17, 57, 23);
		panel.add(execCon);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 45, 450, 48);
		panel_1.setPreferredSize(new Dimension(134, 50));
		panel_1.setBorder(new TitledBorder(null, "选项", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel_1);
		panel_1.setLayout(null);

		final JCheckBox rows = new JCheckBox("\u6570\u636E\u884C");
		rows.setBounds(6, 16, 62, 25);
		panel_1.add(rows);
		rows.setPreferredSize(new Dimension(70, 25));

		final JCheckBox indexs = new JCheckBox("\u7D22\u5F15");
		indexs.setBounds(70, 16, 50, 25);
		panel_1.add(indexs);
		indexs.setPreferredSize(new Dimension(50, 25));

		final JCheckBox compress = new JCheckBox("\u538B\u7F29");
		compress.setBounds(123, 16, 50, 25);
		panel_1.add(compress);
		compress.setPreferredSize(new Dimension(50, 25));

		final JCheckBox whdelrow = new JCheckBox("\u5F7B\u5E95\u5220\u9664\u6570\u636E");
		whdelrow.setBounds(175, 16, 100, 25);
		panel_1.add(whdelrow);
		whdelrow.setToolTipText("\u52FE\u9009\u6B64\u9879,\u5C06\u65E0\u6CD5\u4ECE\u65E5\u5FD7\u4E2D\u6062\u590D\u6570\u636E!");
		whdelrow.setPreferredSize(new Dimension(100, 25));
		whdelrow.setForeground(Color.RED);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(0, 270, 450, 48);
		panel_2.setLayout(null);
		panel_2.setPreferredSize(new Dimension(550, 50));
		panel_2.setBorder(new TitledBorder(null, "工具栏", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel_2);

		JButton button = new JButton("导出");
		button.setBounds(10, 21, 57, 22);
		panel_2.add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				options = new String[3];
				options[0] = String.valueOf(compress.isSelected());
				options[1] = String.valueOf(indexs.isSelected());
				options[2] = String.valueOf(rows.isSelected());

				// 组装表导出命令
				/*
				 * EXP MBZADMIN/MBZADMIN@192.168.3.200/BZPM ROWS=Y INDEXES=N
				 * COMPRESS=N FILE=D:\PERSON_ROLE.DMP TABLES=PERSON_ROLE,
				 */
				final String _file = s_TF_file.getText().toString();
				proinfo.setText("...");
				new Thread() {
					String expcmd = "";
					public void run() {
						try {
							if ("请选择文件".equals(_file)) {
								proinfo.setText("请选择文件路径!");
							} else {
								int r = JOptionPane.showConfirmDialog(SimpleTablePanel.this, "你确定要导出当前选中的数据表么?", "导出表", JOptionPane.YES_NO_OPTION);
								if (r == 0) {
									if (db != null){
										expcmd = ProjectContext.getWinExpPath() + "EXP.EXE " + db.getValue("user") + "/" + db.getValue("pwd") + "@" + db.getValue("ip") + "/" + db.getValue("sid")
												+ " FILE=" + _file + " BUFFER=65536 FEEDBACK=100000";
									}else {
										proinfo.setText("请选择数据库连接");
										return;
									}
									if (options.length > 0) {
										if (Boolean.valueOf(options[0])){
											expcmd = expcmd + " COMPRESS=Y";
										}
										if (Boolean.valueOf(options[1])){
											expcmd = expcmd + " INDEXES=Y";
										}
										if (Boolean.valueOf(options[2])){
											expcmd = expcmd + " ROWS=Y";
										}
									}
									String osName = System.getProperty("os.name");
									String[] cmd = new String[4];
									if (osName.equals("Windows NT")) {
										cmd[0] = "cmd.exe";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									} else if (osName.equals("Windows XP")) {
										cmd[0] = "cmd.exe";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									} else if (osName.equals("Windows 95")) {
										cmd[0] = "command.com";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									} else if (osName.equals("Windows 7")) {
										cmd[0] = "cmd.exe";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									}
									HashSet<String> selectTables = Util.getInstance().getSelectTables();
									String _t = "";
									if (selectTables.size() > 0) {
										for (String tName : selectTables) {
											_t = _t + tName + ",";
										}
									}
									log.info("Length:" + _t.length());
									if (_t.length() > 0) {
										cmd[3] = " TABLES=" + _t;
									} else {
										return;
									}
									Runtime rt = Runtime.getRuntime();
									log.info("Execing " + cmd[0] + cmd[1] + cmd[2] + cmd[3]);
									Process proc = rt.exec(cmd);
									InputStreamReader isr = new InputStreamReader(proc.getErrorStream());
									BufferedReader br = new BufferedReader(isr);
									String line = null;
									while ((line = br.readLine()) != null) {
										if (!line.equals("") || line != null) {
											if (line.contains("ORA-") || line.contains("IMP-") || line.contains("EXP-")) {
												proinfo.setText(line + "\n");
												proinfo.setText("导出异常 ,程序中止...\n");
												break;
											} else {
												proinfo.setText(line + "\n");
											}
										}
									}
								}
							}
						} catch (IOException e) {
							proinfo.setText(e.getMessage());
						}
					}

				}.start();
			}
		});

		JButton imbutton = new JButton("\u5BFC\u5165");
		imbutton.setBounds(70, 21, 57, 22);
		panel_2.add(imbutton);
		imbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				options = new String[3];
				options[0] = String.valueOf(compress.isSelected());
				options[1] = String.valueOf(indexs.isSelected());
				options[2] = String.valueOf(rows.isSelected());

				// 组装表导出命令
				/*
				 * EXP MBZADMIN/MBZADMIN@192.168.3.200/BZPM ROWS=Y INDEXES=N
				 * COMPRESS=N FILE=D:\PERSON_ROLE.DMP TABLES=PERSON_ROLE,
				 */
				final String _file = s_TF_file.getText().toString();
				proinfo.setText("...");

				new Thread() {
					public void run() {
						try {
							if ("请选择文件".equals(_file)) {
								proinfo.setText("请选择文件路径!");
							} else {
								int r = JOptionPane.showConfirmDialog(SimpleTablePanel.this, "你确定要导入当前选中的数据表么?请注意你导出表时所勾选的选项,以免发生错误!", "导入表", JOptionPane.YES_NO_OPTION);
								if (r == 0) {
									String expcmd = ProjectContext.getWinExpPath() + "IMP.EXE " + db.getValue("user") + "/" + db.getValue("pwd") + "@" + db.getValue("ip") + "/" + db.getValue("sid")
											+ " FILE=" + _file + " FROMUSER=" + db.getValue("user") + " TOUSER=" + db.getValue("user") + " BUFFER=65536 FEEDBACK=100000";
									if (options.length > 0) {
										if (Boolean.valueOf(options[0])) {
											expcmd = expcmd + " COMPRESS=Y";
										}
										if (Boolean.valueOf(options[1])) {
											expcmd = expcmd + " INDEXES=Y";
										}
										if (Boolean.valueOf(options[2])) {
											expcmd = expcmd + " ROWS=Y";
										}
									}

									String[] cmd = Util.getInstance().getCmd(4, expcmd);
									HashSet<String> selectTables = Util.getInstance().getSelectTables();
									String _t = "";
									if (selectTables.size() > 0) {
										for (String tName : selectTables) {
											_t = _t + tName + ",";
										}
										cmd[3] = " TABLES=" + _t;

										wf_Db_Connect db_connect = null;
										try {
											db_connect = wf_Db_Server.getInstance().getFactory("dbconfig").get();
											for (String tName : selectTables) {
												Util.getInstance().dropTable(db_connect, tName, proinfo);
											}
											// DbUtil.getInstance().clearTables();
											// initTree(jtree);
											removeNode(jtree);
										} catch (Exception e1) {
											proinfo.setText(e1.getMessage());
										} finally {
											db_connect.close();
										}
										// 删除表
									} else {
										proinfo.setText("没有检测到要导入的表!");
										return;
									}

									// 检查当前的表是否

									Runtime rt = Runtime.getRuntime();
									log.info("Execing " + cmd[0] + cmd[1] + cmd[2] + cmd[3]);
									Process proc = rt.exec(cmd);
									InputStreamReader isr = new InputStreamReader(proc.getErrorStream());
									BufferedReader br = new BufferedReader(isr);
									String line = null;
									while ((line = br.readLine()) != null) {
										if (!line.equals("") || line != null) {

											if (line.contains("ORA-") || line.contains("IMP-")) {
												proinfo.setText(line);
												proinfo.setText("程序中止..." + line);
												break;
											} else {
												proinfo.setText(line);
											}

											proinfo.setText(line);
										}
									}
									Util.getInstance().clearTables();
									initTree(jtree, db);
								}
							}
						} catch (IOException e) {
							e.printStackTrace();
							proinfo.setText(e.getMessage());
						}
					}

				}.start();
			}
		});

		JButton button_3 = new JButton("\u5237\u65B0");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						Util.getInstance().clearTables();
						if (db != null) {
							initTree(jtree, db);
						} else {
							proinfo.setText("请选择数据库连接");
							return;
						}
					}
				}).start();
			}
		});
		button_3.setBounds(130, 21, 57, 22);
		panel_2.add(button_3);

		JButton button_4 = new JButton("\u6E05\u7A7A\u8868");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 清空表
				int r = JOptionPane.showConfirmDialog(SimpleTablePanel.this, "你确定要清空当前选中的数据表中的数据么?", "清空表", JOptionPane.YES_NO_OPTION);
				if (r == 0) {
					final HashSet<String> selectTables = Util.getInstance().getSelectTables();
					if (selectTables.size() > 0) {
						try {
							new Thread(new Runnable() {
								wf_Db_Connect db_connect = null;

								public void run() {
									try {
										if (db != null) {
											db_connect = wf_Db_Server.getInstance().getFactory(db).get();
											for (String tName : selectTables) {
												if (whdelrow.isSelected()) {
													Util.getInstance().truncateTable(db_connect, tName, proinfo);
													log.info("启用truncate删除模式!");
												} else {
													Util.getInstance().deleteTable(db_connect, tName, proinfo);
												}
											}
											initTree(jtree, db);
											Util.getInstance().clearTables();
										} else {
											proinfo.setText("请选择数据库连接");
										}
									} catch (Exception e1) {
										proinfo.setText(e1.getMessage());
									} finally {
										db_connect.close();
									}

								}
							}).start();
						} catch (Exception e1) {
							proinfo.setText(e1.getMessage());
						}
					}
				}

			}
		});
		button_4.setBounds(190, 21, 69, 22);
		panel_2.add(button_4);

		JButton button_5 = new JButton("\u5220\u9664\u8868");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int r = JOptionPane.showConfirmDialog(SimpleTablePanel.this, "你确定要删除当前选中的数据表么?", "删除表", JOptionPane.YES_NO_OPTION);
				if (r == 0) {
					final HashSet<String> selectTables = Util.getInstance().getSelectTables();
					if (selectTables.size() > 0) {
						new Thread(new Runnable() {
							public void run() {
								wf_Db_Connect db_connect = null;
								if (db != null) {
									try {
										db_connect = wf_Db_Server.getInstance().getFactory(db).get();
										for (String tName : selectTables) {
											Util.getInstance().dropTable(db_connect, tName, proinfo);
										}
										Util.getInstance().clearTables();
										initTree(jtree, db);
									} catch (Exception e1) {
										proinfo.setText(e1.getMessage());
									} finally {
										db_connect.close();
									}
								} else {
									proinfo.setText("请选择数据库连接");
								}
							}
						}).start();
					}
				}

			}
		});
		button_5.setBounds(265, 21, 69, 22);
		panel_2.add(button_5);

		JButton impto = new JButton("\u5BFC\u5165\u5230");
		impto.setVerticalAlignment(SwingConstants.BOTTOM);
		impto.setBounds(336, 21, 69, 22);
		panel_2.add(impto);

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(0, 315, 450, 45);
		panel_3.setLayout(null);
		panel_3.setPreferredSize(new Dimension(545, 45));
		panel_3.setBorder(new TitledBorder(null, "文件栏", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel_3);

		JLabel label_1 = new JLabel("\u6587\u4EF6\u8DEF\u5F84\uFF1A");
		label_1.setBounds(10, 19, 60, 15);
		panel_3.add(label_1);

		s_TF_file = new JTextField("\u8BF7\u9009\u62E9\u6587\u4EF6");
		s_TF_file.setPreferredSize(new Dimension(300, 20));
		s_TF_file.setFont(new Font("宋体", Font.PLAIN, 12));
		s_TF_file.setBounds(67, 16, 273, 20);
		panel_3.add(s_TF_file);

		final JButton sbf = new JButton("\u6D4F\u89C8");
		sbf.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File file = SwingUtils.selectSaveFile(new File(s_TF_file.getText()), new FileFilter[] { new FileFilter("dmp", "DMP文件") }, sbf);
				System.out.println(file.getPath());
				if (file != null)
					s_TF_file.setText(file.getPath());
			}
		});
		sbf.setBounds(350, 15, 57, 23);
		panel_3.add(sbf);

		JPanel panel_4 = new JPanel();
		panel_4.setBounds(0, 363, 450, 31);
		panel_4.setLayout(null);
		panel_4.setPreferredSize(new Dimension(550, 26));
		panel_4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel_4);

		proinfo = new JLabel("...");
		proinfo.setSize(new Dimension(18, 15));
		proinfo.setForeground(Color.RED);
		proinfo.setBounds(93, 10, 347, 15);
		panel_4.add(proinfo);

		tableSize = new JLabel("");
		tableSize.setForeground(Color.RED);
		tableSize.setBounds(10, 10, 73, 15);
		panel_4.add(tableSize);

		JScrollPane scrollPane = new JScrollPane(jtree);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.setBounds(0, 95, 450, 175);
		add(scrollPane);

		// super(fSuper);
	}

	private DefaultMutableTreeNode packageRoot(final CheckboxTree tree, final DefaultTreeModel dtm, final DefaultMutableTreeNode dmt, DynamicBean db, String table_schem, JLabel pbarinfo,
			JLabel tableSize) {
		wf_Db_Connect db_connect = null;
		try {
			if (db != null) {
				db_connect = wf_Db_Server.getInstance().getFactory(db).get();
				DatabaseMetaData data = db_connect.getConnect().getMetaData();
				ResultSet schemas = data.getSchemas();
				ResultSetMetaData metaData = schemas.getMetaData();
				int count = metaData.getColumnCount();
				while (schemas.next()) {
					for (int i = 0; i < count; i++) {
						String busschem = schemas.getString("TABLE_SCHEM");
						// 模式
						if (table_schem.equals(busschem)) {
							ResultSet resultSet = data.getTables(null, busschem, "%", new String[] { "TABLE" });
							try {
								int _i = 1;
								while (resultSet.next()) {
									final String tName = resultSet.getString("TABLE_NAME");
									Statement statement = db_connect.getConnect().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
									try {
										ResultSet set = statement.executeQuery("SELECT * FROM " + tName);
										try {
											set.last();
											final int row = set.getRow();
											new Thread() {
												public void run() {
													Runnable r = new Runnable() {
														public void run() {
															DefaultMutableTreeNode _dmt = new DefaultMutableTreeNode(new IconNode(new ImageIcon("image/table.png"), tName.toUpperCase(), row));
															dmt.add(_dmt);
															TreePath path = new TreePath(dmt);
															tree.expandPath(path);
															tree.setScrollsOnExpand(true);
															dtm.insertNodeInto(_dmt, dmt, dmt.getChildCount() - 1);
															dtm.nodeStructureChanged(_dmt);
															Util.getInstance().addCacheTreePath(tName, _dmt);
														}
													};
													SwingUtilities.invokeLater(r);
												}

											}.start();
											pbarinfo.setText("正在读取表:" + tName);
											tableSize.setText("总表数[" + (_i) + "]");
										} finally {
											set.close();
										}
									} finally {
										statement.close();
									}
									_i++;
									Thread.sleep(50);
								}
								pbarinfo.setText("表已读取完毕");
								// tree.repaint();
							} finally {
								resultSet.close();
							}
						}
					}
				}
			} else {
				proinfo.setText("请选择数据库连接");
			}

		} catch (wf_Db_Exception e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (SQLException e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			db_connect.close();
		}

		return dmt;
	}

	public void initTree(final CheckboxTree tree, DynamicBean db) {
		final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(new IconNode(new ImageIcon("image/database_type.png"), String.valueOf(db.getValue("ip")), -1));
		final DefaultTreeModel rootModel = new DefaultTreeModel(rootNode);
		tree.setModel(rootModel);
		tree.setCellRenderer(new IconNodeRenderer());
		tree.setRootVisible(true);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setRowHeight(20);
		tree.setAlignmentY(CENTER_ALIGNMENT);
		tree.setAlignmentX(CENTER_ALIGNMENT);
		tree.setVisibleRowCount(20);

		tree.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			/**
			 * 实现以键盘模糊搜索功能
			 */
			public void keyPressed(KeyEvent e) {
				char keyChar = e.getKeyChar();
				log.info(keyChar);
				String t = Util.getInstance().getKeyInput(String.valueOf(keyChar));
				if (KeyEvent.VK_ENTER == e.getKeyCode()) {
					log.info("T=" + t);
					if (t != null) {
						final DefaultMutableTreeNode tp = Util.getInstance().getCacheTreePath(String.valueOf(t));
						if (tp != null) {
							new Thread() {
								public void run() {
									Runnable r = new Runnable() {
										public void run() {
											TreeNode[] treepath = (TreeNode[]) tp.getPath();
											TreePath path = new TreePath(treepath);
											tree.setSelectionPath(path);
											tree.scrollPathToVisible(path);
										}
									};
									SwingUtilities.invokeLater(r);
								}

							}.start();
						}
					}
				}
			}
		});

		tree.addTreeCheckingListener(new TreeCheckingListener() {
			@Override
			public void valueChanged(TreeCheckingEvent e) {
				TreePath leadingPath = e.getLeadingPath();
				DefaultMutableTreeNode dmt = (DefaultMutableTreeNode) leadingPath.getLastPathComponent();
				IconNode in = (IconNode) dmt.getUserObject();
				boolean isChecked = tree.getCheckingModel().isPathChecked(leadingPath);
				String value = in.getText();
				// log.info("==:" + isChecked + "  value:" + value);
				if (isChecked && value != null) {
					Util.getInstance().add2Array(value);
					Util.getInstance().add2Sarray(tree.getSelectionPath());
				} else {
					Util.getInstance().removeArray(value);
					Util.getInstance().removeSarray(tree.getSelectionPath());
				}
				// 将选中的节点加入到缓存中

				HashSet<String> selectTables = Util.getInstance().getSelectTables();
				for (String s : selectTables) {
					log.info("当前的值是：" + s);
				}
				log.info("ArrayList size = " + Util.getInstance().getSize());
			}
		});
		packageRoot(tree, rootModel, rootNode, db, String.valueOf(db.getValue("pwd")), proinfo, tableSize);
	}

	private void removeNode(final CheckboxTree tree) {
		HashSet<TreePath> selectTreePath = Util.getInstance().getSelectTreePath();
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		for (TreePath treepath : selectTreePath) {
			if (treepath != null) {
				// 下面两行取得选取节点的父节点.
				DefaultMutableTreeNode selectionNode = (DefaultMutableTreeNode) treepath.getLastPathComponent();
				TreeNode parent = (TreeNode) selectionNode.getParent();
				if (parent != null) {
					// 由DefaultTreeModel的removeNodeFromParent()方法删除节点，包含它的子节点。
					treeModel.removeNodeFromParent(selectionNode);
				}
			}
		}
		// TreePath treepath = tree.getSelectionPath();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
}
