package org.dba.beanunit;

import java.io.Serializable;

public class DataBaseConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3037913855463217441L;
	
	private String src_server_ip;
	private String src_user;
	private String src_pwd;
	private String src_sid;
	private String src_service;
	
	private String dest_server_ip;
	private String dest_user;
	private String dest_pwd;
	private String dest_sid;
	private String dest_service;
	
	private boolean indexes;
	private boolean trigger;
	private boolean savedata;
	private boolean buildsql;
	private boolean imexec;
	private boolean openlog;
	private boolean lxs;
	private String charset;
	
	private String compareFilePath;
	
	
	public String getCompareFilePath() {
		return compareFilePath;
	}
	public void setCompareFilePath(String compareFilePath) {
		this.compareFilePath = compareFilePath;
	}
	public String getSrc_server_ip() {
		return src_server_ip;
	}
	public void setSrc_server_ip(String src_server_ip) {
		this.src_server_ip = src_server_ip;
	}
	public String getSrc_user() {
		return src_user;
	}
	public void setSrc_user(String src_user) {
		this.src_user = src_user;
	}
	public String getSrc_pwd() {
		return src_pwd;
	}
	public void setSrc_pwd(String src_pwd) {
		this.src_pwd = src_pwd;
	}
	public String getSrc_sid() {
		return src_sid;
	}
	public void setSrc_sid(String src_sid) {
		this.src_sid = src_sid;
	}
	public String getDest_server_ip() {
		return dest_server_ip;
	}
	public void setDest_server_ip(String dest_server_ip) {
		this.dest_server_ip = dest_server_ip;
	}
	public String getDest_user() {
		return dest_user;
	}
	public void setDest_user(String dest_user) {
		this.dest_user = dest_user;
	}
	public String getDest_pwd() {
		return dest_pwd;
	}
	public void setDest_pwd(String dest_pwd) {
		this.dest_pwd = dest_pwd;
	}
	public String getDest_sid() {
		return dest_sid;
	}
	public void setDest_sid(String dest_sid) {
		this.dest_sid = dest_sid;
	}
	public boolean isIndexes() {
		return indexes;
	}
	public void setIndexes(boolean indexes) {
		this.indexes = indexes;
	}
	public boolean isTrigger() {
		return trigger;
	}
	public void setTrigger(boolean trigger) {
		this.trigger = trigger;
	}
	public boolean isSavedata() {
		return savedata;
	}
	public void setSavedata(boolean savedata) {
		this.savedata = savedata;
	}
	public boolean isBuildsql() {
		return buildsql;
	}
	public void setBuildsql(boolean buildsql) {
		this.buildsql = buildsql;
	}
	public boolean isImexec() {
		return imexec;
	}
	public void setImexec(boolean imexec) {
		this.imexec = imexec;
	}
	public boolean isOpenlog() {
		return openlog;
	}
	public void setOpenlog(boolean openlog) {
		this.openlog = openlog;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getSrc_service() {
		return src_service;
	}
	public void setSrc_service(String src_service) {
		this.src_service = src_service;
	}
	public String getDest_service() {
		return dest_service;
	}
	public void setDest_service(String dest_service) {
		this.dest_service = dest_service;
	}
	public boolean isLxs() {
		return lxs;
	}
	public void setLxs(boolean lxs) {
		this.lxs = lxs;
	}

}
