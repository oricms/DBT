package org.dba.test;

import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import javax.swing.JTextArea;

public class PageReader {
	public JTextArea f = null;
	private String charset = "utf-8";
	private String urlString = "http://www.ifeng.com";
	private StringBuffer HtmlContent = new StringBuffer();

	public PageReader(JTextArea ta) {
		this.f = ta;
		this.startRead();
	}

	private void startRead() {
		try {
			URL url = new URL(urlString);
			URLConnection connection = url.openConnection();

			InputStream is = connection.getInputStream();
			Scanner in = new Scanner(is, this.charset);
			while (in.hasNextLine()) {
				f.append(in.nextLine() + "\r\n");
			}
			in.close();
			is.close();
			// System.out.println(HtmlContent);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}