package org.dba.util;

import org.dba.ui.SuperFrame;

public class FrameManager {

	private static FrameManager fm = null;
	public static SuperFrame sf;

	public static synchronized FrameManager getInstance() {
		if (fm == null) {
			fm = new FrameManager();
		}
		return fm;
	}
	
	public FrameManager(){
		sf = new SuperFrame();
		sf.display();
	}

}
