package org.dba.ui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import org.dba.util.FileFilter;
import org.dba.util.SwingUtils;
import org.dba.util.Util;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.awt.Color;

import javax.swing.SwingConstants;

public class VersionConvertPanel extends JPanel {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6433571649898843633L;
	private JTextField t_file;
	private JTextField startadd;
	private JTextField len;
	public VersionConvertPanel() {
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Oracle\u5907\u4EFD\u6587\u4EF6\u7248\u672C\u8F6C\u6362", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 10, 430, 280);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblDmp = new JLabel("DMP\u6587\u4EF6\uFF1A");
		lblDmp.setBounds(28, 36, 54, 15);
		panel.add(lblDmp);
		
		t_file = new JTextField();
		t_file.setBounds(80, 33, 243, 21);
		panel.add(t_file);
		t_file.setColumns(10);
		
		final JButton browser = new JButton("\u6D4F\u89C8");
		browser.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File file = SwingUtils.selectOpenFile(new File(t_file.getText()), new FileFilter[] { new FileFilter("dmp", "DMP文件") }, browser);
				if (file != null)
					t_file.setText(file.getPath());
			}
		});
		browser.setBounds(333, 32, 60, 23);
		panel.add(browser);
		
		JLabel label = new JLabel("\u6570\u636E\u5E93\u7248\u672C\uFF1A");
		label.setBounds(10, 76, 72, 15);
		panel.add(label);
		
		final JComboBox ver = new JComboBox();
		ver.setEditable(true);
		ver.setModel(new DefaultComboBoxModel(new String[] {"V10.01.00"}));
		ver.setBounds(80, 73, 156, 21);
		panel.add(ver);
		
		JButton button = new JButton("\u8F6C\u6362");
		button.setBounds(246, 72, 60, 23);
		panel.add(button);
		
		JLabel label_1 = new JLabel("\u504F\u79FB\u91CF\uFF1A");
		label_1.setForeground(Color.RED);
		label_1.setBounds(34, 107, 48, 15);
		panel.add(label_1);
		
		startadd = new JTextField();
		startadd.setHorizontalAlignment(SwingConstants.CENTER);
		startadd.setForeground(Color.RED);
		startadd.setText("10");
		startadd.setBounds(80, 104, 28, 21);
		panel.add(startadd);
		startadd.setColumns(10);
		
		JLabel label_2 = new JLabel("\u957F\u5EA6\uFF1A");
		label_2.setForeground(Color.RED);
		label_2.setBounds(118, 107, 36, 15);
		panel.add(label_2);
		
		len = new JTextField();
		len.setHorizontalAlignment(SwingConstants.CENTER);
		len.setForeground(Color.RED);
		len.setText("9");
		len.setBounds(164, 104, 28, 21);
		panel.add(len);
		len.setColumns(10);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String versioninfo = (String)ver.getSelectedItem();
				String file = t_file.getText();
				if(file.equals("")){
					JOptionPane.showMessageDialog(VersionConvertPanel.this, "请选择DMP文件");
					return;
				}
				if(versioninfo == null){
					JOptionPane.showMessageDialog(VersionConvertPanel.this, "请选择数据库版本号");
					return;
				}
				if(Integer.parseInt(startadd.getText()) == 0){
					JOptionPane.showMessageDialog(VersionConvertPanel.this, "偏移量设置错误");
					return;
				}
				if(Integer.parseInt(len.getText()) == 0 || Integer.parseInt(len.getText())>10){
					JOptionPane.showMessageDialog(VersionConvertPanel.this, "长度设置错误");
					return;
				}
				Util.changeFile(file, String.valueOf(ver.getSelectedItem()), Integer.parseInt(startadd.getText()), Integer.parseInt(len.getText()));
			}
		});
	}
}
