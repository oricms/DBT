drop user BZADMIN cascade;
create user BZADMIN identified by BZADMIN;
grant dba,connect,resource to BZADMIN;
exit;