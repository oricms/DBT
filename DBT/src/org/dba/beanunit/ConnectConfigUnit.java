package org.dba.beanunit;

public class ConnectConfigUnit {
	
	private String _ip = null;
	private String _port = null;
	private String _sid = null;
	private String _user = null;
	private String _msm = null;
	private String _pwd = null;
	public String get_ip() {
		return _ip;
	}
	public void set_ip(String _ip) {
		this._ip = _ip;
	}
	public String get_port() {
		return _port;
	}
	public void set_port(String _port) {
		this._port = _port;
	}
	public String get_sid() {
		return _sid;
	}
	public void set_sid(String _sid) {
		this._sid = _sid;
	}
	public String get_user() {
		return _user;
	}
	public void set_user(String _user) {
		this._user = _user;
	}
	public String get_msm() {
		return _msm;
	}
	public void set_msm(String _msm) {
		this._msm = _msm;
	}
	public String get_pwd() {
		return _pwd;
	}
	public void set_pwd(String _pwd) {
		this._pwd = _pwd;
	}
	
	

}
