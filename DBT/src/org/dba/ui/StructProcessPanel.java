package org.dba.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.UIManager;

import java.awt.Color;
import java.io.File;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JProgressBar;

import org.dba.beanunit.DataBaseConfig;
import org.dba.util.DbaXml;
import org.dba.util.ProcessFile;
import org.dba.util.SwingUtils;

public class StructProcessPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -8257291083617052926L;

	private JTextField src_server_ip;
	private JTextField src_user;
	private JPasswordField src_pwd;
	private JTextField src_sid;
	private JTextField dest_server_ip;
	private JTextField dest_user;
	private JPasswordField dest_pwd;
	private JTextField dest_sid;
	private JButton strutscompare;
	private JProgressBar pbar;
	private JLabel proinfo;

	private DataBaseConfig dbc;

	private JCheckBox indexes;
	private JCheckBox trigger;
	private JCheckBox savedata;
	private JCheckBox lxsources;

	public JTextField getSrc_server_ip() {
		return src_server_ip;
	}

	public void setSrc_server_ip(JTextField src_server_ip) {
		this.src_server_ip = src_server_ip;
	}

	public JTextField getSrc_user() {
		return src_user;
	}

	public void setSrc_user(JTextField src_user) {
		this.src_user = src_user;
	}

	public JPasswordField getSrc_pwd() {
		return src_pwd;
	}

	public void setSrc_pwd(JPasswordField src_pwd) {
		this.src_pwd = src_pwd;
	}

	public JTextField getSrc_sid() {
		return src_sid;
	}

	public void setSrc_sid(JTextField src_sid) {
		this.src_sid = src_sid;
	}

	public JTextField getDest_server_ip() {
		return dest_server_ip;
	}

	public void setDest_server_ip(JTextField dest_server_ip) {
		this.dest_server_ip = dest_server_ip;
	}

	public JTextField getDest_user() {
		return dest_user;
	}

	public void setDest_user(JTextField dest_user) {
		this.dest_user = dest_user;
	}

	public JPasswordField getDest_pwd() {
		return dest_pwd;
	}

	public void setDest_pwd(JPasswordField dest_pwd) {
		this.dest_pwd = dest_pwd;
	}

	public JTextField getDest_sid() {
		return dest_sid;
	}

	public void setDest_sid(JTextField dest_sid) {
		this.dest_sid = dest_sid;
	}

	public JCheckBox getIndexes() {
		return indexes;
	}

	public void setIndexes(JCheckBox indexes) {
		this.indexes = indexes;
	}

	public JCheckBox getTrigger() {
		return trigger;
	}

	public void setTrigger(JCheckBox trigger) {
		this.trigger = trigger;
	}

	public JCheckBox getSavedata() {
		return savedata;
	}

	public void setSavedata(JCheckBox savedata) {
		this.savedata = savedata;
	}

	public JCheckBox getImexec() {
		return imexec;
	}

	public void setImexec(JCheckBox imexec) {
		this.imexec = imexec;
	}

	public JRadioButton getBuildsql() {
		return buildsql;
	}

	public void setBuildsql(JRadioButton buildsql) {
		this.buildsql = buildsql;
	}

	public JCheckBox getOpenlog() {
		return openlog;
	}

	public void setOpenlog(JCheckBox openlog) {
		this.openlog = openlog;
	}

	public JComboBox getCharset() {
		return charset;
	}

	public JCheckBox getLxsources() {
		return lxsources;
	}

	public void setLxsources(JCheckBox lxsources) {
		this.lxsources = lxsources;
	}

	public void setCharset(JComboBox charset) {
		this.charset = charset;
	}

	private JCheckBox imexec;
	private JRadioButton buildsql;
	private JCheckBox openlog;
	private JComboBox charset;
	private JTextField src_service;
	private JTextField dest_service;
	private JTextField comparepath;

	public JTextField getSrc_service() {
		return src_service;
	}

	public void setSrc_service(JTextField src_service) {
		this.src_service = src_service;
	}

	public JTextField getDest_service() {
		return dest_service;
	}

	public void setDest_service(JTextField dest_service) {
		this.dest_service = dest_service;
	}

	public StructProcessPanel() {
		init();
	}

	public void init() {
		initData();
		initFace();
		initListener();
	}

	private void initData() {
		dbc = new DataBaseConfig();
	}

	private void initListener() {
		strutscompare.addActionListener(this);
	}

	private void initFace() {

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u5BF9\u6BD4\u9009\u9879", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u6E90\u6570\u636E\u5E93\u670D\u52A1\u5668\u914D\u7F6E", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));

		JLabel label = new JLabel("\u670D\u52A1\u5668\u5730\u5740\uFF1A");
		label.setBounds(10, 27, 72, 15);
		panel_1.add(label);

		src_server_ip = new JTextField(DbaXml.CDD.src_server_ip);
		src_server_ip.setPreferredSize(new Dimension(200, 21));
		src_server_ip.setBounds(81, 24, 118, 21);
		panel_1.add(src_server_ip);

		src_user = new JTextField(DbaXml.CDD.src_user);
		src_user.setPreferredSize(new Dimension(100, 21));
		src_user.setBounds(81, 52, 118, 21);
		panel_1.add(src_user);

		JLabel label_1 = new JLabel("\u7528\u6237\u540D\uFF1A");
		label_1.setBounds(34, 55, 48, 15);
		panel_1.add(label_1);

		JLabel src_pwd_lable = new JLabel(" \u5BC6   \u7801\uFF1A");
		src_pwd_lable.setBounds(22, 83, 60, 21);
		panel_1.add(src_pwd_lable);

		src_pwd = new JPasswordField(DbaXml.CDD.src_pwd);
		src_pwd.setPreferredSize(new Dimension(100, 21));
		src_pwd.setBounds(81, 83, 118, 21);
		panel_1.add(src_pwd);

		src_sid = new JTextField(DbaXml.CDD.src_Instance);
		src_sid.setPreferredSize(new Dimension(250, 21));
		src_sid.setBounds(81, 142, 118, 21);
		panel_1.add(src_sid);

		JLabel src_sid_lable = new JLabel("\u6A21\u5F0F\u540D\uFF1A");
		src_sid_lable.setBounds(34, 145, 48, 15);
		panel_1.add(src_sid_lable);

		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u76EE\u6807\u6570\u636E\u5E93\u670D\u52A1\u5668\u914D\u7F6E", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));

		JLabel label_4 = new JLabel("\u670D\u52A1\u5668\u5730\u5740\uFF1A");
		label_4.setBounds(10, 27, 72, 15);
		panel_2.add(label_4);

		dest_server_ip = new JTextField(DbaXml.CDD.dest_server_ip);
		dest_server_ip.setPreferredSize(new Dimension(200, 21));
		dest_server_ip.setBounds(81, 24, 118, 21);
		panel_2.add(dest_server_ip);

		dest_user = new JTextField(DbaXml.CDD.dest_user);
		dest_user.setPreferredSize(new Dimension(100, 21));
		dest_user.setBounds(81, 52, 118, 21);
		panel_2.add(dest_user);

		JLabel label_5 = new JLabel("\u7528\u6237\u540D\uFF1A");
		label_5.setBounds(34, 55, 48, 15);
		panel_2.add(label_5);

		JLabel label_6 = new JLabel(" \u5BC6   \u7801\uFF1A");
		label_6.setBounds(22, 83, 60, 21);
		panel_2.add(label_6);

		dest_pwd = new JPasswordField(DbaXml.CDD.dest_pwd);
		dest_pwd.setPreferredSize(new Dimension(100, 21));
		dest_pwd.setBounds(81, 83, 118, 21);
		panel_2.add(dest_pwd);

		dest_sid = new JTextField(DbaXml.CDD.dest_Instance);
		dest_sid.setPreferredSize(new Dimension(250, 21));
		dest_sid.setBounds(81, 138, 118, 21);
		panel_2.add(dest_sid);

		JLabel label_7 = new JLabel("\u6A21\u5F0F\u540D\uFF1A");
		label_7.setBounds(34, 141, 48, 15);
		panel_2.add(label_7);

		JLabel label_8 = new JLabel("\u5904\u7406\u8FDB\u5EA6\uFF1A");

		pbar = new JProgressBar();

		proinfo = new JLabel("...");
		proinfo.setForeground(Color.RED);

		strutscompare = new JButton("\u5BF9\u6BD4\u7ED3\u6784");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout.createSequentialGroup().addGap(4).addComponent(label_8).addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(pbar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap())
						.addGroup(
								Alignment.TRAILING,
								groupLayout
										.createSequentialGroup()
										.addContainerGap(4, Short.MAX_VALUE)
										.addGroup(
												groupLayout
														.createParallelGroup(Alignment.TRAILING)
														.addGroup(groupLayout.createSequentialGroup().addGap(4).addComponent(panel, 0, 0, Short.MAX_VALUE))
														.addGroup(
																groupLayout.createSequentialGroup().addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 211, GroupLayout.PREFERRED_SIZE).addGap(18)
																		.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE))).addContainerGap())
						.addGroup(Alignment.TRAILING,
								groupLayout.createSequentialGroup().addContainerGap(41, Short.MAX_VALUE).addComponent(proinfo, GroupLayout.PREFERRED_SIZE, 386, GroupLayout.PREFERRED_SIZE).addGap(28))
						.addGroup(
								groupLayout.createSequentialGroup().addGap(176).addComponent(strutscompare, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
										.addContainerGap(186, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addGroup(
								groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
										.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(
								groupLayout.createParallelGroup(Alignment.LEADING).addComponent(label_8)
										.addComponent(pbar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(proinfo).addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(strutscompare).addContainerGap()));

		JLabel label_10 = new JLabel("\u670D\u52A1\u540D\uFF1A");
		label_10.setBounds(34, 114, 48, 15);
		panel_2.add(label_10);

		dest_service = new JTextField(DbaXml.CDD.dest_sid);
		dest_service.setColumns(10);
		dest_service.setBounds(81, 111, 118, 21);
		panel_2.add(dest_service);

		JLabel label_9 = new JLabel("\u670D\u52A1\u540D\uFF1A");
		label_9.setBounds(32, 117, 48, 15);
		panel_1.add(label_9);

		src_service = new JTextField(DbaXml.CDD.src_sid);
		src_service.setBounds(81, 111, 118, 21);
		panel_1.add(src_service);
		src_service.setColumns(10);
		panel.setLayout(null);

		indexes = new JCheckBox("\u7D22\u5F15");
		indexes.setBounds(6, 22, 49, 23);
		indexes.setSelected(Boolean.valueOf(DbaXml.CDD.indexs));
		panel.add(indexes);

		trigger = new JCheckBox("\u89E6\u53D1\u5668");
		trigger.setBounds(57, 22, 61, 23);
		trigger.setSelected(Boolean.valueOf(DbaXml.CDD.trigger));
		panel.add(trigger);

		savedata = new JCheckBox("\u4FDD\u7559\u76EE\u6807\u5E93\u6570\u636E");
		savedata.setSelected(true);
		savedata.setBounds(120, 22, 109, 23);
		savedata.setSelected(Boolean.valueOf(DbaXml.CDD.savedata));
		panel.add(savedata);

		imexec = new JCheckBox("\u5BF9\u6BD4\u540E\u7ACB\u5373\u6267\u884C");
		imexec.setSelected(true);
		imexec.setBounds(302, 22, 109, 23);
		imexec.setSelected(Boolean.valueOf(DbaXml.CDD.imexec));
		panel.add(imexec);

		buildsql = new JRadioButton("\u751F\u6210sql");
		buildsql.setBounds(231, 22, 67, 23);
		buildsql.setSelected(Boolean.valueOf(DbaXml.CDD.buildsql));
		panel.add(buildsql);

		openlog = new JCheckBox("\u5BF9\u6BD4\u5B8C\u4E4B\u540E\uFF0C\u6253\u5F00\u65E5\u5FD7\u6587\u4EF6");
		openlog.setSelected(true);
		openlog.setBounds(6, 47, 169, 23);
		openlog.setSelected(Boolean.valueOf(DbaXml.CDD.openlog));
		panel.add(openlog);

		charset = new JComboBox();
		charset.setModel(new DefaultComboBoxModel(new String[] { "UTF-8", "GBK" }));
		charset.setBounds(68, 71, 61, 21);
		charset.setSelectedItem(DbaXml.CDD.charset);
		panel.add(charset);

		JLabel label_2 = new JLabel("\u5B57\u7B26\u96C6\u9009\u62E9");
		label_2.setBounds(6, 74, 61, 15);
		panel.add(label_2);

		JLabel label_3 = new JLabel("\u6B64\u5B57\u7B26\u96C6\u662F\u5199\u6570\u636E\u6587\u4EF6\u7F16\u7801\uFF0C\u4E0D\u662F\u6570\u636E\u5E93\u7F16\u7801");
		label_3.setForeground(Color.RED);
		label_3.setBounds(139, 74, 240, 15);
		panel.add(label_3);

		comparepath = new JTextField(DbaXml.CDD.comparepath);
		comparepath.setPreferredSize(new Dimension(190, 21));
		comparepath.setBounds(68, 102, 263, 21);
		panel.add(comparepath);

		JLabel label_11 = new JLabel("\u4FDD\u5B58\u8DEF\u5F84\uFF1A");
		label_11.setBounds(6, 105, 60, 15);
		panel.add(label_11);

		JButton b_save = new JButton("\u6D4F\u89C8");
		b_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				File file = SwingUtils.selectSaveFile();
				if (file != null)
					comparepath.setText(file.getPath());
			}
		});
		b_save.setActionCommand("B_browse");
		b_save.setBounds(342, 101, 57, 23);
		panel.add(b_save);
		
		lxsources = new JCheckBox("\u79BB\u7EBF\u539F\u6570\u636E");
		lxsources.setBounds(182, 47, 85, 23);
		lxsources.setSelected(Boolean.valueOf(DbaXml.CDD.lxsource));
		panel.add(lxsources);
		setLayout(groupLayout);
	}

	public void actionPerformed(ActionEvent e) {

		dbc.setSrc_server_ip(src_server_ip.getText().toString());
		dbc.setSrc_pwd(String.valueOf(src_pwd.getPassword()));
		dbc.setSrc_user(src_user.getText().toString());
		dbc.setSrc_sid(src_sid.getText().toString());
		dbc.setSrc_service(src_service.getText().toString());

		dbc.setDest_server_ip(dest_server_ip.getText().toString());
		dbc.setDest_user(dest_user.getText().toString());
		dbc.setDest_pwd(String.valueOf(dest_pwd.getPassword()));
		dbc.setDest_sid(dest_sid.getText().toString());
		dbc.setDest_service(dest_service.getText().toString());

		dbc.setIndexes(indexes.isSelected());
		dbc.setTrigger(trigger.isSelected());
		dbc.setBuildsql(buildsql.isSelected());
		dbc.setCharset((String) charset.getSelectedItem());
		dbc.setOpenlog(openlog.isSelected());
		dbc.setImexec(imexec.isSelected());
		dbc.setCompareFilePath(comparepath.getText().toString() + "\\");
		dbc.setLxs(lxsources.isSelected());
		if (e.getActionCommand().equals("对比结构")) {
			// 检测各项字段
			if(!lxsources.isSelected()){
				if (src_server_ip.getText().toString().equals("")) {
					JOptionPane.showMessageDialog(this, "源服务器IP地址不能为空");
					return;
				}
				if (src_pwd.getPassword().toString().equals("")) {
					JOptionPane.showMessageDialog(this, "源服务器密码不能为空");
					return;
				}
				if (src_user.getText().toString().equals("")) {
					JOptionPane.showMessageDialog(this, "源服务器用户名不能为空");
					return;
				}
				if (src_sid.getText().toString().equals("")) {
					JOptionPane.showMessageDialog(this, "源服务器服务名不能为空");
					return;
				}
				if (src_service.getText().toString().equals("")) {
					JOptionPane.showMessageDialog(this, "源服务器IP地址不能为空");
					return;
				}
			}
			if (dest_server_ip.getText().toString().equals("")) {
				JOptionPane.showMessageDialog(this, "目标服务器IP地址不能为空");
				return;
			}
			if (dest_user.getText().toString().equals("")) {
				JOptionPane.showMessageDialog(this, "目标服务器用户名不能为空");
				return;
			}
			if (dest_pwd.getPassword().toString().equals("")) {
				JOptionPane.showMessageDialog(this, "目标服务器密码不能为空");
				return;
			}
			if (dest_sid.getText().toString().equals("")) {
				JOptionPane.showMessageDialog(this, "目标服务器服务名不能为空");
				return;
			}
			if (dest_service.getText().toString().equals("")) {
				JOptionPane.showMessageDialog(this, "目标服务器模式名不能为空");
				return;
			}
			if (comparepath.getText().toString().equals("")) {
				JOptionPane.showMessageDialog(this, "保存路径不能为空!");
				return;
			}
			pbar.setMaximum(2000);
			new Thread(new ProcessFile(strutscompare, pbar, proinfo, dbc)).start();
		}
	}

	public JTextField getComparepath() {
		return comparepath;
	}

	public void setComparepath(JTextField t_file) {
		comparepath = t_file;
	}
}
