package org.dba.test;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Timer1 {
	
	public static void main(String[] args) {
		
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				System.out.println(new Date());
				
			}
		}, new Date(), 5*1000);
		
		System.out.println(java.util.UUID.randomUUID().toString().length());
	}

}
