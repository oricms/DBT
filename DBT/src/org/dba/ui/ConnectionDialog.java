package org.dba.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;
import javax.swing.UIManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.DynamicBean;
import org.dba.util.ProjectContext;
import org.dba.util.Util;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class ConnectionDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4457304686323622278L;
	Log log = LogFactory.getLog(getClass());
	private final JPanel contentPanel = new JPanel();
	private JTextField ip;
	private JTextField port;
	private JTextField sid;
	private JTextField user;
	private JTextField msm;
	private JPasswordField pwd;
	private JTextField aliais;

	/**
	 * Create the dialog.
	 */
	public ConnectionDialog(SuperFrame fSuper) {
		super(fSuper);
		setResizable(false);
		setTitle("\u8FDE\u63A5\u914D\u7F6E");
		setBounds(100, 100, 321, 301);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("\u670D\u52A1\u5668IP\uFF1A");
			lblNewLabel.setBounds(10, 50, 69, 15);
			contentPanel.add(lblNewLabel);
		}

		ip = new JTextField();
		ip.setBounds(67, 47, 203, 21);
		contentPanel.add(ip);
		ip.setColumns(10);

		JLabel label = new JLabel("\u5BC6\u7801\uFF1A");
		label.setBounds(36, 187, 36, 15);
		contentPanel.add(label);

		JLabel lblNewLabel_1 = new JLabel("\u7AEF\u53E3\uFF1A");
		lblNewLabel_1.setBounds(36, 84, 36, 15);
		contentPanel.add(lblNewLabel_1);

		JLabel label_1 = new JLabel("\u670D \u52A1 \u540D\uFF1A");
		label_1.setBounds(10, 109, 60, 15);
		contentPanel.add(label_1);

		JLabel label_2 = new JLabel("\u7528 \u6237 \u540D\uFF1A");
		label_2.setBounds(10, 162, 60, 15);
		contentPanel.add(label_2);

		final JCheckBox autosave = new JCheckBox("\u81EA\u52A8\u4FDD\u5B58");
		autosave.setSelected(true);
		autosave.setBounds(36, 211, 103, 23);
		contentPanel.add(autosave);

		port = new JTextField("1521");
		port.setBounds(67, 78, 203, 21);
		contentPanel.add(port);
		port.setColumns(10);

		sid = new JTextField();
		sid.setBounds(67, 106, 203, 21);
		contentPanel.add(sid);
		sid.setColumns(10);

		user = new JTextField();
		user.setBounds(67, 156, 203, 21);
		contentPanel.add(user);
		user.setColumns(10);

		JLabel label_3 = new JLabel("\u6A21\u5F0F\u540D\uFF1A");
		label_3.setBounds(24, 137, 48, 15);
		contentPanel.add(label_3);

		msm = new JTextField();
		msm.setBounds(67, 131, 203, 21);
		contentPanel.add(msm);
		msm.setColumns(10);

		pwd = new JPasswordField();
		pwd.setBounds(67, 184, 203, 21);
		contentPanel.add(pwd);
		
		JLabel label_4 = new JLabel("\u522B\u540D\uFF1A");
		label_4.setBounds(34, 25, 36, 15);
		contentPanel.add(label_4);
		
		aliais = new JTextField();
		aliais.setBounds(67, 19, 203, 21);
		contentPanel.add(aliais);
		aliais.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("\u786E\u5B9A");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						String _ip = ip.getText().trim();
						String _alisais = aliais.getText().trim();
						String _port = port.getText().trim();
						String _sid = sid.getText().trim();
						String _user = user.getText().trim();
						String _msm = msm.getText().trim();
						String _pwd = String.valueOf(pwd.getPassword());
						
						if (_alisais.equals("")) {
							JOptionPane.showMessageDialog(ConnectionDialog.this, "别名不能为空!");
							return;
						}
						
						if (_ip.equals("")) {
							JOptionPane.showMessageDialog(ConnectionDialog.this, "IP不能为空!");
							return;
						}
						if (_port.equals("")) {
							JOptionPane.showMessageDialog(ConnectionDialog.this, "端口不能为空!");
							return;
						}
						if (_sid.equals("")) {
							JOptionPane.showMessageDialog(ConnectionDialog.this, "服务名不能为空!");
							return;
						}
						if (_user.equals("")) {
							JOptionPane.showMessageDialog(ConnectionDialog.this, "用户名不能为空!");
							return;
						}
						if (_msm.equals("")) {
							JOptionPane.showMessageDialog(ConnectionDialog.this, "模式名称不能为空!");
							return;
						}
						if (_pwd.equals("")) {
							JOptionPane.showMessageDialog(ConnectionDialog.this, "密码不能为空!");
							return;
						}
						DynamicBean db = new DynamicBean(new String[] { "aliais:String="+_alisais,"ip:String=" + _ip, "port:String=" + _port, "sid:String=" + _sid, "user:String=" + _user, "msm:String=" + _msm,
								"pwd:String=" + _pwd });
						if (autosave.isSelected()) {
							if (Util.getInstance().build_config_file(db)) {
								ConnectionDialog cd = (ConnectionDialog) UIManager.get("ConnectionDialog");
								cd.setVisible(false);
							}
						}
						JComboBox connectComBox = (JComboBox) UIManager.get("connectComBox");

						connectComBox.setModel(new DefaultComboBoxModel(Util.getInstance().readConfig()));
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("\u53D6\u6D88");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						ConnectionDialog cd = (ConnectionDialog) UIManager.get("ConnectionDialog");
						cd.setVisible(false);
					}
				});
			}
		}
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
	}
}
