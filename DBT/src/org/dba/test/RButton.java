package org.dba.test;

/**
 * @(#)RJButton.java  0.1.0  2007-9-11
 */

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Custom JButton
 */
public class RButton extends JButton {
	private static final long serialVersionUID = 39082560987930759L;
	public static final Color BUTTON_COLOR1 = new Color(205, 255, 205);
	public static final Color BUTTON_COLOR2 = new Color(51, 154, 47);
	// public static final Color BUTTON_COLOR1 = new Color(125, 161, 237);
	// public static final Color BUTTON_COLOR2 = new Color(91, 118, 173);
	public static final Color BUTTON_FOREGROUND_COLOR = Color.WHITE;
	private boolean hover;

	@Override
	protected void paintComponent(Graphics g) {

		Graphics2D g2d = (Graphics2D) g.create();
		int h = getHeight();
		int w = getWidth();
		float tran = 0.3F;

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		// 抗锯齿提示值——使用抗锯齿模式完成呈现
		// 告诉绘制API我们需要平滑一点，否则绘制出来会有很多锯齿哟。

		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
				tran));
		// 告诉绘图API我们需要绘制一个有透明度的，tran就是透明度（0-1）

		// RoundRectangle2D，这个就是边角都为圆角的方形
		RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, w, h, 5,
				5);
		/*
		 * x - 设置此 RoundRectangle2D 的 X 坐标 y - 设置此 RoundRectangle2D 的 Y 坐标 w -
		 * 设置此 RoundRectangle2D 的宽度 h - 设置此 RoundRectangle2D 的高度 arcWidth - 设置此
		 * RoundRectangle2D 的弧宽 arcHeight - 设置此 RoundRectangle2D 的弧高
		 */

		/* Rectangle2D.Float r2d = new Rectangle2D.Float(0,0,20,20); */
//		Shape clip = g2d.getClip();
//		g2d.clip(r2d);
//		g2d.fillRect(0, 0, w, h);
//		g2d.setClip(clip);
//		System.out.println(clip.getBounds());
//		g2d.drawRoundRect(0, 0, w - 1, h - 1, 5, 5);
//		// 画图片
//		// g2d.setClip(0,0,w,h);
//		//Image image = this.getToolkit().createImage("database_connect.png");
//		ImageIcon ImageIcon1 = new ImageIcon("G:\\DBT\\DBT\\image\\database_connect.png");
//		g2d.drawImage(ImageIcon1.getImage(), 10, 10, null);
//
//		// 倒三角
//		g2d.setClip(w / 2, 0, w, h);
//		/*
//		 * x - 新剪贴区矩形的 x 坐标。 y - 新剪贴区矩形的 y 坐标。 width - 新剪贴区矩形的宽度。 height -
//		 * 新剪贴区矩形的高度。
//		 */
//		g2d.setColor(Color.black);
//		int x1 = (w / 8) * 5;
//		int y1 = h / 4;
//		int x2 = (w / 8) * 7;
//		int y2 = h / 4;
//		int x3 = (w / 8) * 6;
//		int y3 = (h / 4) * 3;
//		g2d.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);// 三角形
//		g2d.dispose();
		//super.paintComponent(g);

	}

	public RButton(String icon1) {
		ImageIcon ImageIcon1 = new ImageIcon("G:\\DBT\\DBT\\image\\database_connect.png");

		// setLayout(new BorderLayout());
		setFont(new Font("system", Font.PLAIN, 12));
		/*
		 * JLabel j1 = new JLabel(ImageIcon1); j1.setBounds(0, 0, 20, 20);
		 * add(j1,BorderLayout.LINE_START);
		 */

		setBorderPainted(false);// 不绘制边框
		setForeground(BUTTON_COLOR2);
		setFocusPainted(false);
		setContentAreaFilled(false);// 不绘制内容
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				// setForeground(BUTTON_FOREGROUND_COLOR);
				// setBackground(Color.red);
				setBorderPainted(true);
			
				repaint();
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setForeground(BUTTON_COLOR2);
				setBorderPainted(false);
				repaint();
			}
		});
	}

}