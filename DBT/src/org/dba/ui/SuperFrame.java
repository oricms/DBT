package org.dba.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.dba.util.DbaXml;
import org.dba.util.ProjectContext;

public class SuperFrame extends JFrame implements WindowListener {
	private static final long serialVersionUID = 6587875556279522932L;
	
	private JTabbedPane TAB_content;
	private ImportPanel P_import;
	private ExportPanel P_export;
	private StructProcessPanel structcom;
	private ClobProcessPanel clobpro;
	private SimpleTablePanel stp ; 
	private VersionConvertPanel vcp;
	private InitDataPanel idp;
	
	public SuperFrame() {
		super("ORACLE数据库工具");
		setIconImage(Toolkit.getDefaultToolkit().getImage(SuperFrame.class.getResource("/org/dba/ui/database.png")));
		init();
	}
	
	public void init() {
		initData();
		initComponent();
		initListener();
		initFace();
	}
	
	private void initData() {
	}
	private void initComponent() {
		P_import = new ImportPanel();
		P_export = new ExportPanel();
		structcom = new StructProcessPanel();
		TAB_content = new JTabbedPane();
		stp = new SimpleTablePanel();
		vcp = new VersionConvertPanel();
		idp = new InitDataPanel();
		
	}
	private void initListener() {
		this.addWindowListener(this);
	}
	private void initFace() {
		JPanel base = new JPanel(new BorderLayout());
		base.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		TAB_content.add("导入", P_import);
		TAB_content.add("导出", P_export);
		TAB_content.add("库结构对比", structcom);
		TAB_content.add("表操作", stp);
		TAB_content.add("三员账号初始", idp);
		TAB_content.add("DMP文件版本转换", vcp);
		base.add(TAB_content);
		setContentPane(base);
		setSize(470, 465);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}
	
	public void windowActivated(WindowEvent arg0) {
	}
	public void windowClosed(WindowEvent arg0) {
	}
	public void windowClosing(WindowEvent arg0) {
		try {
			DbaXml.IMP.admin = P_import.getTF_admin().getText().trim();
			DbaXml.IMP.user = P_import.getTF_user().getText().trim();
			DbaXml.IMP.sid = P_import.getTF_sid().getText().trim();
			DbaXml.IMP.file = P_import.getTF_file().getText().trim();
			
			DbaXml.EXP.user = P_export.getTF_user().getText().trim();
			DbaXml.EXP.pwd = String.valueOf(P_export.getPF_user().getPassword());
			DbaXml.EXP.sid = P_export.getTF_sid().getText().trim();
			DbaXml.EXP.file = P_export.getTF_file().getText().trim();
			DbaXml.EXP.logfile = P_export.getTlog().getText().trim();
			DbaXml.EXP.sip = P_export.getServerip().getText().trim();
			DbaXml.EXP.instanceName = P_export.getInstanceName().getText().trim();
			DbaXml.EXP.islog = String.valueOf(P_export.getIslog().isSelected());
			DbaXml.EXP.compress = String.valueOf(P_export.getCompress().isSelected());
			DbaXml.EXP.indexes = String.valueOf(P_export.getIndexes().isSelected());
			DbaXml.EXP.usermodel = String.valueOf(P_export.getUsermodel().isSelected());
			DbaXml.EXP.fullmodel = String.valueOf(P_export.getFullmodel().isSelected());
			DbaXml.EXP.zl = String.valueOf(P_export.getIszl().isSelected());
			
			DbaXml.CDD.src_server_ip = String.valueOf(structcom.getSrc_server_ip().getText().trim());
			DbaXml.CDD.src_user = String.valueOf(structcom.getSrc_user().getText().trim());
			DbaXml.CDD.src_pwd = String.valueOf(structcom.getSrc_pwd().getPassword());
			DbaXml.CDD.src_sid = String.valueOf(structcom.getSrc_service().getText().trim());
			DbaXml.CDD.src_Instance = String.valueOf(structcom.getSrc_sid().getText().trim());
			DbaXml.CDD.dest_server_ip = String.valueOf(structcom.getDest_server_ip().getText().trim());
			DbaXml.CDD.dest_pwd = String.valueOf(structcom.getDest_pwd().getPassword());
			DbaXml.CDD.dest_sid = String.valueOf(structcom.getDest_service().getText().trim());
			DbaXml.CDD.dest_user = String.valueOf(structcom.getDest_user().getText().trim());
			DbaXml.CDD.dest_Instance = String.valueOf(structcom.getDest_sid().getText().trim());
			DbaXml.CDD.indexs = String.valueOf(structcom.getIndexes().isSelected());
			DbaXml.CDD.trigger = String.valueOf(structcom.getTrigger().isSelected());
			DbaXml.CDD.savedata = String.valueOf(structcom.getSavedata().isSelected());
			DbaXml.CDD.buildsql = String.valueOf(structcom.getBuildsql().isSelected());
			DbaXml.CDD.imexec = String.valueOf(structcom.getImexec().isSelected());
			DbaXml.CDD.openlog = String.valueOf(structcom.getOpenlog().isSelected());
			DbaXml.CDD.charset = String.valueOf(structcom.getCharset().getSelectedItem());
			DbaXml.CDD.comparepath = String.valueOf(structcom.getComparepath().getText().trim());
			DbaXml.CDD.lxsource = String.valueOf(structcom.getLxsources().isSelected());
			DbaXml.write(new File(ProjectContext.getSourcePath()+"dba.xml"));
		}catch(Exception exc) {
			JOptionPane.showMessageDialog(this, exc);
		}
		System.exit(0);
	}
	public void windowDeactivated(WindowEvent arg0) {
	}
	public void windowDeiconified(WindowEvent arg0) {
	}
	public void windowIconified(WindowEvent arg0) {
	}
	public void windowOpened(WindowEvent arg0) {
	}
	
	public void display() {
		this.display(null);
	}
	public void display(Component parent) {
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	
	
}
