package org.dba.util;

import java.io.File;
import java.io.Serializable;

public class FileFilter extends javax.swing.filechooser.FileFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	private String extName;	//��չ��
	private String value;
	
	public FileFilter(String extName, String value) {
		super();
		this.extName = extName.toLowerCase();
		this.value = value;
	}

	public boolean accept(File file) {
		boolean ba = true;
		if(!file.isDirectory()) {
			String fileName = file.getName().toLowerCase();
			ba = fileName.endsWith(extName);
		}
		return ba;
	}

	public String getDescription() {
		return value;
	}

	public String getExtName() {
		return extName;
	}

	public void setExtName(String extName) {
		this.extName = extName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
