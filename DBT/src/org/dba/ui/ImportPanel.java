package org.dba.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.dba.util.Util;
import org.dba.util.DbaXml;
import org.dba.util.FileFilter;
import org.dba.util.FrameManager;
import org.dba.util.ProjectContext;
import org.dba.util.SwingUtils;

public class ImportPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = -8257291083617052926L;

	private JTextField TF_admin;
	private JPasswordField PF_admin;
	private JTextField TF_user;
	private JPasswordField PF_user;
	private JTextField TF_sid;
	private JTextField TF_file;
	private JButton B_browse;
	private JButton startImport;

	private String psd;
	private String sid;
	private String file;
	private String admin;
	private String adminpsd;
	private String user;
	private String _sip;
	private JTextField sip;

	public JTextField getTF_admin() {
		return TF_admin;
	}

	public ImportPanel() {
		init();
	}

	public void init() {
		initData();
		initComponent();
		initFace();
		initListener();
	}

	private void initData() {

	}

	private void initComponent() {
		TF_admin = new JTextField(DbaXml.IMP.admin);
		TF_admin.setPreferredSize(new Dimension(250, 21));
		PF_admin = new JPasswordField();
		PF_admin.setPreferredSize(new Dimension(250, 21));
		startImport = new JButton("开始导入");
	}

	private void initListener() {
		B_browse.addActionListener(this);
		B_browse.setActionCommand("B_browse");
		startImport.addActionListener(this);
		startImport.setActionCommand("startImport");
	}

	private void initFace() {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		JPanel north = new JPanel(new GridLayout(2, 1));
		north.setBorder(BorderFactory.createTitledBorder("管理员"));
		north.setPreferredSize(new Dimension(1, 80));
		JPanel north0 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		north0.add(new JLabel("管理员："));
		north0.add(TF_admin);
		JPanel north1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		north1.add(new JLabel("  密码："));
		north1.add(PF_admin);
		north.add(north0);
		north.add(north1);

		JPanel center = new JPanel();
		center.setBorder(BorderFactory.createTitledBorder("导入参数"));
		center.setLayout(null);

		JPanel south = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		south.setPreferredSize(new Dimension(1, 30));
		south.add(startImport);

		this.add(north, "North");
		this.add(center, "Center");

		TF_user = new JTextField(DbaXml.IMP.user);
		TF_user.setBounds(95, 63, 250, 21);
		center.add(TF_user);
		TF_user.setPreferredSize(new Dimension(250, 21));

		JLabel label = new JLabel("\u7528\u6237\u540D\uFF1A");
		label.setBounds(37, 66, 48, 15);
		center.add(label);
		TF_sid = new JTextField(DbaXml.IMP.sid);
		TF_sid.setBounds(95, 129, 250, 21);
		center.add(TF_sid);
		TF_sid.setPreferredSize(new Dimension(250, 21));
		JLabel label_1 = new JLabel("   SID：");
		label_1.setBounds(37, 132, 48, 15);
		center.add(label_1);
		B_browse = new JButton("浏览");
		B_browse.setBounds(295, 160, 57, 23);
		center.add(B_browse);
		TF_file = new JTextField(DbaXml.IMP.file);
		TF_file.setBounds(95, 161, 190, 21);
		center.add(TF_file);
		TF_file.setPreferredSize(new Dimension(190, 21));
		JLabel label_2 = new JLabel("   DMP：");
		label_2.setBounds(37, 164, 48, 15);
		center.add(label_2);
		PF_user = new JPasswordField();
		PF_user.setBounds(95, 98, 250, 21);
		center.add(PF_user);
		PF_user.setPreferredSize(new Dimension(250, 21));
		JLabel label_3 = new JLabel("  密码：");
		label_3.setBounds(37, 97, 54, 15);
		center.add(label_3);

		JLabel label_4 = new JLabel("\u670D\u52A1\u5668\u5730\u5740\uFF1A");
		label_4.setBounds(16, 28, 72, 15);
		center.add(label_4);

		sip = new JTextField();
		sip.setBounds(95, 25, 250, 21);
		center.add(sip);
		sip.setColumns(10);
		this.add(south, "South");
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if (cmd.equals("B_browse")) {
			File file = SwingUtils.selectOpenFile(new File(TF_file.getText()), new FileFilter[] { new FileFilter("dmp", "DMP文件") }, this);
			if (file != null)
				TF_file.setText(file.getPath());
		} else if (cmd.equals("startImport")) {
			try {
				admin = TF_admin.getText().trim();
				adminpsd = String.valueOf(PF_admin.getPassword());
				user = TF_user.getText().trim();
				psd = String.valueOf(PF_user.getPassword());
				sid = TF_sid.getText().trim();
				file = TF_file.getText().trim();
				_sip = sip.getText().trim();
				if (_sip.length() == 0) {
					JOptionPane.showMessageDialog(this, "服务器地址不能为空!");
					return;
				}
				if (admin.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入管理员ID!");
					return;
				}
				if (adminpsd.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入管理员密码!");
					return;
				}
				if (user.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入用户名!");
					return;
				}
				if (psd.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入密码!");
					return;
				}
				if (sid.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入SID!");
					return;
				}
				if (file.length() == 0) {
					JOptionPane.showMessageDialog(this, "请选择DMP文件保存位置!");
					return;
				}

				new ImportInfoPanel(FrameManager.sf).setVisible(true);
				// Thread t = new Thread(new Runnable() {
				// public void run() {
				// try {
				// FrameManager.sf.setEnabled(false);
				// DbaService.executeImport(admin, adminpsd, user, psd, sid,
				// file);
				// JOptionPane.showMessageDialog(FrameManager.sf, "导入成功!");
				// }catch(Exception exp) {
				// JOptionPane.showMessageDialog(FrameManager.sf,
				// "出现错误: "+exp);
				// }finally {
				// FrameManager.sf.setEnabled(true);
				// }
				// }
				// });
				// t.start();
			} catch (Exception exp) {
				JOptionPane.showMessageDialog(this, "出现错误: " + exp);
			}
		}
	}

	public JTextField getTF_user() {
		return TF_user;
	}

	public JTextField getTF_sid() {
		return TF_sid;
	}

	public JTextField getTF_file() {
		return TF_file;
	}

	public String getUser() {
		return user;
	}

	class ImportInfoPanel extends JDialog implements WindowListener {

		/**
         * 
         */
		private static final long serialVersionUID = 6162899017966256858L;

		JTextArea exportinfo = new JTextArea();

		public ImportInfoPanel(SuperFrame fSuper) {
			super(fSuper);
			initGUI();
		}

		private void initGUI() {
			this.setTitle("数据库导入");
			this.setSize(550, 300);
			this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
			Font x = new Font("Serif", 0, 12);
			exportinfo.setFont(x);
			JScrollPane jsp = new JScrollPane(exportinfo);
			getContentPane().add(jsp, "Center");
			this.setLocationRelativeTo(null);
			this.setAlwaysOnTop(true);
			this.addWindowListener((WindowListener) this);
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						FrameManager.sf.setEnabled(false);
						File sqlfile = new File(ProjectContext.getSourcePath() + "user.sql");
						BufferedWriter bw = null;
						try {
							bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(sqlfile)));
							bw.write("drop user " + user + " cascade;\n");
							bw.write("create user " + user + " identified by " + psd + ";\n");
							bw.write("grant dba,connect,resource to " + user + ";\n");
							bw.write("commit;\n");
							bw.write("exit;");
						} finally {
							if (bw != null)
								bw.close();
						}
						boolean isStop = false;
						String usrcmd = ProjectContext.getWinExpPath() + "sqlplus.exe " + admin + "/" + adminpsd + "@" + sip.getText().toString() + "/" + sid + " @" + sqlfile.getCanonicalPath();
						Process process = Runtime.getRuntime().exec(Util.getInstance().getCmd(3,usrcmd));
						InputStreamReader isr = new InputStreamReader(process.getInputStream());
						BufferedReader br = new BufferedReader(isr);
						String line = null;
						
						try {
							while ((line = br.readLine()) != null) {
								if (line.contains("ORA-")) {
									isStop = true;
									exportinfo.append(line + "\n");
									exportinfo.append("程序中止...\n");
									exportinfo.setSelectionStart(exportinfo.getText().length());
									break;
								} else {
									exportinfo.append(line + "\n");
									exportinfo.setSelectionStart(exportinfo.getText().length());
								}
							}
							if(!isStop){
								String impcmd = ProjectContext.getWinExpPath() + "imp " + user + "/" + psd + "@" + sip.getText().toString() + "/" + sid + " file=" + file
										+ " full=y commit=y ignore=n log=" + ProjectContext.getRoot() + "db.log";
								process = Runtime.getRuntime().exec(Util.getInstance().getCmd(3,impcmd));
								InputStreamReader isr1 = new InputStreamReader(process.getErrorStream());
								BufferedReader br1 = new BufferedReader(isr1);
								String lineinfo = null;
								try {
									while ((lineinfo = br1.readLine()) != null) {
										if (!lineinfo.equals("") || lineinfo != null) {
											exportinfo.append(lineinfo + "\n");
											exportinfo.setSelectionStart(exportinfo.getText().length());
										}
									}
								} finally {
									if (isr1 != null)
										isr1.close();
									if (br1 != null)
										br1.close();
								}
							}
							
						} finally {
							if (isr != null)
								isr.close();
							if (br != null)
								br.close();
						}
						if (sqlfile.exists())
							sqlfile.delete();
					} catch (Exception exp) {
						exp.printStackTrace();
						JOptionPane.showMessageDialog(FrameManager.sf, "出现错误: " + exp);
					} finally {
						FrameManager.sf.setEnabled(true);
					}
				}
			});
			t.start();
		}

		@Override
		public void windowOpened(WindowEvent e) {

		}

		@Override
		public void windowClosing(WindowEvent e) {

		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {

		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowActivated(WindowEvent e) {

		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			if (!this.isVisible()) {
				startImport.setEnabled(true);
			}

		}
	}
}
