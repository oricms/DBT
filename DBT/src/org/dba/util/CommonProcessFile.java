package org.dba.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.BuildSQLUnit;
import org.dba.beanunit.DataBaseConfig;
import org.dba.beanunit.DataBaseModel;
import org.dba.dbunit.wf_Db_Connect;
import org.dba.dbunit.wf_Db_Exception;
import org.dba.dbunit.wf_Db_Server;
import org.dba.exception.ProcessException;
import org.dba.service.CoreService;

public abstract class CommonProcessFile {
	
	Log log = LogFactory.getLog(getClass());
	public static String filePath;
	public static String fileEncode ;
	private String[] tempcolumns;
	public String enc = "UTF-8";

	public enum FieldType {
		VARCHAR2, NUMBER, CHAR, NCHAR, NVARCHAR2, DATE, LONG, RAW, BLOB, CLOB, NCLOB, BFILE, INTEGER, FLOAT;
		static FieldType lookup(String filedType) {
			for (FieldType f : FieldType.values()) {
				if (f.toString().equals(filedType))
					return f;
			}
			return null;
		}
	}

	enum OpType {
		ALTER, UPDATE, COMMIT, ADD, // 新增字段
		ADDTABLE, // 新增表
		SET, DROP, // 删除表
		DROPTABLE, // 删除字段
		MODIFY, // 更新字段
		RENAME;
		static OpType lookup(String optype) {
			for (OpType s : OpType.values()) {
				if (s.toString().equalsIgnoreCase(optype)) {
					return s;
				}
			}
			return null;
		}
	}

	// filed@type@length@digits@nullable
	enum FieldOpType {
		fieldOpUpdate, // 字段名更改
		typeOpUpdate, // 类型更改
		lengthOpUpdate, // 长度更改
		digitsOpUpdate, // 位数更改
		nullableOpUpdate, // 是否为空更改
	}

	/**
	 * #project must have following properties: LOG=off TRACE=off
	 * 
	 * #database properties DATABASE_TYPE=oracle
	 * JDBC_DRIVER=oracle.jdbc.OracleDriver
	 * CONNECTION_URL=jdbc:oracle:thin:@127.0.0.1:1521:BZPM LOGIN_ID=BZADMIN
	 * LOGIN_PASSWORD=BZADMIN MIN_CONNECTION=10 MAX_CONNECTION=15
	 * MAX_CONNECT_TIME=60 IDLETIME=2 DB_LOG_FILE=bzserver.log
	 * 
	 * @param dbc
	 */
	public boolean build_config_file(DataBaseConfig dbc, String databseType) {
		OutputStreamWriter os = null;
		String path = ProjectContext.getSourcePath();
		try {
			if (databseType.equals("src")) {
				path = path + "/src.property";
			} else {
				path = path + "/dest.property";
			}
			File file = new File(path);
			if (!file.canRead()) {
				file = new File(file.getAbsolutePath());
			}
			os = new OutputStreamWriter(new FileOutputStream(file), dbc.getCharset());
			try {
				os.write("LOG=off" + System.getProperty("line.separator"));
				os.write("TRACE=off" + System.getProperty("line.separator"));
				os.write("" + System.getProperty("line.separator"));
				os.write("#database properties" + System.getProperty("line.separator"));
				os.write("DATABASE_TYPE=oracle" + System.getProperty("line.separator"));
				os.write("JDBC_DRIVER=oracle.jdbc.OracleDriver" + System.getProperty("line.separator"));
				if (databseType.equals("src")) {
					os.write("CONNECTION_URL=jdbc:oracle:thin:@" + dbc.getSrc_server_ip() + ":1521:" + dbc.getSrc_service() + System.getProperty("line.separator"));
					os.write("LOGIN_ID=" + dbc.getSrc_user() + System.getProperty("line.separator"));
					os.write("LOGIN_PASSWORD=" + dbc.getSrc_pwd() + System.getProperty("line.separator"));
				} else {
					os.write("CONNECTION_URL=jdbc:oracle:thin:@" + dbc.getDest_server_ip() + ":1521:" + dbc.getDest_service() + System.getProperty("line.separator"));
					os.write("LOGIN_ID=" + dbc.getDest_user() + System.getProperty("line.separator"));
					os.write("LOGIN_PASSWORD=" + dbc.getDest_pwd() + System.getProperty("line.separator"));
				}

				os.write("MIN_CONNECTION=10" + System.getProperty("line.separator"));

				os.write("MAX_CONNECTION=15" + System.getProperty("line.separator"));
				os.write("MAX_CONNECT_TIME=60" + System.getProperty("line.separator"));
				os.write("IDLETIME=2" + System.getProperty("line.separator"));
				os.write("DB_LOG_FILE=bzserver.log" + System.getProperty("line.separator"));
				os.flush();
				return true;

			} finally {
				os.close();
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			return false;
		}
	}

	public int loadDataBaseInfo(DataBaseConfig dbc,String database, String table_schem, JProgressBar pbar, JButton strutscompare, JLabel proinfo, int t) {
		// 表的个数
		log.info(database+"="+table_schem+"="+t);
		wf_Db_Connect db_connect = null;
		try {
			db_connect = wf_Db_Server.getInstance().getFactory(database).get();
			DatabaseMetaData data = db_connect.getConnect().getMetaData();
			DataBaseModel dm = new DataBaseModel();

			ResultSet schemas = data.getSchemas();
			ResultSetMetaData metaData = schemas.getMetaData();
			int count = metaData.getColumnCount();
			int a = 0;
			while (schemas.next()) {
				for (int i = 0; i < count; i++) {
					String busschem = schemas.getString("TABLE_SCHEM");
					// 模式
					if (table_schem.equals(busschem)) {

						ResultSet resultSet = data.getTables(null, busschem, "%", new String[] { "TABLE" });
						try {
							while (resultSet.next()) {
								String tName = resultSet.getString("TABLE_NAME");
								proinfo.setText("正在读取:" + tName);
								
								if (tName != null ){
									dm.settName(tName);
									//排除临时索引表和大字段优化表
									if(tName.contains("DR$")){
										continue;
									}
								}
								Statement statement = db_connect.getConnect().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
								try {
									ResultSet set = statement.executeQuery("SELECT * FROM " + tName);
									try {
										int j = set.getMetaData().getColumnCount();
										set.last();
										int row = set.getRow();
										if (row > 0)
											dm.setHasResult(1);
										else
											dm.setHasResult(0);
										// log.info("Result size:" + row);
										tempcolumns = new String[j];
									} finally {
										set.close();
									}
								} finally {
									statement.close();
								}

								/**
								 * 主键
								 */
								ResultSet primaryKeys = data.getPrimaryKeys(null, busschem, tName);
								try {
									// int row = 0;
									String prikey = "";
									while (primaryKeys.next()) {
										// row = primaryKeys.getRow();
										// log.info(ss);
										prikey = primaryKeys.getString("COLUMN_NAME") + "," + prikey;
									}
									// l加入到bean中
									if (prikey != null) {
										String[] _p = prikey.split(",");
										String[] _prikey = new String[_p.length];
										if (_p.length > 0) {
											for (int __p = 0; __p < _p.length; __p++) {
												// log.info(_p[__p]);
												_prikey[__p] = _p[__p];
											}
										}
										dm.setPrimarykey(_prikey);
									}
								} finally {
									primaryKeys.close();
								}

								/**
								 * 触发器
								 */

								ResultSet columns = data.getColumns(null, busschem, tName, "%");
								try {
									while (columns.next()) {
										String columnName = columns.getString("COLUMN_NAME");
										String columnType = columns.getString("TYPE_NAME");
										int datasize = columns.getInt("COLUMN_SIZE");
										int digits = columns.getInt("DECIMAL_DIGITS");
										int nullable = columns.getInt("NULLABLE");
										tempcolumns[a] = columnName + "@" + columnType + "@" + datasize + "@" + digits + "@" + nullable;
										// log.info("a size:" + a);
										a++;
									}
								} finally {
									a = 0;
									columns.close();
								}
								dm.setTableschem(busschem);
								dm.setColumn(tempcolumns);
								buildFile(dbc.getCompareFilePath(), dm, fileEncode, database);
								// }
								t++;
								pbar.setValue(t);
							}
						} finally {
							resultSet.close();
						}
					}
				}
			}
		} catch (wf_Db_Exception e) {
			proinfo.setText(e.getMessage());
			strutscompare.setEnabled(true);
			log.error(e.getMessage());
		} catch (SQLException e) {
			log.error(e.getMessage());
		} finally {
			db_connect.close();
		}
		return t;
	}

	/**
	 * 生成数据文件
	 * 
	 * @param filePath
	 * @param dm
	 * @param enc
	 */
	private void buildFile(String filePath, DataBaseModel dm, String enc, String frefix) {
		String[] _t = dm.getColumn();
		String tableschem = dm.getTableschem();
		String tName = dm.gettName();
		String[] primarykey = dm.getPrimarykey();// 主键
		OutputStreamWriter os = null;
		String _s = "";
		try {
			File file = new File(filePath + (frefix + "_" + dm.getFileName()));
			if (file.exists()) {
				file.delete();
				file = new File(file.getAbsolutePath());
			}
			if (enc == null || enc.length() == 0) {
				os = new OutputStreamWriter(new FileOutputStream(file));
			} else {
				os = new OutputStreamWriter(new FileOutputStream(file), enc);
			}
			try {
				os.write(tableschem + "@" + tName + "@" + dm.getHasResult() + System.getProperty("line.separator"));
				for (int b = 0; b < _t.length; b++) {
					os.write(_t[b] + System.getProperty("line.separator"));
				}

				// 如果没有主键
				for (int i = 0; i < primarykey.length; i++) {
					if (primarykey[i].equals("")) {
						_s = _s + "null@";
					} else {
						_s = _s + primarykey[i] + "@";
					}
				}
				_s += "";

				os.write(_s);
			} finally {
				os.close();
				_s = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将文件内容装入内存
	 * 
	 * @param filePath
	 * @return
	 */
	public abstract List<String> load2Mem(String filePath) throws ProcessException;

	public abstract List<String> load2Mem(File[] filelist) throws ProcessException;
	
	public abstract void buildFile(String filepath) throws ProcessException;

	/**
	 * 将内存当中的两个文件进行比较
	 * 
	 * @param sflist
	 * @param tglist
	 */
	public abstract boolean compareFile(List<String> sflist, List<String> tglist) throws ProcessException;

	public abstract boolean compareFile(String svalue, String opCode) throws ProcessException;

	/**
	 * 
	 * @param optype
	 *            操作命令
	 * @param table_schem
	 *            表模式
	 * @param table_name
	 *            表名
	 * @param svalue
	 *            源始值
	 * @param dvalue
	 *            目标值
	 * @param fot
	 *            字段更改类型
	 * @throws ProcessException
	 */
	public void build2Sql(OpType optype, String table_schem, String table_name, String svalue, String dvalue, FieldOpType fot, String hasResult) throws ProcessException {
		StringBuffer sb = new StringBuffer();
		// filed@type@length@digits@nullable
		BuildSQLUnit bsu = new BuildSQLUnit();
		try {
			if (optype == OpType.lookup("ADD")) {

				String[] _s = svalue.split("@");
				if (_s.length == 5) {
					sb.append("ALTER TABLE \"");
					sb.append(table_schem);
					sb.append("\".\"");
					sb.append(table_name);
					sb.append("\" ADD ( \"");
					sb.append(_s[0]);
					sb.append("\" ");
					if (FieldType.lookup(_s[1]).equals(FieldType.VARCHAR2)) {
						sb.append(_s[1] + " (");
						sb.append(_s[2] + " BYTE) ");
					} else if (FieldType.lookup(_s[1]).equals(FieldType.lookup("NUMBER"))||FieldType.lookup(_s[1]).equals(FieldType.lookup("FLOAT"))||FieldType.lookup(_s[1]).equals(FieldType.lookup("DATE"))||FieldType.lookup(_s[1]).equals(FieldType.lookup("BLOB")) || FieldType.lookup(_s[1]).equals(FieldType.lookup("CLOB"))) {
						sb.append(_s[1] + " ");
					}
					if (_s[4].equals("0")) {
						if (FieldType.lookup(_s[1]).equals(FieldType.NUMBER)) {// 数字
							sb.append(" NOT NULL );");
						} else if (FieldType.lookup(_s[1]).equals(FieldType.VARCHAR2) || FieldType.lookup(_s[1]).equals(FieldType.NCHAR)) {
							sb.append(" NOT NULL );");
						} else if (FieldType.lookup(_s[1]).equals(FieldType.DATE)) {
							sb.append(" NOT NULL );");
						}
					} else {
						sb.append("NULL );");
					}
					String sql = sb.toString();
					bsu.setFlag("INCOMING");
					bsu.setSql(sql);
					CoreService.getInstance().addQueue(bsu);
				}
			} else if (optype == OpType.lookup("MODIFY")) {
				StringBuffer _sb = new StringBuffer();
				if (svalue != null) {
					String[] _s = svalue.split("@");
					if (_s.length == 5) {
						sb.append("ALTER TABLE \"");
						sb.append(table_schem);
						sb.append("\".\"");
						sb.append(table_name);
						sb.append("\" MODIFY (");
						sb.append(" \"" + _s[0] + "\" ");
						if (FieldType.lookup(_s[1]).equals(FieldType.VARCHAR2)||FieldType.lookup(_s[1]).equals(FieldType.lookup("CHAR"))) {
							sb.append(_s[1] + "(");
							sb.append(_s[2] + " BYTE) ");
						} else if (FieldType.lookup(_s[1]).equals(FieldType.lookup("FLOAT"))||FieldType.lookup(_s[1]).equals(FieldType.NUMBER)||FieldType.lookup(_s[1]).equals(FieldType.lookup("BLOB")) ||
								FieldType.lookup(_s[1]).equals(FieldType.lookup("CLOB"))
								||FieldType.lookup(_s[1]).equals(FieldType.lookup("DATE"))) {
							sb.append(_s[1] + " ");
						}

						if (_s[4].equals("0")) {
							// 如果修改的字段不能为空，也有数据时，则需要进行特殊处理
							if (FieldType.lookup(_s[1]).equals(FieldType.NUMBER) || FieldType.lookup(_s[1]).equals(FieldType.FLOAT)) {// 数字
								sb.append("NOT NULL );");
							} else if (FieldType.lookup(_s[1]).equals(FieldType.VARCHAR2) || FieldType.lookup(_s[1]).equals(FieldType.NCHAR)) {
								sb.append("NOT NULL );");
							} else if (FieldType.lookup(_s[1]).equals(FieldType.DATE)||FieldType.lookup(_s[1]).equals(FieldType.CHAR)) {
								sb.append("NOT NULL );");
							}
							BuildSQLUnit _bsu = new BuildSQLUnit();
							_bsu.setFlag("UPDATING");
							_bsu.setSql(_sb.toString());
							CoreService.getInstance().addQueue(_bsu);
						} else {
							sb.append(" NULL );");
						}

						String sql = sb.toString();
						// log.info(sql);
						bsu.setFlag("INCOMING");
						bsu.setSql(sql);
						CoreService.getInstance().addQueue(bsu);
					}
				}
			} else if (optype == OpType.lookup("DROPTABLE")) { // DROP 表

				// DROP table tablename;
				sb.append("DROP TABLE \"");
				sb.append(table_schem);
				sb.append("\".\"");
				sb.append(table_name);
				sb.append("\";");
				String sql = sb.toString();
				// log.info(sql);

				bsu.setFlag("INCOMING");
				bsu.setSql(sql);
				CoreService.getInstance().addQueue(bsu);

			} else if (optype == OpType.lookup("ADDTABLE")) { // 新增表结构
				// DROP table tablename;
				// BZADMIN@ABC@0,PROPTYID@VARCHAR2@50@0@0,PROPTYVALUE@VARCHAR2@300@0@1,APRBILLID@VARCHAR2@120@0@0,PROPTYTYPE@VARCHAR2@50@0@0,[APRTYPE@APRFROM@APRAC@]
				/*
				 * 
				 * CREATE TABLE "BZADMIN"."YYXMXX_CL" ( "PARENTID" VARCHAR2(50
				 * BYTE) NOT NULL , "ID" VARCHAR2(50 BYTE) NOT NULL , "DISTANCE"
				 * NUMBER NOT NULL , "PARENTNAME" VARCHAR2(200 BYTE) NOT NULL ,
				 * "NAME" VARCHAR2(200 BYTE) NOT NULL , CONSTRAINT
				 * "PK_YYXMXX_CL" PRIMARY KEY ("PARENTID", "ID") ) LOGGING
				 * NOCOMPRESS NOCACHE ;
				 */
				if (svalue != null) {
					sb.append("CREATE TABLE \"");
					sb.append(table_schem);
					sb.append("\".\"");
					sb.append(table_name + "\" (\n");
					String[] s = svalue.split(",");
					for (int i = 1; i < s.length - 1; i++) {
						String[] _s = s[i].split("@");
						sb.append("\"" + _s[0] + "\" ");
						if (FieldType.lookup(_s[1]).equals(FieldType.VARCHAR2)) {
							sb.append(_s[1] + "(");
							sb.append(_s[2] + " BYTE) ");
						} else if (FieldType.lookup(_s[1]).equals(FieldType.NUMBER) || FieldType.lookup(_s[1]).equals(FieldType.FLOAT)||FieldType.lookup(_s[1]).equals(FieldType.lookup("DATE"))||FieldType.lookup(_s[1]).equals(FieldType.lookup("BLOB")) || FieldType.lookup(_s[1]).equals(FieldType.lookup("CLOB"))) {
							sb.append(_s[1]);
						}
						// 是否可以为空
						if (_s[4].equals("1")) {
							sb.append(" NULL");
						} else if (_s[4].equals("0")) {
							sb.append(" NOT NULL");
						}
						if (i == (s.length - 2)) {
							sb.append("\n");
						} else {
							sb.append(",\n");
						}
					}

					sb.append(")\n");
					sb.append("LOGGING\n");
					sb.append("NOCOMPRESS\n");
					sb.append("NOCACHE\n");
					sb.append(";\n");

					// 记录 如果需要初始化一些数据，需在此地方进行插入

					// 索引

					// 触发器

					// 主键

					// 检查

					// 主键 ALTER TABLE "BZADMIN"."ACTIVITY_OWNER_SET" ADD PRIMARY
					// KEY

					// ("MPID", "PROCESSID", "MAID", "PERSONID");
					// "BZADMIN"."ACTIVITY_OWNER_SET"")
					// PRIMARY KEY APRTYPE@APRFROM@APRAC@
					// 无主键修正 2013-11-11
					String pk = s[s.length - 1];
					// 去除首尾的控制符
					if (pk.startsWith("@")) {
						pk = pk.substring(1);
					}
					if (pk.endsWith("@")) {
						pk = pk.substring(0, pk.length() - 1);
					}
					if (!pk.equals("null")) {
						sb.append("-- ------------------------------------ -- \n");
						sb.append("-- 生成主键 Primary Key structure for table ");
						sb.append("\"");
						sb.append(table_schem + "\".\"");
						sb.append(table_name + "\"\n");
						sb.append("-- ------------------------------------ -- \n");
						sb.append("ALTER TABLE \"");
						sb.append(table_schem + "\".\"");
						sb.append(table_name + "\"");
						sb.append(" ADD PRIMARY KEY (\"");

						String[] _pk = pk.split("@");
						for (int i = 0; i < _pk.length; i++) {
							if (!_pk[i].equals("")) {
								if ((i + 1) == _pk.length) {
									sb.append(_pk[i] + "\"");
								} else {
									sb.append(_pk[i] + "\",\"");
								}
							}
						}
						sb.append(");\n");
					}
				}
				String sql = sb.toString();
				// log.info(sql);

				bsu.setFlag("INCOMING");
				bsu.setSql(sql);
				CoreService.getInstance().addQueue(bsu);

			}
		} finally {
			// bsu = null;
		}

	}

	public List<File[]> wordkAllocation(String folder, JLabel proinfo, // int
																		// perPNum,
			String fileprefix) {
		List<File[]> lf = new LinkedList<File[]>();
		// 计算总任务 "E:\\oracl"
		File file = new File(folder);
		// int totalwork = 0;
		File[] files = null;
		if (file.isDirectory()) {
			if (fileprefix.equals("src")) {
				FilenameFilter srcfilter = new FilenameFilter() {
					public boolean isSrcprefix(String fileName) {
						if (fileName.startsWith("src"))
							return true;
						return false;
					}

					@Override
					public boolean accept(File dir, String name) {
						return isSrcprefix(name);
					}
				};
				files = file.listFiles(srcfilter);
				// totalwork = files.length;
			} else {
				FilenameFilter srcfilter = new FilenameFilter() {
					public boolean isSrcprefix(String fileName) {
						if (fileName.startsWith("dest"))
							return true;
						return false;
					}

					@Override
					public boolean accept(File dir, String name) {
						return isSrcprefix(name);
					}
				};
				files = file.listFiles(srcfilter);
				// totalwork = files.length;
			}
			lf.add(files);

		}
		// 默认2个线程处理
		// final int perThreadProsize = perPNum;
		// int a = 0;
		// for (; a < totalwork;) {
		// File[] _filesList = new File[perThreadProsize];
		// for (int perprosize = 0; perprosize < perThreadProsize; perprosize++)
		// {
		// _filesList[perprosize] = files[a + perprosize];
		// if ((a + 1) + perprosize == totalwork) {
		// lf.add(_filesList);
		// break;
		// } else {
		// int _tp = perprosize + 1;
		// if (_tp == perThreadProsize) {
		// lf.add(_filesList);
		// continue;
		// }
		// }
		// }
		// a += perThreadProsize;
		// }
		return lf;
	}

	public static Object clone(Object obj) throws Exception {
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		ObjectOutputStream outStream = new ObjectOutputStream(byteOut);
		outStream.writeObject(obj);
		ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
		ObjectInputStream inStream = new ObjectInputStream(byteIn);
		return inStream.readObject();
	}

	public void makefile(LinkedList<BuildSQLUnit> queue) throws ProcessException {

	}
	
}
