package org.dba.ui;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.Color;

import javax.swing.JPasswordField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JTextPane;

import java.awt.SystemColor;
import java.awt.Font;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.DynamicBean;
import org.dba.dbunit.wf_Db_Connect;
import org.dba.dbunit.wf_Db_Server;
import org.dba.util.FileFilter;
import org.dba.util.FrameManager;
import org.dba.util.ProjectContext;
import org.dba.util.SwingUtils;
import org.dba.util.Util;

import javax.swing.JCheckBox;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class InitDataPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6489321939204827602L;
	Log log = LogFactory.getLog(getClass());

	private JTextField dataip;
	private JTextField user;
	private JTextField sid;
	private JTextField soaadd;
	private JTextField dwcode;
	private JPasswordField pwd;
	private JTextField Jtcode;
	private JButton init;
	private final JLabel info;
	private JTextField upwd;
	private JCheckBox security;
	private JCheckBox admin;
	private JCheckBox audit;
	private JCheckBox srole;
	private JCheckBox adrole;
	private JCheckBox arole;
	private JCheckBox exp;
	private static boolean isOK = false;
	private static boolean dataInitisOK = false;

	private static boolean isSuccessExp = false;
	private JTextField TF_file;
	private JButton button;

	private String _dataip;
	private String _user;
	private String _pwd;
	private String _dwcode;
	private String _sid;
	private String _upwd;
	private String _soaadd;
	private String _Jtcode;
	private String tfile;
	private JCheckBox compress;
	private boolean _compress;

	private ExportInfoPanel ep = null;

	public InitDataPanel() {
		setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u6570\u636E\u521D\u59CB\u5316\u53C2\u6570", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 10, 430, 400);
		add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("\u6570\u636E\u5E93\u5730\u5740\uFF1A");
		label.setBounds(10, 23, 72, 15);
		panel.add(label);

		dataip = new JTextField();
		dataip.setText("127.0.0.1");
		dataip.setBounds(77, 20, 120, 21);
		panel.add(dataip);
		dataip.setColumns(10);

		JLabel label_1 = new JLabel("\u7528\u6237\u540D\uFF1A");
		label_1.setBounds(34, 51, 48, 15);
		panel.add(label_1);

		user = new JTextField();
		user.setText("BZADMIN");
		user.setBounds(77, 48, 120, 21);
		panel.add(user);
		user.setColumns(10);

		JLabel label_2 = new JLabel("\u5BC6\u7801\uFF1A");
		label_2.setBounds(44, 79, 36, 15);
		panel.add(label_2);

		JLabel lblSid = new JLabel("SID\uFF1A");
		lblSid.setBounds(52, 104, 30, 15);
		panel.add(lblSid);

		sid = new JTextField();
		sid.setText("BZPM");
		sid.setBounds(77, 101, 120, 21);
		panel.add(sid);
		sid.setColumns(10);

		JLabel lblSoa = new JLabel("SOA\u5730\u5740\uFF1A");
		lblSoa.setForeground(Color.RED);
		lblSoa.setBounds(25, 135, 57, 15);
		panel.add(lblSoa);

		soaadd = new JTextField();
		soaadd.setForeground(Color.RED);
		soaadd.setText("http://10.5.1.35:5888/http/pmisupload");
		soaadd.setBounds(77, 132, 313, 21);
		panel.add(soaadd);
		soaadd.setColumns(10);

		JLabel label_3 = new JLabel("\u5355\u4F4D\u7F16\u53F7\uFF1A");
		label_3.setBounds(232, 29, 60, 15);
		panel.add(label_3);

		dwcode = new JTextField();
		dwcode.setBounds(287, 26, 103, 21);
		panel.add(dwcode);
		dwcode.setColumns(10);

		init = new JButton("\u521D\u59CB\u5316");
		init.setBounds(156, 356, 83, 23);
		panel.add(init);

		pwd = new JPasswordField();
		pwd.setBounds(77, 76, 120, 21);
		panel.add(pwd);

		JLabel label_4 = new JLabel("\u96C6\u56E2\u7F16\u53F7\uFF1A");
		label_4.setForeground(Color.RED);
		label_4.setBounds(232, 54, 60, 15);
		panel.add(label_4);

		Jtcode = new JTextField();
		Jtcode.setForeground(Color.RED);
		Jtcode.setText("0000");
		Jtcode.setBounds(287, 51, 103, 21);
		panel.add(Jtcode);
		Jtcode.setColumns(10);

		JTextPane txtpnsoa = new JTextPane();
		txtpnsoa.setFont(txtpnsoa.getFont().deriveFont(txtpnsoa.getFont().getStyle() | Font.BOLD));
		txtpnsoa.setBackground(SystemColor.menu);
		txtpnsoa.setForeground(Color.RED);
		txtpnsoa.setText("\u672C\u9879\u529F\u80FD\u5C06\u521D\u59CB\u5316\u6210\u5458\u5355\u4F4D\u4E09\u5458\u8D26\u53F7\u548CSOA\u4E2D\u95F4\u4EF6\u670D\u52A1\u914D\u7F6E;\u7EA2\u8272\u914D\u7F6E\u9879\u5728\u975E\u7279\u6B8A\u60C5\u51B5\u4E0B\u8BF7\u4E0D\u8981\u4FEE\u6539");
		txtpnsoa.setBounds(20, 275, 395, 40);
		panel.add(txtpnsoa);

		JLabel label_5 = new JLabel("\u5904\u7406\u8FDB\u5EA6\uFF1A");
		label_5.setBounds(10, 325, 72, 15);
		panel.add(label_5);

		info = new JLabel("..............");
		info.setForeground(Color.RED);
		info.setBounds(66, 326, 324, 15);
		panel.add(info);

		JLabel label_7 = new JLabel("\u9ED8\u8BA4\u5BC6\u7801\uFF1A");
		label_7.setBounds(232, 104, 60, 15);
		panel.add(label_7);

		upwd = new JTextField();
		upwd.setText("123456");
		upwd.setBounds(287, 101, 103, 21);
		panel.add(upwd);
		upwd.setColumns(10);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "\u521D\u59CB\u9009\u9879", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 160, 405, 106);
		panel.add(panel_1);
		panel_1.setLayout(null);

		security = new JCheckBox("\u5B89\u5168\u5458");
		security.setBounds(6, 20, 62, 23);
		panel_1.add(security);
		security.setSelected(true);

		admin = new JCheckBox("\u7BA1\u7406\u5458");
		admin.setBounds(71, 20, 62, 23);
		panel_1.add(admin);
		admin.setSelected(true);

		audit = new JCheckBox("\u5BA1\u8BA1\u5458");
		audit.setBounds(134, 20, 61, 23);
		panel_1.add(audit);
		audit.setSelected(true);

		adrole = new JCheckBox("\u5BA1\u8BA1\u5458\u6743\u9650");
		adrole.setBounds(197, 20, 85, 23);
		panel_1.add(adrole);
		adrole.setSelected(true);

		arole = new JCheckBox("\u7BA1\u7406\u5458\u6743\u9650");
		arole.setBounds(284, 20, 85, 23);
		panel_1.add(arole);
		arole.setSelected(true);

		compress = new JCheckBox("\u6253\u5305\u538B\u7F29");
		compress.setBounds(93, 45, 74, 23);
		panel_1.add(compress);
		compress.setToolTipText("\u5C06\u751F\u6210\u7684dmp\u6587\u4EF6\u8FDB\u884C\u538B\u7F29");
		compress.setSelected(true);

		srole = new JCheckBox("\u5B89\u5168\u5458\u6743\u9650");
		srole.setBounds(6, 45, 85, 23);
		panel_1.add(srole);
		srole.setSelected(true);

		exp = new JCheckBox("\u521D\u59CB\u5B8C\u4E4B\u540E,\u5BFC\u51FA\u6570\u636E\u5E93");
		exp.setSelected(true);
		exp.setBounds(169, 45, 160, 23);
		panel_1.add(exp);

		JLabel label_6 = new JLabel("\u4FDD\u5B58\u8DEF\u5F84\uFF1A");
		label_6.setBounds(10, 74, 60, 15);
		panel_1.add(label_6);

		TF_file = new JTextField("");
		TF_file.setPreferredSize(new Dimension(190, 21));
		TF_file.setBounds(71, 71, 263, 21);
		panel_1.add(TF_file);

		button = new JButton("\u6D4F\u89C8");
		button.setBounds(344, 70, 57, 23);
		panel_1.add(button);

		initListener();
	}

	public void initListener() {
		init.addActionListener(this);
		init.setActionCommand("init");
		button.addActionListener(this);
		button.setActionCommand("B_browse");

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("init")) {

			// init.setEnabled(false);

			/**
			 * 开始进行参数设置
			 */
			_dataip = dataip.getText().trim();
			_user = user.getText().trim();
			_pwd = String.valueOf(pwd.getPassword());
			_dwcode = dwcode.getText().trim();
			_sid = sid.getText().trim();
			_upwd = upwd.getText().trim();
			_soaadd = soaadd.getText().trim();
			_Jtcode = Jtcode.getText().trim();
			tfile = TF_file.getText().trim();
			_compress = compress.isSelected();
			String f = "";

			if (_dataip.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "数据库地址不能为空!");
				init.setEnabled(true);
				return;
			}
			if (_user.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "用户名不能为空!");
				init.setEnabled(true);
				return;
			}
			if (_pwd.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "密码不能为空!");
				init.setEnabled(true);
				return;
			}
			if (_dwcode.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "单位编码不能为空!");
				init.setEnabled(true);
				return;
			}
			if (_sid.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "服务名不能为空!");
				init.setEnabled(true);
				return;
			}
			if (_upwd.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "用户默认密码不能为空!");
				init.setEnabled(true);
				return;
			}
			if (_soaadd.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "SOA地址不能为空!");
				init.setEnabled(true);
				return;
			}
			if (_Jtcode.equals("")) {
				JOptionPane.showMessageDialog(InitDataPanel.this, "集团编号不能为空!");
				init.setEnabled(true);
				return;
			}

			if (exp.isSelected()) {
				f = TF_file.getText().trim();
				if (f.equals("")) {
					init.setEnabled(true);
					info.setText("路径不能为空!");
					return;
				}
			DynamicBean db = new DynamicBean(new String[] { "ip:String=" + _dataip, "port:String=" + "1521", "sid:String=" + _sid, "user:String=" + _user, "pwd:String=" + _pwd });

			/**
			 * 开始初始化三员人员
			 */
			String sec_personid = Util.generate();
			String admin_personid = Util.generate();
			String audit_personid = Util.generate();
			wf_Db_Connect db_connect = null;
			try {
				db_connect = wf_Db_Server.getInstance().getFactory(db).get();
				if (security.isSelected()) {
					Util.getInstance().addUserSet(db_connect, new String[] { sec_personid, _dwcode + "security", _dwcode, _dwcode + "安全员", _upwd }, info);
					if (srole.isSelected()) {
						/**
						 * 312F27E8-8536-4F90-987D-F432096FC293
						 */
						// 获取安全员角色表
						List<String[]> secRoles = Util.getInstance().replacePersonRole(ProjectContext.getSourcePath() + "/PERSON_ROLE(SECURITY).property", sec_personid);
						if (!secRoles.isEmpty()) {
							for (String[] _secRoles : secRoles) {
								Util.getInstance().addPersonRole(db_connect, _secRoles, info);
							}
							Util.getInstance().addPersonOrg(db_connect, sec_personid, _dwcode, info);
						}
					}
				}
				if (admin.isSelected()) {
					Util.getInstance().addUserSet(db_connect, new String[] { admin_personid, _dwcode + "admin", _dwcode, _dwcode + "管理员", _upwd }, info);
					if (arole.isSelected()) {
						List<String[]> adminRoles = Util.getInstance().replacePersonRole(ProjectContext.getSourcePath() + "/PERSON_ROLE(ADMIN).property", admin_personid);
						if (!adminRoles.isEmpty()) {
							for (String[] _adminRoles : adminRoles) {
								Util.getInstance().addPersonRole(db_connect, _adminRoles, info);
							}
							Util.getInstance().addPersonOrg(db_connect, admin_personid, _dwcode, info);
						}
					}
				}
				if (audit.isSelected()) {
					Util.getInstance().addUserSet(db_connect, new String[] { audit_personid, _dwcode + "audit", _dwcode, _dwcode + "审计员", _upwd }, info);
					if (adrole.isSelected()) {
						List<String[]> auditRoles = Util.getInstance().replacePersonRole(ProjectContext.getSourcePath() + "/PERSON_ROLE(AUDIT).property", audit_personid);
						if (!auditRoles.isEmpty()) {
							for (String[] _auditRoles : auditRoles) {
								Util.getInstance().addPersonRole(db_connect, _auditRoles, info);
							}
							Util.getInstance().addPersonOrg(db_connect, audit_personid, _dwcode, info);
						}
					}
				}
				if (audit.isSelected() && security.isSelected() && admin.isSelected()) {
					List<String[]> personStationOrgs = Util.getInstance().replacePersonStationOrg(ProjectContext.getSourcePath() + "/PERSON_STATION_ORG.property", sec_personid, _dwcode);
					if (!personStationOrgs.isEmpty()) {
						for (String[] _personStationOrgs : personStationOrgs) {
							Util.getInstance().addPersonStationOrg(db_connect, _personStationOrgs, info);
						}
					}
				}
				Util.getInstance().deleteEsbDanweiHttp(db_connect, info);
				Util.getInstance().updateEsbDanweiHttp(db_connect, new String[] { _Jtcode, "中国兵器装备集团公司", _soaadd }, info);
				db_connect.Submit();
				dataInitisOK = true;

			} catch (Exception e1) {
				info.setText(e1.getMessage());
				info.setToolTipText(e1.getMessage());
				init.setEnabled(true);
				dataInitisOK = false;
			} finally {
				db_connect.close();
			}
			if (dataInitisOK ) {
					new Thread(new ProcessDmp("数据处理OK",f)).start();
				}
			}
		} else if (e.getActionCommand().equals("B_browse")) {
			File file = SwingUtils.selectSaveFile(new File(TF_file.getText()), new FileFilter[] { new FileFilter("dmp", "DMP文件") }, this);
			if (file != null) {
				TF_file.setText(file.getPath());
			}
		}

	}

	class ProcessDmp implements Runnable {
		String msg = null;
		String f = null;
		public ProcessDmp(String msg,String f) {
			this.msg = msg;
			this.f = f;
		}

		@Override
		public void run() {
			try {
				if (this.msg != null) {
					info.setText(this.msg);
				}
				info.setText("开始导出数据库");
				ep = new ExportInfoPanel(FrameManager.sf);
				ep.setVisible(true);
				while (true) {
					if (isSuccessExp) {
						ep.setVisible(false);
						ep = null;
						new ZipThread(f, f.substring(0, f.indexOf(".")) + ".zip", true, true, info).start();
						break;
					}
					Thread.sleep(500);
				}
				isSuccessExp = false;
				init.setEnabled(true);
				info.setText("..................");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	class ExportInfoPanel extends JDialog implements WindowListener {

		private static final long serialVersionUID = 6162899017966256858L;

		JTextArea exportinfo = new JTextArea();

		public ExportInfoPanel(SuperFrame fSuper) {
			super(fSuper);
			initGUI();
		}

		private void initGUI() {
			this.setTitle("数据库导出");
			this.setSize(550, 300);
			this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
			Font x = new Font("Serif", 0, 12);
			exportinfo.setFont(x);
			JScrollPane jsp = new JScrollPane(exportinfo);
			getContentPane().add(jsp, "Center");
			this.setLocationRelativeTo(null);
			this.setAlwaysOnTop(true);
			this.addWindowListener((WindowListener) this);
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						FrameManager.sf.setEnabled(false);
						String expcmd = ProjectContext.getWinExpPath() + "exp.exe " + _user + "/" + _pwd + "@" + _dataip + "/" + _sid + " file=" + tfile + " buffer=65536 feedback=100000";
						String osName = System.getProperty("os.name");
						String[] cmd = new String[3];

						if (osName.equals("Windows NT")) {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						} else if (osName.equals("Windows XP")) {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						} else if (osName.equals("Windows 95")) {
							cmd[0] = "command.com";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						} else if (osName.equals("Windows 7")) {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						} else {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						}

						Runtime rt = Runtime.getRuntime();
						log.info("Execing " + cmd[0] + " " + cmd[1] + " " + cmd[2]);
						Process proc = rt.exec(cmd);
						InputStreamReader isr = new InputStreamReader(proc.getErrorStream());
						BufferedReader br = new BufferedReader(isr);
						String line = null;
						while ((line = br.readLine()) != null) {
							if (!line.equals("") || line != null) {
								exportinfo.append(line + "\n");
								exportinfo.setSelectionStart(exportinfo.getText().length());
							}
						}
						isSuccessExp = true;
					} catch (Exception exp) {
						JOptionPane.showMessageDialog(FrameManager.sf, "出现错误: " + exp);
					} finally {
						FrameManager.sf.setEnabled(true);
					}
				}
			});
			t.start();
		}

		@Override
		public void windowOpened(WindowEvent e) {

		}

		@Override
		public void windowClosing(WindowEvent e) {

		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {

		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowActivated(WindowEvent e) {

		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			if (!this.isVisible()) {
				init.setEnabled(true);
			}

		}
	}

	class ZipThread extends Thread {
		String fileName = null;
		String zipFileName = null;
		boolean isdelsources = false;
		boolean compress = false;
		private JLabel info = null;

		public ZipThread(String fileName, String zipFileName, boolean isdelsources, boolean compress, JLabel info) {
			this.fileName = fileName;
			this.zipFileName = zipFileName;
			this.isdelsources = isdelsources;
			this.compress = compress;
			this.info = info;
		}

		@Override
		public void run() {
			if (this.compress) {
				Util.getInstance().zip(this.fileName, this.zipFileName, this.info);
				if (this.isdelsources) {
					deleteFile(fileName);
				}
			}
		}

		private boolean deleteFile(String sourcesfile) {
			if (sourcesfile != null) {
				File file = new File(sourcesfile);
				synchronized (file) {
					try {
						if (file.exists()) {
							return file.delete();
						}
					} finally {
						file = null;
					}
				}
			}
			return false;
		}
	}
}
