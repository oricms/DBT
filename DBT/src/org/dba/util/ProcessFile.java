package org.dba.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.BuildSQLUnit;
import org.dba.beanunit.DataBaseConfig;
import org.dba.exception.ProcessException;
import org.dba.exception.ProjectException;
import org.dba.service.CoreService;

/**
 * 
 * 文件通用模块处理
 * 
 */
public class ProcessFile extends CommonProcessFile implements Runnable {

	Log log = LogFactory.getLog(getClass());

	private static String src = "src";
	private static String dest = "dest";

	private JProgressBar pbar;
	private JLabel proinfo;
	private JButton strutscompare;
	private DataBaseConfig dbc;

	private LinkedList<String> srctlist = null;
	private LinkedList<String> desttlist = null;

	public ProcessFile(JButton strutscompare, JProgressBar pbar, JLabel proinfo, DataBaseConfig dbc) {
		this.strutscompare = strutscompare;
		this.pbar = pbar;
		this.proinfo = proinfo;
		this.dbc = dbc;
		srctlist = new LinkedList<String>();
		desttlist = new LinkedList<String>();
		filePath = dbc.getCompareFilePath();
		fileEncode = dbc.getCharset();
	}

	public void run() {
		log.info("对比线程已开启!");
		boolean isOk = false;
		try {
			while (!isOk) {
				isOk = build_config_file(dbc, "src");
				isOk = build_config_file(dbc, "dest");
				Thread.sleep(1000);
				if (isOk == true)
					break;
			}
			this.strutscompare.setEnabled(false);
			this.proinfo.setText("正在读取表结构...");
			this.pbar.setValue(100);
			// 原数据表
			int info = 0;
			if(!this.dbc.isLxs()){
				info = loadDataBaseInfo(this.dbc,src, this.dbc.getSrc_sid(), this.pbar, this.strutscompare, this.proinfo, 100);
			}
			// 目标数据表
			int info2 = loadDataBaseInfo(this.dbc,dest, this.dbc.getDest_sid(), this.pbar, this.strutscompare, this.proinfo, info + 1);
			this.proinfo.setText("已完成对表结构的读取...");
			this.pbar.setValue(info2 + 1);
			// 计算总任务
			File file = new File(dbc.getCompareFilePath());
			int totalwork = 0;
			File[] files = null;
			synchronized (file) {
				while(!file.exists()){
					file.mkdirs();
				}
				if (file.isDirectory()) {
					files = file.listFiles();
					totalwork = files.length;
					log.info(totalwork);
				}
			}
			this.proinfo.setText("正在按配置分捡文件...");
			Thread.sleep(1000);

			// ***************************//

			final List<File[]> src_allocation = wordkAllocation(dbc.getCompareFilePath(), proinfo,
			// 4,
					"src");

			final List<File[]> dest_allocation = wordkAllocation(dbc.getCompareFilePath(), proinfo, // 4,
					"dest");

			this.proinfo.setText("分捡文件完成,正在将文件加入到内存...");
			// System.out.println("Size:" + src_allocation.size());
			Thread.sleep(1000);
			for (int __a = 0; __a < src_allocation.size(); __a++) {
				new RunnableThread(src_allocation.get(__a), "src");
			}

			for (int __a = 0; __a < dest_allocation.size(); __a++) {
				new RunnableThread(dest_allocation.get(__a), "dest");
			}

			this.proinfo.setText("加载成功...");
			Thread.sleep(1000);

			// *************************************
			LinkedList<String> srcdata = getsrcData();
			LinkedList<String> destdata = getdestData();
			HashMap<String, String> srcMap = new HashMap<String, String>();
			HashMap<String, String> destMap = new HashMap<String, String>();
			int src_size = srcdata.size();
			int dest_size = destdata.size();
			String src_key = "", dest_key = "";

			for (int a = 0; a < src_size;) {
				String _temp = "";
				while (true) {
					if (a < src_size) {
						String s1 = srcdata.get(a);
						if (s1 != null && s1.contains("BZADMIN")) {
							src_key = s1;
						}
						_temp = _temp + "," + s1;
						proinfo.setText("src[" + s1 + "]");
						a++;
						if (s1.equals("=end=")) {
							// 截取字符串中第一个逗号和末尾的"=end="
							// key格式 BZADMIN@GSQLJH@1
							String __t = _temp.substring(1, _temp.length() - 5);
							// if(!__t.contains("BZADMIN")){
							// System.out.println(__t);
							// }
							srcMap.put(src_key.substring(0, src_key.length() - 2), __t);
							break;
						}
					}
				}
			}

			for (int a = 0; a < dest_size;) {
				String _temp = "";
				while (true) {
					if (a < dest_size) {
						String s1 = destdata.get(a);
						if (s1 != null && s1.contains("BZADMIN")) {
							dest_key = s1;
						}
						_temp = _temp + "," + s1;
						a++;
						proinfo.setText("dest[" + s1 + "]");
						if (s1.equals("=end=")) {
							// 截取字符串中第一个逗号和末尾的"=end="
							// key格式 BZADMIN@GSQLJH
							String __t = _temp.substring(1, _temp.length() - 5);
							destMap.put(dest_key.substring(0, dest_key.length() - 2), __t);
							break;
						}
					}
				}
			}
			map2list(srcMap, destMap);
			// *************************************************************************
			this.strutscompare.setEnabled(true);
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	class RunnableThread {

		public RunnableThread(final File[] f, final String fileprefix) {

			new Thread(new Runnable() {
				public void run() {
					List<String> _t = load2Mem(f);
					// int a = 0;
					for (String __t : _t) {
						if (__t != null) {
							if (fileprefix.equals("src")) {
								// if (__t.contains("BZADMIN")) {
								// a++;
								// System.out.println("==" + __t);
								// }
								add2srcList(__t);
							} else if (fileprefix.equals("dest")) {
								// if (__t.contains("BZADMIN")) {
								// a++;
								// System.out.println("==" + __t);
								// }
								add2destList(__t);
							}
						}
					}
					// System.out.println(a);
				}
			}).start();
		}
	}

	/**
	 * 
	 * @param srcmap
	 *            源库结构缓存
	 * @param destmap
	 *            目标库结构缓存
	 * @throws Exception
	 */
	private void map2list(HashMap<String, String> srcmap, HashMap<String, String> destmap) throws Exception {
		if (srcmap.size() > 0 && destmap.size() > 0) {
			/**
			 * 拷贝map
			 */
			HashMap<String, String> _srcmap = (HashMap<String, String>) this.clone(srcmap);
			HashMap<String, String> _destmap = (HashMap<String, String>) this.clone(destmap);
			/**
			 * 清除不存在的数据表
			 */
				Iterator<Entry<String, String>> it1 = _destmap.entrySet().iterator();
				Entry<String, String> e1;
				while (it1.hasNext()) {
					e1 = it1.next();
					String _destKey = e1.getKey();
					String _destValue = e1.getValue();
					if (!_srcmap.containsKey(_destKey)) {
						// 可以drop的数据表
						compareFile(_destValue,"DROPTABLE");
						log.info("可删除的表:"+_destKey);
					}

				}

			synchronized (srcmap) {
				Iterator<Entry<String, String>> it = srcmap.entrySet().iterator();
				Entry<String, String> entity;
				List<String> srclist = null;
				List<String> destlist = null;
				while (it.hasNext()) {
					srclist = new ArrayList<String>();
					destlist = new ArrayList<String>();
					entity = it.next();
					/*
					 * 根据srcmap中的key.在destmap中找出对 应的key 因为两个map中的key是一样的
					 * 如果不一样，说明有表增加
					 */
					String srcKey = entity.getKey();
					String value = entity.getValue();
					/**
					 * 目的库中是否包含源表中
					 */
					if (destmap.containsKey(srcKey)) {
						/**
						 * 表结构有可能变化
						 */
						synchronized (srclist) {
							String[] _s = value.split(",");
							if (!value.contains("BZADMIN")) {
								System.out.println(value);
							}
							for (String __s : _s) {
								srclist.add(__s);
							}
						}
						synchronized (destlist) {
							String dest_value = destmap.get(srcKey);
							String[] _s = dest_value.split(",");
							for (String __s : _s) {
								destlist.add(__s);
							}
						}
						boolean isOk = compareFile(srclist, destlist);
						if (isOk) {
							// 在循环外面删除成功的表
							synchronized (srcmap) {
								it.remove();
								srcmap.remove(srcKey);
							}
							synchronized (destmap) {
								destmap.remove(srcKey);
							}
							// 清除表缓存
							srclist = null;
							destlist = null;
						}
					} else {
						/**
						 * 有新增的数据表
						 */
						try {
							//log.info("新增的数据表:"+value);
							compareFile(value, "ADDTABLE");
						} finally {
						}

					}
				}

			}
			/**
			 * 发送结束信号
			 */
			BuildSQLUnit bsu =  null;
			try{
			bsu = new BuildSQLUnit();
			bsu.setSql("NULL");
			bsu.setFlag("--END");
			CoreService.getInstance().addQueue(bsu);
			}finally{
				bsu = null;
			}
		}

	}

	@Override
	public List<String> load2Mem(File[] filelist) throws ProcessException {
		List<String> _tempList = new ArrayList<String>();
		BufferedReader filebuffer = null;
		FileInputStream fis = null;
		File[] files = filelist;
		for (File file : files) {
			if (file != null) {
				if (file.canRead()) {
					try {
						try {
							fis = new FileInputStream(file);
							filebuffer = new BufferedReader(new InputStreamReader(fis, fileEncode));
							String _t = "";
							do {
								_t = filebuffer.readLine();
								_tempList.add(_t);
							} while (_t != null);
							_tempList.add("=end=");
						} finally {
							filebuffer.close();
							fis.close();
						}
					} catch (FileNotFoundException e) {
						throw new ProcessException("未找到文件!" + e.getMessage());
					} catch (UnsupportedEncodingException e) {
						throw new ProcessException("不支持的文件编码!" + e.getMessage());
					} catch (IOException e) {
						throw new ProcessException("文件的IO异常!" + e.getMessage());
					}
				} else {
					throw new ProjectException("文件不能被读,请检查文件读写权限!");
				}
			}
		}
		return _tempList;
	}

	public List<String> load2Mem(File[] filelist, String prefix) throws ProcessException {
		List<String> _tempList = new ArrayList<String>();
		int inNum = filelist.length;
		int row = 0;
		File[] files = filelist;
		for (File file : files) {
			if (file != null) {
				if (file.canRead()) {
					if (file.getName().startsWith(prefix)) {
						try {
							BufferedReader filebuffer = null;
							FileInputStream fis = null;
							BufferedWriter File_bak = null;
							try {
								fis = new FileInputStream(file);
								filebuffer = new BufferedReader(new InputStreamReader(fis, fileEncode));
								String _t = "";
								do {
									_t = filebuffer.readLine();
									_tempList.add(_t);
								} while (_t != null);

								String _temp = new String();
								synchronized (_tempList) {
									// 在内存中对第一行字符内容进行替换
									if (_tempList.size() > 1) {
										String __temp = _tempList.get(0);
										if (__temp != null) {
											if (!__temp.contains("[1]")) {
												try {
													// BZADMIN@BUREAUSET@1[isprocess]
													// 标记当前被读入到内存中的数据文件
													File_bak = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), fileEncode));
													_tempList.add(0, __temp + "[1]");
													_tempList.remove(1);
													for (int i = 0; i < _tempList.size() - 1; i++) {
														_temp = _tempList.get(i);
														// 替换第一行
														File_bak.write(_temp + System.getProperty("line.separator"));
													}
													// 必须先刷新,才能用close关闭
													File_bak.flush();
												} finally {
													File_bak.close();
												}
											}
											_tempList.clear();
										}
									}
								}
							} finally {
								filebuffer.close();
								fis.close();
							}
						} catch (FileNotFoundException e) {
							throw new ProcessException("未找到文件!" + e.getMessage());
						} catch (UnsupportedEncodingException e) {
							throw new ProcessException("不支持的文件编码!" + e.getMessage());
						} catch (IOException e) {
							throw new ProcessException("文件的IO异常!" + e.getMessage());
						}
					}
				} else {
					throw new ProjectException("文件不能被读,请检查文件读写权限!");
				}
			}
			row++;
			if (row == inNum)
				break;
		}
		log.info("row = " + row);
		return _tempList;
	}

	public List<String> load2Mem(String folder, String prefix, int inNum) throws ProcessException {
		List<String> _tempList = new ArrayList<String>();
		File _f = new File(folder);
		int row = 0;
		if (_f != null) {
			File[] files = _f.listFiles();
			for (File file : files) {
				if (file.canRead()) {
					if (file.getName().startsWith(prefix)) {
						try {
							BufferedReader filebuffer = null;
							FileInputStream fis = null;
							BufferedWriter File_bak = null;
							try {
								fis = new FileInputStream(file);
								filebuffer = new BufferedReader(new InputStreamReader(fis, fileEncode));
								String _t = "";
								do {
									_t = filebuffer.readLine();
									_tempList.add(_t);
								} while (_t != null);

								String _temp = new String();
								synchronized (_tempList) {
									// 在内存中对第一行字符内容进行替换
									if (_tempList.size() > 1) {
										String __temp = _tempList.get(0);
										if (__temp != null) {
											if (!__temp.contains("[1]")) {
												try {
													// BZADMIN@BUREAUSET@1[isprocess]
													// 标记当前被读入到内存中的数据文件
													File_bak = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), fileEncode));
													_tempList.add(0, __temp + "[1]");
													_tempList.remove(1);
													for (int i = 0; i < _tempList.size() - 1; i++) {
														_temp = _tempList.get(i);
														// 替换第一行
														File_bak.write(_temp + System.getProperty("line.separator"));
													}
													// 必须先刷新,才能用close关闭
													File_bak.flush();
												} finally {
													File_bak.close();
												}
											}
											_tempList.clear();
										}
									}
								}
							} finally {
								filebuffer.close();
								fis.close();
							}
						} catch (FileNotFoundException e) {
							throw new ProcessException("未找到文件!" + e.getMessage());
						} catch (UnsupportedEncodingException e) {
							throw new ProcessException("不支持的文件编码!" + e.getMessage());
						} catch (IOException e) {
							throw new ProcessException("文件的IO异常!" + e.getMessage());
						}
					}
				} else {
					throw new ProjectException("文件不能被读,请检查文件读写权限!");
				}
				row++;
				if (row == inNum)
					break;
			}
		}
		log.info("row = " + row);
		return _tempList;
	}

	@Override
	public boolean compareFile(String svalue, String opCode) throws ProcessException {
		String table_schem = "";
		String table_name = "";
		// BZADMIN@ABC@0,PROPTYID@VARCHAR2@50@0@0,PROPTYVALUE@VARCHAR2@300@0@1,APRBILLID@VARCHAR2@120@0@0,PROPTYTYPE@VARCHAR2@50@0@0,
		if (svalue != null) {
			String[] _s = svalue.split(",");
			if (_s.length > 0) {
				String[] __s = _s[0].split("@");
				if (__s.length > 2) {
					table_schem = __s[0];
					table_name = __s[1];
				}
			} else {
				// for (int a = 0; a < list.size(); a++)
				// System.out.println(list.get(a));
			}
		}
		build2Sql(OpType.lookup(opCode), table_schem, table_name, svalue, null, null,null);
		return true;
	}

	@Override
	public boolean compareFile(List<String> sflist, List<String> tglist) throws ProcessException {
		/*
		 * 以sflist为标准来比较tglist filed@type@length@digits@nullable
		 */
		String table_schem = "";
		String table_name = "";
		String hasResult = "";

		HashMap<String, String> s = new HashMap<String, String>();
		HashMap<String, String> d = new HashMap<String, String>();
		try {
			if (sflist.size() > 0) {
				String tinfo = sflist.get(0);
				if (tinfo != null) {
					String[] _s = tinfo.split("@");
					if (_s.length == 3) {
						table_schem = _s[0];
						table_name = _s[1];
						hasResult = _s[2];
					} else {
						for (int a = 0; a < sflist.size(); a++)
							System.out.println(sflist.get(a));
					}
					if (hasResult.equals("1")) {
						// log.info("有数据!");
					}
				}
				//log.info("tgList.[0]value:"+tglist.get(0));
				sflist.remove(0);
				tglist.remove(0);
				int s_size = sflist.size();
				int t_size = tglist.size();
				sflist.remove(s_size-1);
				tglist.remove(t_size-1);
				
				for (String _sf : sflist) {
					if (_sf != null) {
						if (!tglist.contains(_sf)) {
							String[] __sf = _sf.split("@");
							s.put(__sf[0], _sf);
						}
					}
				}

				for (String _tg : tglist) {
					String[] __tg = _tg.split("@");
					d.put(__tg[0], _tg);
				}
				// 针对不存在的数据进行进一步的细分
				Iterator<Entry<String, String>> it = s.entrySet().iterator();
				while (it.hasNext()) {
					Entry<String, String> next = it.next();
					String key = next.getKey();
					String __svalue = next.getValue();
					String __dvalue = d.get(key);
					if (__dvalue == null) {
						build2Sql(OpType.lookup("ADD"), table_schem, table_name, __svalue, __dvalue, FieldOpType.fieldOpUpdate,hasResult);
						// log.info(" src:filed = " + __sf[0]);
					} else {
						// filed@type@length@digits@nullable
						String[] __sf = __svalue.split("@");
						String[] __tg = __dvalue.split("@");
						if (!__tg[1].equals(__sf[1])) {
							// 类型变化
							build2Sql(OpType.lookup("MODIFY"), table_schem, table_name, __svalue, __dvalue, FieldOpType.typeOpUpdate,hasResult);
							// log.info(__sf[0] + " target:type = " + __tg[1] +
							// " src:type = " + __sf[1]);
						}
						if (!__tg[2].equals(__sf[2])) {
							build2Sql(OpType.lookup("MODIFY"), table_schem, table_name, __svalue, __dvalue, FieldOpType.lengthOpUpdate,hasResult);
							// 长度变化
							// log.info(__sf[0] + " target:length = " + __tg[2]
							// +
							// " src:length = " + __sf[2]);
						}
						if (!__tg[3].equals(__sf[3])) {
							build2Sql(OpType.lookup("MODIFY"), table_schem, table_name, __svalue, __dvalue, FieldOpType.digitsOpUpdate,hasResult);
							// 位数变化
							// log.info(__sf[0] + " target:digits = " + __tg[3]
							// +
							// " src:digits = " + __sf[3]);
						}
						if (!__tg[4].equals(__sf[4])) {
							build2Sql(OpType.lookup("MODIFY"), table_schem, table_name, __svalue, __dvalue, FieldOpType.nullableOpUpdate,hasResult);
							// 是否为空变化
							// log.info(__sf[0] + " target:nullable = " +
							// __tg[4] +
							// " src:nullable = " + __sf[4]);
						}
					}
				}

			}
		} catch (Exception e) {
			//for(String _s:tglist){
				//System.out.println(_s);
			//}
			e.printStackTrace();
		}
		return true;
	}

	private void add2srcList(String data) {
		synchronized (srctlist) {
			if (data != null) {
				srctlist.addLast(data);
			}
		}
	}

	private LinkedList<String> getsrcData() {
		synchronized (srctlist) {
			if (srctlist != null) {
				return srctlist;
			}
		}
		return null;
	}

	private void add2destList(String data) {
		synchronized (desttlist) {
			if (data != null) {
				desttlist.addLast(data);
			}
		}
	}

	private LinkedList<String> getdestData() {
		synchronized (desttlist) {
			if (desttlist != null) {
				return desttlist;
			}
		}
		return null;
	}

	@Override
	public List<String> load2Mem(String filePath) throws ProcessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void buildFile(String filepath) throws ProcessException {
		// TODO Auto-generated method stub
		
	}

}
