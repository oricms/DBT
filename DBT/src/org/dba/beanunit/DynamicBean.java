package org.dba.beanunit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.beans.BeanMap;

public class DynamicBean implements Cloneable{
	
	
	private Object object = null;
	private HashMap<Object, Object> var = null;
	private static BeanMap beanMap = null;

	public DynamicBean(Map propertyMap) {
		this.object = generateBean(propertyMap);
		beanMap = BeanMap.create(this.object);
	}
	

	/**
	 * pro = String[]{"id:Integer=1","name:String=2"}
	 * @param pro
	 */
	public DynamicBean(String[] pro) {
		var = new HashMap<Object, Object>();
		generateBean(pro);
	}

	public void setValue(String property, Object value) {
		beanMap.put(property, value);
	}

	public Object getValue(String property) {
		return beanMap.get(property);
	}

	public Class getPropertyType(String pro) {
		return beanMap.getPropertyType(pro);
	}

	public Object getObject() {
		return this.object;
	}

	@SuppressWarnings("unchecked")
	private Object generateBean(Map propertyMap) {
		BeanGenerator generator = new BeanGenerator();
		Set keySet = propertyMap.keySet();
		for (Iterator i = keySet.iterator(); i.hasNext();) {
			String key = (String) i.next();
			generator.addProperty(key, (Class) propertyMap.get(key));
		}
		return generator.create();
	}

	/**
	 * pro = String[]{"id:Integer=1","name:String=2"}
	 * 
	 * @param pro
	 * @return
	 */
	private Object generateBean(String[] pro) {
		BeanGenerator generator = new BeanGenerator();
		try {
			if (pro.length > 0) {
				for (String s : pro) {
					if (s != null) {
						String[] _s = s.split(":");
						if (_s.length == 2) {
							if (_s[1] != null) {
								String[] __s = _s[1].split("=");
								generator.addProperty(_s[0], Class.forName("java.lang." + __s[0].trim()));
								var.put(_s[0],_s[1]);
							}

						}
					}
				}
				beanMap = BeanMap.create(generator.create());
				try{
				// ���и�ֵ
					if (var != null) {
						Iterator<Entry<Object, Object>> it = var.entrySet().iterator();
						while (it.hasNext()) {
							Entry<Object, Object> next = it.next();
							Object key = next.getKey();
							Object value = next.getValue();
							//String=2
							if(value != null){
								String[] v = value.toString().split("=");
								Class<?> c = Class.forName("java.lang."+v[0].trim());
								Object r = null;
								Method method = null;
								if(v[0].equals("String")){
									method = c.getMethod("valueOf",Object.class);
									r = method.invoke(String.class, v[1].trim());
								}else if(v[0].equals("Integer")){
									method = c.getMethod("parseInt",String.class);
									r = method.invoke(Integer.class, v[1].trim());
								}else if(v[0].equals("Boolean")){
									method = c.getMethod("valueOf",String.class);
									r = method.invoke(Boolean.class, v[1].trim());
								}
								beanMap.put(key, r);
							}
						}
					}
				} finally {
					var = null;
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} 
		return generator.create();
	}

	private void ValidateType(String type) {
		if (type != null) {

		}
	}

	private void validateVariableType(String variable) {
		if (variable != null) {

		}
	}


	public Object clone() {  
		DynamicBean db = null;  
        try{  
        	db = (DynamicBean)super.clone();  
        }catch(CloneNotSupportedException e) {  
            e.printStackTrace();  
        }  
        return db;  
    } 
	
}
