package org.dba.dbunit;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;

public class wf_Db_Exception extends Exception {
	private static final long serialVersionUID = 1L;
	private Exception ex = null;
	private boolean ignoreError = false;

	public wf_Db_Exception() {
	}

	public wf_Db_Exception(Exception ex) {
		super(ex.getMessage());
		this.ex = ex;
	}

	public wf_Db_Exception(String message) {
		super(message);
	}

	public wf_Db_Exception(boolean ignoreError) {
		this.ignoreError = ignoreError;
	}

	public wf_Db_Exception(Exception ex, boolean ignoreError) {
		super(ex.getMessage());
		this.ex = ex;
		this.ignoreError = ignoreError;
	}

	public wf_Db_Exception(String message, boolean ignoreError) {
		super(message);
		this.ignoreError = ignoreError;
	}

	public boolean isIgnoreError() {
		return this.ignoreError;
	}

	public void setIgnoreError(boolean ignoreError) {
		this.ignoreError = ignoreError;
	}

	public String getMessage() {
		if (super.getMessage() != null)
			return super.getMessage();
		if (this.ex != null) {
			if ((this.ex instanceof InvocationTargetException)) {
				if ((((InvocationTargetException) this.ex).getTargetException() != null) && (((InvocationTargetException) this.ex).getMessage() != null)) {
					return ((InvocationTargetException) this.ex).getMessage();
				}

			} else if (this.ex.getMessage() != null) {
				return this.ex.getMessage();
			}
		}

		if ((getCause() != null) && (getCause().getMessage() != null)) {
			return getCause().getMessage();
		}
		return "";
	}

	public void printStackTrace() {
		super.printStackTrace();
		if (this.ex != null)
			this.ex.printStackTrace();
	}

	public void printStackTrace(PrintStream s) {
		super.printStackTrace(s);
		if (this.ex != null)
			this.ex.printStackTrace(s);
	}

	public void printStackTrace(PrintWriter s) {
		super.printStackTrace(s);
		if (this.ex != null)
			this.ex.printStackTrace(s);
	}
}