package org.dba.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.ScrollPaneLayout;
import javax.swing.SpringLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.ExportBeanUnit;
import org.dba.beanunit.IconNode;
import org.dba.dbunit.wf_Db_Connect;
import org.dba.dbunit.wf_Db_Exception;
import org.dba.dbunit.wf_Db_Server;
import org.dba.util.Util;
import org.dba.util.DbaXml;
import org.dba.util.FileFilter;
import org.dba.util.FrameManager;
import org.dba.util.ProjectContext;
import org.dba.util.SwingUtils;

import java.awt.Color;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

public class ExportPanel extends JPanel implements ActionListener {

	Log log = LogFactory.getLog(getClass());

	private static final long serialVersionUID = -8257291083617052926L;

	private JTextField TF_user;
	private JPasswordField PF_user;
	private JTextField TF_sid;
	private JTextField TF_file;

	private JButton B_browse;
	private JButton startExport;

	private String user;
	private String psd;
	private String sid;
	private String file;
	private String sip;
	private String _instanceName;
	private JTextField serverip;

	private JButton logbutton;
	private JRadioButton fullmodel;
	private JCheckBox iszl;
	private JCheckBox islog;

	private String[] options;
	private JTextField Tlog;
	private JCheckBox indexes;
	private JCheckBox compress;
	private JRadioButton usermodel;
//	private JRadioButton custExp;
	private ExportBeanUnit ebu = null;
	private JLabel proinfo;
	private JLabel tableSize;
	private JTextField instanceName;

	public JTextField getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(JTextField instanceName) {
		this.instanceName = instanceName;
	}

	public ExportPanel() {
		init();
	}

	public void init() {
		initData();
		initComponent();
		initFace();
		initListener();
	}

	private void initData() {
		ebu = new ExportBeanUnit();
		ebu.setUser(DbaXml.EXP.user);
		ebu.setFile(DbaXml.EXP.file);
		ebu.setPsd(DbaXml.EXP.pwd);
		ebu.setSid(DbaXml.EXP.sid);
		ebu.setSip(DbaXml.EXP.sip);
		ebu.setInstanceName(DbaXml.EXP.instanceName);
	}

	private void initComponent() {
		startExport = new JButton("开始导出");
		startExport.setBounds(168, 0, 81, 23);
		startExport.setVerticalAlignment(SwingConstants.TOP);
	}

	private void initListener() {
		B_browse.addActionListener(this);
		B_browse.setActionCommand("B_browse");
		logbutton.addActionListener(this);
		logbutton.setActionCommand("log");
		startExport.addActionListener(this);
		startExport.setActionCommand("startExport");
	}

	private void initFace() {
		JPanel south = new JPanel();
		south.setPreferredSize(new Dimension(1, 30));
		south.setLayout(null);
		south.add(startExport);
		iszl = new JCheckBox("\u542F\u7528\u589E\u91CF");
		iszl.setSelected(Boolean.valueOf(DbaXml.EXP.zl));

		islog = new JCheckBox("\u65E5\u5FD7");
		islog.setSelected(Boolean.valueOf(DbaXml.EXP.islog));

		usermodel = new JRadioButton("\u7528\u6237\u6A21\u5F0F");
		usermodel.setSelected(Boolean.valueOf(DbaXml.EXP.usermodel));

		fullmodel = new JRadioButton("\u5B8C\u5168\u6A21\u5F0F");
		fullmodel.setBounds(228, 22, 73, 23);
		fullmodel.setSelected(Boolean.valueOf(DbaXml.EXP.fullmodel));

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u9009\u9879", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "\u670D\u52A1\u5668\u914D\u7F6E", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addGroup(
								groupLayout.createParallelGroup(Alignment.TRAILING).addComponent(panel_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup().addGap(4).addComponent(south, GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE))
										.addGroup(groupLayout.createSequentialGroup().addGap(4).addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout.createSequentialGroup().addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(south, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(94)));
		panel_1.setLayout(null);

		JLabel label_2 = new JLabel("\u670D\u52A1\u5668\u5730\u5740\uFF1A");
		label_2.setBounds(10, 24, 72, 15);
		panel_1.add(label_2);

		serverip = new JTextField(DbaXml.EXP.sip);
		serverip.setBounds(81, 21, 157, 21);
		panel_1.add(serverip);
		serverip.setPreferredSize(new Dimension(200, 21));
		JLabel label = new JLabel("用户名：");
		label.setBounds(34, 49, 48, 15);
		panel_1.add(label);
		TF_user = new JTextField(DbaXml.EXP.user);
		TF_user.setBounds(81, 49, 157, 21);
		panel_1.add(TF_user);
		TF_user.setPreferredSize(new Dimension(100, 21));
		JLabel label_1 = new JLabel(" \u5BC6   \u7801\uFF1A");
		label_1.setBounds(22, 74, 60, 21);
		panel_1.add(label_1);
		PF_user = new JPasswordField(DbaXml.EXP.pwd);
		PF_user.setBounds(81, 74, 157, 21);
		panel_1.add(PF_user);
		PF_user.setPreferredSize(new Dimension(100, 21));
		JLabel label_4 = new JLabel("\u670D\u52A1\u540D\uFF1A");
		label_4.setBounds(34, 108, 48, 15);
		panel_1.add(label_4);
		TF_sid = new JTextField(DbaXml.EXP.sid);
		TF_sid.setBounds(81, 105, 157, 21);
		panel_1.add(TF_sid);
		TF_sid.setPreferredSize(new Dimension(250, 21));
		B_browse = new JButton("浏览");
		B_browse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		B_browse.setBounds(353, 167, 57, 23);
		panel_1.add(B_browse);
		JLabel label_3 = new JLabel("\u4FDD\u5B58\u8DEF\u5F84\uFF1A");
		label_3.setBounds(22, 171, 60, 15);
		panel_1.add(label_3);
		TF_file = new JTextField(DbaXml.EXP.file);
		TF_file.setBounds(81, 168, 263, 21);
		panel_1.add(TF_file);
		TF_file.setPreferredSize(new Dimension(190, 21));

		JLabel lblOracle = new JLabel("Oracle\u6570\u636E\u5E93\u5B9E\u4F8B\u670D\u52A1\u540D");
		lblOracle.setBounds(248, 108, 186, 15);
		panel_1.add(lblOracle);

		JLabel label_5 = new JLabel("\u670D\u52A1\u5668\u5730\u5740\u5982\u679C\u4E0D\u586B\uFF0C\u5219\u9ED8\u8BA4\u4E3A\u672C\u673A");
		label_5.setToolTipText("\u670D\u52A1\u5668\u5730\u5740\u5982\u679C\u4E0D\u586B\uFF0C\u5219\u9ED8\u8BA4\u4E3A\u672C\u673A");
		label_5.setBounds(248, 24, 186, 21);
		panel_1.add(label_5);

		final JLabel loglable = new JLabel("\u65E5\u5FD7\u8DEF\u5F84\uFF1A");
		loglable.setBounds(22, 204, 60, 15);
		if (!islog.isSelected()) {
			loglable.setEnabled(false);
		} else {
			loglable.setEnabled(true);
		}
		panel_1.add(loglable);

		Tlog = new JTextField(DbaXml.EXP.logfile);
		Tlog.setBounds(81, 201, 263, 21);
		if (!islog.isSelected()) {
			Tlog.setEnabled(false);
		} else {
			Tlog.setEnabled(true);
		}
		panel_1.add(Tlog);

		logbutton = new JButton("\u6D4F\u89C8");
		logbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		logbutton.setActionCommand("B_browse");
		logbutton.setBounds(353, 200, 57, 23);
		if (!islog.isSelected()) {
			logbutton.setEnabled(false);
		}
		panel_1.add(logbutton);

		JLabel label_6 = new JLabel("\u5B9E\u4F8B\u540D\uFF1A");
		label_6.setBounds(34, 133, 48, 28);
		panel_1.add(label_6);

		instanceName = new JTextField(DbaXml.EXP.instanceName);
		instanceName.setBounds(81, 137, 157, 21);
		panel_1.add(instanceName);
		instanceName.setColumns(10);
		panel.setLayout(null);

		compress = new JCheckBox("\u538B\u7F29");
		compress.setBounds(6, 22, 49, 23);
		compress.setSelected(Boolean.valueOf(DbaXml.EXP.compress));
		panel.add(compress);

		indexes = new JCheckBox("\u7D22\u5F15");
		indexes.setBounds(57, 22, 49, 23);
		indexes.setSelected(Boolean.valueOf(DbaXml.EXP.indexes));
		panel.add(indexes);

		if (islog.isSelected()) {
			Tlog.setEnabled(true);
		} else {
			Tlog.setEnabled(false);
		}
		islog.setBounds(102, 22, 49, 23);
		islog.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (islog.isSelected()) {
					loglable.setEnabled(true);
					Tlog.setEnabled(true);
					logbutton.setEnabled(true);
				} else {
					loglable.setEnabled(false);
					Tlog.setEnabled(false);
					logbutton.setEnabled(false);
				}
			}

		});
		panel.add(islog);

		if (!usermodel.isSelected()) {
			fullmodel.setEnabled(true);
			fullmodel.setSelected(true);
			iszl.setEnabled(true);
			iszl.setSelected(false);
		} else {
			fullmodel.setEnabled(false);
			fullmodel.setSelected(false);
			iszl.setEnabled(false);
			iszl.setSelected(false);
		}
		usermodel.setBounds(153, 22, 73, 23);
		usermodel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (usermodel.isSelected()) {
					fullmodel.setEnabled(false);
					iszl.setEnabled(false);
				} else {
					fullmodel.setEnabled(true);
					iszl.setEnabled(true);
				}
			}
		});

		panel.add(usermodel);

		if (!fullmodel.isSelected()) {
			usermodel.setEnabled(true);
			usermodel.setSelected(true);
		} else {
			usermodel.setEnabled(false);
			usermodel.setSelected(false);
		}

		fullmodel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (fullmodel.isSelected()) {
					usermodel.setEnabled(false);
					//custExp.setEnabled(false);
				} else {
					usermodel.setEnabled(true);
					//custExp.setEnabled(true);
				}
			}
		});
		panel.add(fullmodel);

		iszl.setForeground(Color.RED);
		iszl.setToolTipText("\u6CE8\u610F\uFF1A\u4EC5\u9650\u4E8E\u6570\u636E\u5E93\u7B2C\u4E00\u6B21\u4F7F\u7528\u4E86\u5B8C\u5168\u6A21\u5F0F\u5BFC\u51FA\uFF0C\u624D\u80FD\u4F7F\u7528\u3010\u589E\u91CF\u3011");
		iszl.setBounds(303, 22, 73, 23);
		panel.add(iszl);

//		custExp = new JRadioButton("\u8868\u64CD\u4F5C");
//		custExp.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				if (custExp.isSelected()) {
//					fullmodel.setEnabled(false);
//				} else {
//					fullmodel.setEnabled(true);
//				}
//
//				ebu = new ExportBeanUnit();
//				ebu.setUser(TF_user.getText().toString());
//				ebu.setFile(TF_file.getText().toString());
//				String pwd = String.valueOf(PF_user.getText());
//				ebu.setPsd(pwd);
//				ebu.setSid(TF_sid.getText().toString());
//				ebu.setSip(serverip.getText().toString());
//
//				if (TF_user.getText().toString().length() == 0) {
//					JOptionPane.showMessageDialog(ExportPanel.this, "请输入用户名!");
//					return;
//				}
//				if (pwd.length() == 0) {
//					JOptionPane.showMessageDialog(ExportPanel.this, "请输入密码!");
//					return;
//				}
//				if (TF_sid.getText().toString().length() == 0) {
//					JOptionPane.showMessageDialog(ExportPanel.this, "请输入SID!");
//					return;
//				}
//				if (instanceName.getText().toString().length() == 0) {
//					JOptionPane.showMessageDialog(ExportPanel.this, "实例名不能为空!");
//					return;
//				}
//
//				new SingleExportInfoPanel(FrameManager.sf, ebu).setVisible(true);
//			}
//		});
//		custExp.setBounds(6, 47, 66, 23);
//		panel.add(custExp);
		setLayout(groupLayout);
	}

	public void actionPerformed(ActionEvent e) {
		ExportBeanUnit ebu = new ExportBeanUnit();
		options = new String[6];
		options[0] = String.valueOf(compress.isSelected());
		options[1] = String.valueOf(indexes.isSelected());
		options[2] = String.valueOf(islog.isSelected());
		options[3] = String.valueOf(usermodel.isSelected());
		options[4] = String.valueOf(fullmodel.isSelected());
		options[5] = String.valueOf(iszl.isSelected());
		String cmd = e.getActionCommand();
		if (cmd.equals("B_browse")) {
			File file = SwingUtils.selectSaveFile(new File(TF_file.getText()), new FileFilter[] { new FileFilter("dmp", "DMP文件") }, this);
			if (file != null)
				TF_file.setText(file.getPath());
		} else if (cmd.equals("log")) {
			File file = SwingUtils.selectSaveFile(new File(Tlog.getText()), new FileFilter[] { new FileFilter("log", "日志文件") }, this);
			if (file != null)
				Tlog.setText(file.getPath());
		} else if (cmd.equals("startExport")) {
			try {
				user = TF_user.getText().trim();
				psd = String.valueOf(PF_user.getPassword());
				sid = TF_sid.getText().trim();
				file = TF_file.getText().trim();
				sip = serverip.getText().trim();
				_instanceName = instanceName.getText().trim();
				if (user.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入用户名!");
					return;
				}
				if (psd.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入密码!");
					return;
				}
				if (sid.length() == 0) {
					JOptionPane.showMessageDialog(this, "请输入SID!");
					return;
				}
				if (file.length() == 0) {
					JOptionPane.showMessageDialog(this, "请选择DMP文件保存位置!");
					return;
				}
				if (_instanceName.length() == 0) {
					JOptionPane.showMessageDialog(this, "实例名不能为空!");
					return;
				}
				ebu.setFile(file);
				ebu.setOptions(options);
				ebu.setPsd(psd);
				ebu.setSid(sid);
				ebu.setSip(sip);
				ebu.setInstanceName(_instanceName);
				ebu.setTlog(Tlog.getText().toString());
				ebu.setUser(user);
				startExport.setEnabled(false);
				new ExportInfoPanel(FrameManager.sf).setVisible(true);
			} catch (Exception exp) {
				JOptionPane.showMessageDialog(this, "出现错误: " + exp);
			}
		}
	}

	class ExportInfoPanel extends JDialog implements WindowListener {

		private static final long serialVersionUID = 6162899017966256858L;

		JTextArea exportinfo = new JTextArea();

		public ExportInfoPanel(SuperFrame fSuper) {
			super(fSuper);
			initGUI();
		}

		private void initGUI() {
			this.setTitle("数据库导出");
			this.setSize(550, 300);
			this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
			Font x = new Font("Serif", 0, 12);
			exportinfo.setFont(x);
			JScrollPane jsp = new JScrollPane(exportinfo);
			getContentPane().add(jsp, "Center");
			this.setLocationRelativeTo(null);
			this.setAlwaysOnTop(true);
			this.addWindowListener((WindowListener) this);
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						FrameManager.sf.setEnabled(false);
						String expcmd = ProjectContext.getWinExpPath() + "exp.exe " + user + "/" + psd + "@" + sip + "/" + sid + " file=" + file + " buffer=65536 feedback=100000";
						if (options.length > 0) {
							if (Boolean.valueOf(options[0])) {
								expcmd = expcmd + " compress=y";
							}
							if (Boolean.valueOf(options[1])) {
								expcmd = expcmd + " indexes=y";
							}
							if (Boolean.valueOf(options[2])) {
								expcmd = expcmd + " log=" + Tlog.getText().toString();
							}
							if (Boolean.valueOf(options[3])) {
								expcmd = expcmd + " owner=" + user;
							}
							if (Boolean.valueOf(options[4])) {
								expcmd = expcmd + " full=y";
							}
							if (Boolean.valueOf(options[5])) {
								expcmd = expcmd + " full=y inctype=incremental";
							}
						}

						String osName = System.getProperty("os.name");
						String[] cmd = new String[3];

						if (osName.equals("Windows NT")) {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						} else if (osName.equals("Windows XP")) {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						} else if (osName.equals("Windows 95")) {
							cmd[0] = "command.com";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						} else if (osName.equals("Windows 7")) {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						}else {
							cmd[0] = "cmd.exe";
							cmd[1] = "/C";
							cmd[2] = expcmd;
						}

						Runtime rt = Runtime.getRuntime();
						System.out.println("Execing " + cmd[0] + " " + cmd[1] + " " + cmd[2]);
						Process proc = rt.exec(cmd);
						InputStreamReader isr = new InputStreamReader(proc.getErrorStream());
						BufferedReader br = new BufferedReader(isr);
						String line = null;
						while ((line = br.readLine()) != null) {
							if (!line.equals("") || line != null) {
								exportinfo.append(line + "\n");
								exportinfo.setSelectionStart(exportinfo.getText().length());
							}
						}
					} catch (Exception exp) {
						JOptionPane.showMessageDialog(FrameManager.sf, "出现错误: " + exp);
					} finally {
						FrameManager.sf.setEnabled(true);
					}
				}
			});
			t.start();
		}

		@Override
		public void windowOpened(WindowEvent e) {

		}

		@Override
		public void windowClosing(WindowEvent e) {

		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {

		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowActivated(WindowEvent e) {

		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			if (!this.isVisible()) {
				startExport.setEnabled(true);
			}

		}
	}

	public JTextField getTF_user() {
		return TF_user;
	}

	public JTextField getTF_sid() {
		return TF_sid;
	}

	public JTextField getTF_file() {
		return TF_file;
	}

	public String getSip() {
		return sip;
	}

	public void setSip(String sip) {
		this.sip = sip;
	}

	public JTextField getServerip() {
		return serverip;
	}

	public void setServerip(JTextField serverip) {
		this.serverip = serverip;
	}

	public JTextField getTlog() {
		return Tlog;
	}

	public void setTlog(JTextField tlog) {
		Tlog = tlog;
	}

	public JCheckBox getIslog() {
		return islog;
	}

	public void setIslog(JCheckBox islog) {
		this.islog = islog;
	}

	public JRadioButton getFullmodel() {
		return fullmodel;
	}

	public void setFullmodel(JRadioButton fullmodel) {
		this.fullmodel = fullmodel;
	}

	public JCheckBox getIszl() {
		return iszl;
	}

	public void setIszl(JCheckBox iszl) {
		this.iszl = iszl;
	}

	public JCheckBox getIndexes() {
		return indexes;
	}

	public void setIndexes(JCheckBox indexes) {
		this.indexes = indexes;
	}

	public JCheckBox getCompress() {
		return compress;
	}

	public void setCompress(JCheckBox compress) {
		this.compress = compress;
	}

	public JRadioButton getUsermodel() {
		return usermodel;
	}

	public void setUsermodel(JRadioButton usermodel) {
		this.usermodel = usermodel;
	}

	public JPasswordField getPF_user() {
		return PF_user;
	}

	public void setPF_user(JPasswordField pF_user) {
		PF_user = pF_user;
	}

	private DefaultMutableTreeNode packageRoot(final CheckboxTree tree, final DefaultTreeModel dtm, final DefaultMutableTreeNode dmt, String database, String table_schem, JLabel pbarinfo,
			JLabel tableSize) {
		wf_Db_Connect db_connect = null;
		try {
			db_connect = wf_Db_Server.getInstance().getFactory(database).get();
			DatabaseMetaData data = db_connect.getConnect().getMetaData();

			ResultSet schemas = data.getSchemas();
			ResultSetMetaData metaData = schemas.getMetaData();
			int count = metaData.getColumnCount();
			while (schemas.next()) {
				for (int i = 0; i < count; i++) {
					String busschem = schemas.getString("TABLE_SCHEM");
					// 模式
					if (table_schem.equals(busschem)) {
						ResultSet resultSet = data.getTables(null, busschem, "%", new String[] { "TABLE" });
						try {
							int _i = 1;
							while (resultSet.next()) {
								final String tName = resultSet.getString("TABLE_NAME");
								Statement statement = db_connect.getConnect().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
								try {
									ResultSet set = statement.executeQuery("SELECT * FROM " + tName);
									try {
										set.last();
										final int row = set.getRow();
										new Thread() {
											public void run() {
												Runnable r = new Runnable() {
													public void run() {
														DefaultMutableTreeNode _dmt = new DefaultMutableTreeNode(new IconNode(new ImageIcon("image/table.png"), tName.toUpperCase(), row));
														dmt.add(_dmt);
														TreePath path = new TreePath(dmt);
														tree.expandPath(path);
														tree.setScrollsOnExpand(true);
														dtm.insertNodeInto(_dmt, dmt, dmt.getChildCount() - 1);
														dtm.nodeStructureChanged(_dmt);
														Util.getInstance().addCacheTreePath(tName, _dmt);
													}
												};
												SwingUtilities.invokeLater(r);
											}

										}.start();
										pbarinfo.setText("正在读取表:" + tName);
										tableSize.setText("总表数[" + (_i) + "]");
									} finally {
										set.close();
									}
								} finally {
									statement.close();
								}
								_i++;
								Thread.sleep(50);
							}
							pbarinfo.setText("表已读取完毕");
							// tree.repaint();
						} finally {
							resultSet.close();
						}
					}
				}
			}
		} catch (wf_Db_Exception e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
		} catch (SQLException e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
		} catch (Exception e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
		} finally {
			db_connect.close();
		}

		return dmt;
	}

	public boolean build_config_file(ExportBeanUnit ebu) {
		OutputStreamWriter os = null;
		String path = ProjectContext.getSourcePath() + "/dbconfig.property";
		;
		try {
			File file = new File(path);
			if (!file.canRead()) {
				file = new File(file.getAbsolutePath());
			}
			os = new OutputStreamWriter(new FileOutputStream(file), "GBK");
			try {
				os.write("LOG=off" + System.getProperty("line.separator"));
				os.write("TRACE=off" + System.getProperty("line.separator"));
				os.write("" + System.getProperty("line.separator"));
				os.write("#database properties" + System.getProperty("line.separator"));
				os.write("DATABASE_TYPE=oracle" + System.getProperty("line.separator"));
				os.write("JDBC_DRIVER=oracle.jdbc.OracleDriver" + System.getProperty("line.separator"));
				os.write("CONNECTION_URL=jdbc:oracle:thin:@" + ebu.getSip() + ":1521:" + ebu.getSid() + System.getProperty("line.separator"));
				os.write("LOGIN_ID=" + ebu.getUser() + System.getProperty("line.separator"));
				os.write("LOGIN_PASSWORD=" + ebu.getPsd() + System.getProperty("line.separator"));
				os.write("MIN_CONNECTION=10" + System.getProperty("line.separator"));
				os.write("MAX_CONNECTION=15" + System.getProperty("line.separator"));
				os.write("MAX_CONNECT_TIME=60" + System.getProperty("line.separator"));
				os.write("IDLETIME=2" + System.getProperty("line.separator"));
				os.write("DB_LOG_FILE=bzserver.log" + System.getProperty("line.separator"));
				os.flush();
				return true;

			} finally {
				os.close();
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			return false;
		}
	}

}
