package org.dba.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.ExportBeanUnit;
import org.dba.beanunit.IconNode;
import org.dba.dbunit.wf_Db_Connect;
import org.dba.dbunit.wf_Db_Exception;
import org.dba.dbunit.wf_Db_Server;
import org.dba.util.Util;
import org.dba.util.FileFilter;
import org.dba.util.ProjectContext;
import org.dba.util.SwingUtils;
import java.awt.Font;

public class SimpleTablePanel1 extends JDialog implements WindowListener {
	
	private String[] options;
	private JLabel proinfo;
	private JLabel tableSize;
	private ExportBeanUnit ebu = null;

	private static final long serialVersionUID = 6162899017966256858L;
	Log log = LogFactory.getLog(getClass());

	public SimpleTablePanel1() {
		//super(fSuper);
		initGUI(ebu);
	}

	private void initGUI(final ExportBeanUnit ebu) {
		this.setTitle("表操作");
		this.setSize(550, 500);
		this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.addWindowListener((WindowListener) this);
		this.setResizable(false);

		JPanel bJPanel = new JPanel();
		bJPanel.setLayout(new BorderLayout());

		JPanel status_jpanel = new JPanel();
		status_jpanel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		status_jpanel.setPreferredSize(new Dimension(getWidth(), 30));
		proinfo = new JLabel("...");
		proinfo.setLocation(39, 10);
		proinfo.setSize(new Dimension(18, 15));
		proinfo.setForeground(Color.RED);

		tableSize = new JLabel("总表数");
		tableSize.setSize(new Dimension(getWidth() / 2, 10));
		tableSize.setForeground(Color.RED);
		status_jpanel.setLayout(null);

		status_jpanel.add(proinfo);
		status_jpanel.add(tableSize, FlowLayout.LEFT);

		final CheckboxTree jtree = new CheckboxTree();

		BorderLayout bl = new BorderLayout();
		getContentPane().setLayout(bl);

		JScrollPane jsp = new JScrollPane(jtree);
		jsp.setLayout(new ScrollPaneLayout());
		jsp.setPreferredSize(new Dimension(300, 50));
		getContentPane().add(jsp, "Center");

		JPanel xx = new JPanel();
		xx.setBorder(new TitledBorder(null, "选项", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		xx.setPreferredSize(new Dimension(134, 50));

		SpringLayout splayout = new SpringLayout();
		xx.setLayout(splayout);

		final JCheckBox indexs = new JCheckBox("索引");
		indexs.setPreferredSize(new Dimension(50, 25));
		xx.add(indexs);

		final JCheckBox rows = new JCheckBox("数据行");
		rows.setPreferredSize(new Dimension(70, 25));
		xx.add(rows);

		final JCheckBox compress = new JCheckBox("压缩");
		compress.setPreferredSize(new Dimension(50, 25));
		xx.add(compress);

		final JCheckBox whdelrow = new JCheckBox("彻底删除数据");
		whdelrow.setPreferredSize(new Dimension(100, 25));
		whdelrow.setForeground(Color.RED);
		whdelrow.setToolTipText("勾选此项,将无法从日志中恢复数据!");
		xx.add(whdelrow);

		splayout.putConstraint(SpringLayout.NORTH, indexs, 0, SpringLayout.NORTH, xx);
		splayout.putConstraint(SpringLayout.WEST, indexs, 10, SpringLayout.WEST, xx);

		splayout.putConstraint(SpringLayout.NORTH, rows, 0, SpringLayout.NORTH, xx);
		splayout.putConstraint(SpringLayout.WEST, rows, 60, SpringLayout.WEST, indexs);

		splayout.putConstraint(SpringLayout.NORTH, compress, 0, SpringLayout.NORTH, xx);
		splayout.putConstraint(SpringLayout.WEST, compress, 70, SpringLayout.WEST, rows);

		splayout.putConstraint(SpringLayout.NORTH, whdelrow, 0, SpringLayout.NORTH, xx);
		splayout.putConstraint(SpringLayout.WEST, whdelrow, 70, SpringLayout.WEST, compress);

		// 把txt3按前面设好的约束，添加到父容器中。

		getContentPane().add(xx, "North");

		JPanel pfileJl = new JPanel();
		pfileJl.setLayout(new BorderLayout());

		JPanel fileJl = new JPanel();
		fileJl.setBorder(new TitledBorder(null, "文件栏", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		fileJl.setLayout(null);

		JLabel stf = new JLabel("文件路径：");
		stf.setBounds(29, 19, 60, 15);
		fileJl.add(stf);

		final JTextField s_TF_file = new JTextField("请选择文件");
		s_TF_file.setFont(new Font("宋体", Font.PLAIN, 12));
		s_TF_file.setBounds(89, 16, 300, 20);
		s_TF_file.setPreferredSize(new Dimension(300, 20));
		fileJl.add(s_TF_file);

		final JButton sbf = new JButton("浏览");
		sbf.setBounds(399, 15, 57, 23);
		fileJl.add(sbf);

		sbf.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File file = SwingUtils.selectSaveFile(new File(s_TF_file.getText()), new FileFilter[] { new FileFilter("dmp", "DMP文件") }, sbf);
				if (file != null)
					s_TF_file.setText(file.getPath());

			}
		});

		fileJl.setPreferredSize(new Dimension(545, 45));
		pfileJl.add(fileJl, BorderLayout.WEST);

		JPanel exJl = new JPanel();
		exJl.setBorder(new TitledBorder(null, "工具栏", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		exJl.setPreferredSize(new Dimension(550, 50));
		exJl.setLayout(null);

		JButton button = new JButton("导出");
		button.setBounds(21, 21, 57, 22);
		exJl.add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				options = new String[3];
				options[0] = String.valueOf(compress.isSelected());
				options[1] = String.valueOf(indexs.isSelected());
				options[2] = String.valueOf(rows.isSelected());

				// 组装表导出命令
				/*
				 * EXP MBZADMIN/MBZADMIN@192.168.3.200/BZPM ROWS=Y INDEXES=N
				 * COMPRESS=N FILE=D:\PERSON_ROLE.DMP TABLES=PERSON_ROLE,
				 */
				final String _sip = ebu.getSip();// 服务器ip
				final String _user = ebu.getUser();
				final String _psd = ebu.getPsd();
				final String _sid = ebu.getSid();
				final String _file = s_TF_file.getText().toString();
				proinfo.setText("...");

				new Thread() {
					public void run() {
						try {
							if ("请选择文件".equals(_file)) {
								proinfo.setText("请选择文件路径!");
							} else {
								int r = JOptionPane.showConfirmDialog(SimpleTablePanel1.this, "你确定要导出当前选中的数据表么?", "导出表", JOptionPane.YES_NO_OPTION);
								if (r == 0) {
									String expcmd = ProjectContext.getWinExpPath() + "EXP.EXE " + _user + "/" + _psd + "@" + _sip + "/" + _sid + " FILE=" + _file + " BUFFER=65536 FEEDBACK=100000";
									if (options.length > 0) {
										if (Boolean.valueOf(options[0])) {
											expcmd = expcmd + " COMPRESS=Y";
										}
										if (Boolean.valueOf(options[1])) {
											expcmd = expcmd + " INDEXES=Y";
										}
										if (Boolean.valueOf(options[2])) {
											expcmd = expcmd + " ROWS=Y";
										}
									}
									String osName = System.getProperty("os.name");
									String[] cmd = new String[4];

									if (osName.equals("Windows NT")) {
										cmd[0] = "cmd.exe";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									} else if (osName.equals("Windows XP")) {
										cmd[0] = "cmd.exe";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									} else if (osName.equals("Windows 95")) {
										cmd[0] = "command.com";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									} else if (osName.equals("Windows 7")) {
										cmd[0] = "cmd.exe";
										cmd[1] = "/C";
										cmd[2] = expcmd;
									}

									HashSet<String> selectTables = Util.getInstance().getSelectTables();
									String _t = "";
									if (selectTables.size() > 0) {
										for (String tName : selectTables) {
											_t = _t + tName + ",";
										}
									}
									log.info("Length:" + _t.length());
									if (_t.length() > 0) {
										cmd[3] = " TABLES=" + _t;
									} else {
										return;
									}
									Runtime rt = Runtime.getRuntime();
									log.info("Execing " + cmd[0] + cmd[1] + cmd[2] + cmd[3]);
									Process proc = rt.exec(cmd);
									InputStreamReader isr = new InputStreamReader(proc.getErrorStream());
									BufferedReader br = new BufferedReader(isr);
									String line = null;
									while ((line = br.readLine()) != null) {
										if (!line.equals("") || line != null) {
											if (line.contains("ORA-") || line.contains("IMP-") || line.contains("EXP-")) {
												proinfo.setText(line + "\n");
												proinfo.setText("程序中止...\n");
												break;
											} else {
												proinfo.setText(line + "\n");
											}
										}
									}
								}
							}
						} catch (IOException e) {
							proinfo.setText(e.getMessage());
						}
					}

				}.start();
			}
		});

		JButton imbutton = new JButton("导入");
		imbutton.setBounds(90, 21, 57, 22);
		imbutton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				options = new String[3];
				options[0] = String.valueOf(compress.isSelected());
				options[1] = String.valueOf(indexs.isSelected());
				options[2] = String.valueOf(rows.isSelected());

				// 组装表导出命令
				/*
				 * EXP MBZADMIN/MBZADMIN@192.168.3.200/BZPM ROWS=Y INDEXES=N
				 * COMPRESS=N FILE=D:\PERSON_ROLE.DMP TABLES=PERSON_ROLE,
				 */
				final String _sip = ebu.getSip();// 服务器ip
				final String _user = ebu.getUser();
				final String _psd = ebu.getPsd();
				final String _sid = ebu.getSid();
				final String _file = s_TF_file.getText().toString();
				proinfo.setText("...");

				new Thread() {
					public void run() {
						try {
							if ("请选择文件".equals(_file)) {
								proinfo.setText("请选择文件路径!");
							} else {
								int r = JOptionPane.showConfirmDialog(SimpleTablePanel1.this, "你确定要导入当前选中的数据表么?请注意你导出表时所勾选的选项,以免发生错误!", "导入表", JOptionPane.YES_NO_OPTION);
								if (r == 0) {
									String expcmd = ProjectContext.getWinExpPath() + "IMP.EXE " + _user + "/" + _psd + "@" + _sip + "/" + _sid + " FILE=" + _file + " FROMUSER=" + _user
											+ " TOUSER=" + _user + " BUFFER=65536 FEEDBACK=100000";
									if (options.length > 0) {
										if (Boolean.valueOf(options[0])) {
											expcmd = expcmd + " COMPRESS=Y";
										}
										if (Boolean.valueOf(options[1])) {
											expcmd = expcmd + " INDEXES=Y";
										}
										if (Boolean.valueOf(options[2])) {
											expcmd = expcmd + " ROWS=Y";
										}
									}

									String[] cmd = Util.getInstance().getCmd(4, expcmd);
									HashSet<String> selectTables = Util.getInstance().getSelectTables();
									String _t = "";
									if (selectTables.size() > 0) {
										for (String tName : selectTables) {
											_t = _t + tName + ",";
										}
										cmd[3] = " TABLES=" + _t;

										wf_Db_Connect db_connect = null;
										try {
											db_connect = wf_Db_Server.getInstance().getFactory("dbconfig").get();
											for (String tName : selectTables) {
												Util.getInstance().dropTable(db_connect, tName, proinfo);
											}
											// DbUtil.getInstance().clearTables();
											// initTree(jtree);
											removeNode(jtree);
										} catch (Exception e1) {
											proinfo.setText(e1.getMessage());
										} finally {
											db_connect.close();
										}
										// 删除表
									} else {
										proinfo.setText("没有检测到要导入的表!");
										return;
									}

									// 检查当前的表是否

									Runtime rt = Runtime.getRuntime();
									log.info("Execing " + cmd[0] + cmd[1] + cmd[2] + cmd[3]);
									Process proc = rt.exec(cmd);
									InputStreamReader isr = new InputStreamReader(proc.getErrorStream());
									BufferedReader br = new BufferedReader(isr);
									String line = null;
									while ((line = br.readLine()) != null) {
										if (!line.equals("") || line != null) {

											if (line.contains("ORA-") || line.contains("IMP-")) {
												proinfo.setText(line);
												proinfo.setText("程序中止..." + line);
												break;
											} else {
												proinfo.setText(line);
											}

											proinfo.setText(line);
										}
									}
									Util.getInstance().clearTables();
									initTree(jtree);
								}
							}
						} catch (IOException e) {
							e.printStackTrace();
							proinfo.setText(e.getMessage());
						}
					}

				}.start();
			}
		});

		exJl.add(imbutton);

		JButton refresh = new JButton("刷新");
		refresh.setBounds(157, 21, 57, 22);
		refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						Util.getInstance().clearTables();
						initTree(jtree);
					}
				}).start();
			}
		});
		exJl.add(refresh);
		JButton deleteD = new JButton("清空表");
		deleteD.setBounds(225, 21, 69, 22);
		deleteD.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int r = JOptionPane.showConfirmDialog(SimpleTablePanel1.this, "你确定要清空当前选中的数据表中的数据么?", "清空表", JOptionPane.YES_NO_OPTION);
				if (r == 0) {
					final HashSet<String> selectTables = Util.getInstance().getSelectTables();
					if (selectTables.size() > 0) {
						try {
							new Thread(new Runnable() {
								wf_Db_Connect db_connect = null;

								public void run() {
									try {
										db_connect = wf_Db_Server.getInstance().getFactory("dbconfig").get();
										for (String tName : selectTables) {
											if (whdelrow.isSelected()) {
												Util.getInstance().truncateTable(db_connect, tName, proinfo);
												log.info("启用truncate删除模式!");
											} else {
												Util.getInstance().deleteTable(db_connect, tName, proinfo);
											}
										}
										initTree(jtree);
										Util.getInstance().clearTables();
									} catch (Exception e1) {
										proinfo.setText(e1.getMessage());
									} finally {
										db_connect.close();
									}

								}
							}).start();
							// reLoadTree(jtree);
						} catch (Exception e1) {
							proinfo.setText(e1.getMessage());
						}
					}
				}
			}
		});
		exJl.add(deleteD);
		JButton deleteT = new JButton("删除表");
		deleteT.setBounds(304, 21, 69, 22);
		exJl.add(deleteT);
		deleteT.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				int r = JOptionPane.showConfirmDialog(SimpleTablePanel1.this, "你确定要删除当前选中的数据表么?", "删除表", JOptionPane.YES_NO_OPTION);
				if (r == 0) {
					final HashSet<String> selectTables = Util.getInstance().getSelectTables();
					if (selectTables.size() > 0) {
						// /
						new Thread(new Runnable() {
							public void run() {
								wf_Db_Connect db_connect = null;
								try {
									db_connect = wf_Db_Server.getInstance().getFactory("dbconfig").get();
									for (String tName : selectTables) {
										Util.getInstance().dropTable(db_connect, tName, proinfo);
									}
									Util.getInstance().clearTables();
									initTree(jtree);
								} catch (Exception e1) {
									proinfo.setText(e1.getMessage());
								} finally {
									db_connect.close();
								}
							}
						}).start();
						//
					}
				}
			}
		});

		bJPanel.add(exJl, BorderLayout.NORTH);
		bJPanel.add(pfileJl, "East");
		bJPanel.add(status_jpanel, "South");

		getContentPane().add(bJPanel, "South");
		// 启动线程加载
		build_config_file(ebu);
		new Thread(new Runnable() {
			@Override
			public void run() {
				initTree(jtree);
			}
		}).start();
	}

	@Override
	public void windowOpened(WindowEvent e) {
		//startExport.setEnabled(false);
		//custExp.setEnabled(false);
	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		if (!this.isVisible()) {
			//startExport.setEnabled(true);
			//custExp.setEnabled(true);
		}

	}
	
	private DefaultMutableTreeNode packageRoot(final CheckboxTree tree, final DefaultTreeModel dtm, final DefaultMutableTreeNode dmt, String database, String table_schem, JLabel pbarinfo,
			JLabel tableSize) {
		wf_Db_Connect db_connect = null;
		try {
			db_connect = wf_Db_Server.getInstance().getFactory(database).get();
			DatabaseMetaData data = db_connect.getConnect().getMetaData();

			ResultSet schemas = data.getSchemas();
			ResultSetMetaData metaData = schemas.getMetaData();
			int count = metaData.getColumnCount();
			while (schemas.next()) {
				for (int i = 0; i < count; i++) {
					String busschem = schemas.getString("TABLE_SCHEM");
					// 模式
					if (table_schem.equals(busschem)) {
						ResultSet resultSet = data.getTables(null, busschem, "%", new String[] { "TABLE" });
						try {
							int _i = 1;
							while (resultSet.next()) {
								final String tName = resultSet.getString("TABLE_NAME");
								Statement statement = db_connect.getConnect().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
								try {
									ResultSet set = statement.executeQuery("SELECT * FROM " + tName);
									try {
										set.last();
										final int row = set.getRow();
										new Thread() {
											public void run() {
												Runnable r = new Runnable() {
													public void run() {
														DefaultMutableTreeNode _dmt = new DefaultMutableTreeNode(new IconNode(new ImageIcon("image/table.png"), tName.toUpperCase(), row));
														dmt.add(_dmt);
														TreePath path = new TreePath(dmt);
														tree.expandPath(path);
														tree.setScrollsOnExpand(true);
														dtm.insertNodeInto(_dmt, dmt, dmt.getChildCount() - 1);
														dtm.nodeStructureChanged(_dmt);
														Util.getInstance().addCacheTreePath(tName, _dmt);
													}
												};
												SwingUtilities.invokeLater(r);
											}

										}.start();
										pbarinfo.setText("正在读取表:" + tName);
										tableSize.setText("总表数[" + (_i) + "]");
									} finally {
										set.close();
									}
								} finally {
									statement.close();
								}
								_i++;
								Thread.sleep(50);
							}
							pbarinfo.setText("表已读取完毕");
							// tree.repaint();
						} finally {
							resultSet.close();
						}
					}
				}
			}
		} catch (wf_Db_Exception e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
		} catch (SQLException e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
		} catch (Exception e) {
			pbarinfo.setText(e.getMessage());
			log.error(e.getMessage());
		} finally {
			db_connect.close();
		}

		return dmt;
	}
	
	public boolean build_config_file(ExportBeanUnit ebu) {
		OutputStreamWriter os = null;
		String path = ProjectContext.getSourcePath() + "/dbconfig.property";
		;
		try {
			File file = new File(path);
			if (!file.canRead()) {
				file = new File(file.getAbsolutePath());
			}
			os = new OutputStreamWriter(new FileOutputStream(file), "GBK");
			try {
				os.write("LOG=off" + System.getProperty("line.separator"));
				os.write("TRACE=off" + System.getProperty("line.separator"));
				os.write("" + System.getProperty("line.separator"));
				os.write("#database properties" + System.getProperty("line.separator"));
				os.write("DATABASE_TYPE=oracle" + System.getProperty("line.separator"));
				os.write("JDBC_DRIVER=oracle.jdbc.OracleDriver" + System.getProperty("line.separator"));
				os.write("CONNECTION_URL=jdbc:oracle:thin:@" + ebu.getSip() + ":1521:" + ebu.getSid() + System.getProperty("line.separator"));
				os.write("LOGIN_ID=" + ebu.getUser() + System.getProperty("line.separator"));
				os.write("LOGIN_PASSWORD=" + ebu.getPsd() + System.getProperty("line.separator"));
				os.write("MIN_CONNECTION=10" + System.getProperty("line.separator"));
				os.write("MAX_CONNECTION=15" + System.getProperty("line.separator"));
				os.write("MAX_CONNECT_TIME=60" + System.getProperty("line.separator"));
				os.write("IDLETIME=2" + System.getProperty("line.separator"));
				os.write("DB_LOG_FILE=bzserver.log" + System.getProperty("line.separator"));
				os.flush();
				return true;

			} finally {
				os.close();
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			return false;
		}
	}

	public void initTree(final CheckboxTree tree) {
		final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(new IconNode(new ImageIcon("image/database_type.png"), ebu.getSip(), -1));
		final DefaultTreeModel rootModel = new DefaultTreeModel(rootNode);
		tree.setModel(rootModel);
		tree.setCellRenderer(new IconNodeRenderer());
		tree.setRootVisible(true);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setRowHeight(20);
		tree.setAlignmentY(CENTER_ALIGNMENT);
		tree.setAlignmentX(CENTER_ALIGNMENT);
		tree.setVisibleRowCount(20);

		tree.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			/** 
			 * 实现以键盘模糊搜索功能
			 */
			public void keyPressed(KeyEvent e) {
				char keyChar = e.getKeyChar();
				log.info(keyChar);
				String t = Util.getInstance().getKeyInput(String.valueOf(keyChar));
				if (KeyEvent.VK_ENTER == e.getKeyCode()) {
					log.info("T=" + t);
					if (t != null) {
						final DefaultMutableTreeNode tp = Util.getInstance().getCacheTreePath(String.valueOf(t));
						if (tp != null) {
							new Thread() {
								public void run() {
									Runnable r = new Runnable() {
										public void run() {
											TreeNode[] treepath = (TreeNode[]) tp.getPath();
											TreePath path = new TreePath(treepath);
											tree.setSelectionPath(path);
											tree.scrollPathToVisible(path);
										}
									};
									SwingUtilities.invokeLater(r);
								}

							}.start();
						}
					}
				}
			}
		});

		tree.addTreeCheckingListener(new TreeCheckingListener() {
			@Override
			public void valueChanged(TreeCheckingEvent e) {
				TreePath leadingPath = e.getLeadingPath();
				DefaultMutableTreeNode dmt = (DefaultMutableTreeNode) leadingPath.getLastPathComponent();
				IconNode in = (IconNode) dmt.getUserObject();
				boolean isChecked = tree.getCheckingModel().isPathChecked(leadingPath);
				String value = in.getText();
				//log.info("==:" + isChecked + "  value:" + value);
				if (isChecked && value != null) {
					Util.getInstance().add2Array(value);
					Util.getInstance().add2Sarray(tree.getSelectionPath());
				} else {
					Util.getInstance().removeArray(value);
					Util.getInstance().removeSarray(tree.getSelectionPath());
				}
				//将选中的节点加入到缓存中
				
				
				HashSet<String> selectTables = Util.getInstance().getSelectTables();
				for (String s : selectTables) {
					log.info("当前的值是：" + s);
				}
				log.info("ArrayList size = " + Util.getInstance().getSize());
			}
		});
		packageRoot(tree, rootModel, rootNode, "dbconfig", ebu.getPsd(), proinfo, tableSize);
	}
	
	private void removeNode(final CheckboxTree tree) {
		HashSet<TreePath> selectTreePath = Util.getInstance().getSelectTreePath();
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		for(TreePath treepath:selectTreePath){
			if (treepath != null) {
				// 下面两行取得选取节点的父节点.
				DefaultMutableTreeNode selectionNode = (DefaultMutableTreeNode) treepath.getLastPathComponent();
				TreeNode parent = (TreeNode) selectionNode.getParent();
				if (parent != null) {
					// 由DefaultTreeModel的removeNodeFromParent()方法删除节点，包含它的子节点。
					treeModel.removeNodeFromParent(selectionNode);
				}
			}
		}
		//TreePath treepath = tree.getSelectionPath();
	}
	
}
