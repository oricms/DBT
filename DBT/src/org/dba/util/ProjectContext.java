package org.dba.util;

import java.io.File;

public class ProjectContext {
	
	public static String root;
	
	public static String getRoot() {
		return root;
	}
	
	public static String getLibPath() {
		return root + "lib" + File.separator;
	}
	
	public static String getSourcePath() {
		return root + "source" + File.separator;
	}
	
	public static String getWinExpPath() {
		return root + "wbin" + File.separator;
	}
	
}
