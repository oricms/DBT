package org.dba;


import java.io.File;

/**
 * @author jacklei
 *
 */
public class ServerStart {
	
	private static ServerLoader serverLoader;
	public ServerStart() {
	}

	public static void main(String args[]) {
		try {
			String rootPath;
			if (args.length > 0)
				rootPath = args[0];
			else
				rootPath = (new File("")).getAbsolutePath();
			serverLoader = new ServerLoader();
			serverLoader.pluginLoad(rootPath);
			serverLoader.startServer(args);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
}
