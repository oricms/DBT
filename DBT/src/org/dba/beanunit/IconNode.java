package org.dba.beanunit;

import javax.swing.Icon;

public class IconNode {
	/**
	 * 
	 */
	protected Icon icon;
	protected String txt;
	protected int row;

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	// 只包含文本的节点构造
	public IconNode(String txt) {
		this.txt = txt;
	}

	// 包含文本和图片的节点构造
	public IconNode(Icon icon, String txt) {
		this.icon = icon;
		this.txt = txt;
	}

	// 包含文本和图片的节点构造
	public IconNode(Icon icon, String txt, int row) {
		this.icon = icon;
		this.txt = txt;
		this.row = row;
	}

	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	public Icon getIcon() {
		return icon;
	}

	public void setText(String txt) {
		this.txt = txt;
	}

	public String getText() {
		return txt;
	}

}
