package org.dba.dbunit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.DynamicBean;
import org.dba.util.ProjectContext;

public class wf_Db_Server {
	private static wf_Db_Server server;
	private wf_Db_Factory factory;
	Log log = LogFactory.getLog(getClass());

	public static synchronized wf_Db_Server getInstance() {
		if (server == null) {
			server = new wf_Db_Server();
		}
		return server;
	}


	public wf_Db_Factory getFactory(String database) throws wf_Db_Exception{
		String path = ProjectContext.getSourcePath()+database+".property";
		path = path.replace("\\", "/");
		File file = new File(path);
		InputStream stream = null;
		try {
			stream = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			log.info("数据库配置文件没有找到!");
		}
		log.info("数据库连接配置文件:"+path);
		this.factory = new wf_Db_Factory();
		this.factory.load(stream);
		return this.factory;
	}
	
	/**
	 * 读取配置文件流来获取数据库连接
	 * @param stream
	 * @return
	 * @throws wf_Db_Exception
	 */
	public wf_Db_Factory getFactory(DynamicBean db) throws wf_Db_Exception{
		Properties properties = new Properties();
		properties.setProperty("DATABASE_TYPE","oracle");
		properties.setProperty("JDBC_DRIVER","oracle.jdbc.OracleDriver");
		log.info("DataBase INFO:IP="+db.getValue("ip") +" PORT="+String.valueOf(db.getValue("port"))+" SID="+db.getValue("sid"));
		properties.setProperty("CONNECTION_URL","jdbc:oracle:thin:@" + db.getValue("ip") + ":"+String.valueOf(db.getValue("port"))+":" + db.getValue("sid"));
		properties.setProperty("LOGIN_ID",String.valueOf(db.getValue("user")));
		properties.setProperty("LOGIN_PASSWORD",String.valueOf(db.getValue("pwd")));
		properties.setProperty("MIN_CONNECTION","5");
		properties.setProperty("MAX_CONNECTION","20");
		properties.setProperty("MAX_CONNECT_TIME","60");
		properties.setProperty("IDLETIME","5");
		this.factory = new wf_Db_Factory();
		this.factory.build(properties);
		return this.factory;
	}
}