package org.dba.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import org.dba.exception.ProcessException;

public class ConnectConfig extends CommonProcessFile {

	@Override
	public List<String> load2Mem(String filePath) throws ProcessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> load2Mem(File[] filelist) throws ProcessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean compareFile(List<String> sflist, List<String> tglist) throws ProcessException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean compareFile(String svalue, String opCode) throws ProcessException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void buildFile(String filepath) throws ProcessException {
		OutputStreamWriter os = null;
		String _s = "";
		try {
			File file = new File(filePath);
			if (file.exists()) {
				file.delete();
				file = new File(file.getAbsolutePath());
			}
			if (enc == null || enc.length() == 0) {
				os = new OutputStreamWriter(new FileOutputStream(file));
			} else {
				os = new OutputStreamWriter(new FileOutputStream(file), enc);
			}
			try {
				
				
				os.write(_s);
			} finally {
				os.close();
				_s = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
