package org.dba.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class ProcTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Process process = null;
		try {
			process = Runtime.getRuntime().exec("cmd");
		} catch (IOException e) {
			e.printStackTrace();
		}
		new Thread(new ProcRunnable(process.getInputStream())).start();
		new Thread(new ProcRunnable(process.getErrorStream())).start();

		PrintWriter out = new PrintWriter(process.getOutputStream());
		java.util.Scanner in = new java.util.Scanner("exp");

		while(in.hasNextLine()){
			out.println(in.nextLine());
			out.flush();
		}
	}
}
class ProcRunnable implements Runnable{

	InputStream inputStream = null;
	String name;
	public ProcRunnable(InputStream inputStream){
		this.inputStream = inputStream;
	}
	@Override
	public void run() {
		Scanner scanner = new Scanner(inputStream);
		while(scanner.hasNextLine()){
			System.out.println(""+scanner.nextLine());
		}
		try {
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		scanner.close();
	}
}
