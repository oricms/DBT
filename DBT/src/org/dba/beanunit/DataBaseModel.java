package org.dba.beanunit;

import java.io.Serializable;

public class DataBaseModel implements Serializable {

	private static final long serialVersionUID = 7499583163527644630L;
	
	private String tName;//表名
	private String[] column;
	private String tableschem;
	private int hasResult;//是否有数据
	private String[] primarykey;//主键
	private boolean isProcess;
	
	public String getFileName(){
		return gettName();
	}
	
	public String gettName() {
		return tName;
	}
	
	public void settName(String tName) {
		this.tName = tName;
	}

	public String[] getColumn() {
		return column;
	}

	public void setColumn(String[] column) {
		this.column = column;
	}

	public String getTableschem() {
		return tableschem;
	}

	public void setTableschem(String tableschem) {
		this.tableschem = tableschem;
	}

	public int getHasResult() {
		return hasResult;
	}

	public void setHasResult(int hasResult) {
		this.hasResult = hasResult;
	}

	public String[] getPrimarykey() {
		return primarykey;
	}

	public void setPrimarykey(String[] primarykey) {
		this.primarykey = primarykey;
	}

	public boolean isProcess() {
		return isProcess;
	}

	public void setProcess(boolean isProcess) {
		this.isProcess = isProcess;
	}
	
}
