package org.dba.test;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JDialog;

public class EditJDialog extends javax.swing.JDialog implements WindowListener {
	public EditJDialog(Frame owner, boolean modal) {
		super(owner, modal);
		setSize(800, 600);
		setVisible(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.addWindowListener((WindowListener) this);
	}

	@Override
	public void windowActivated(WindowEvent e) {
		System.out.println(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println(1);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println(2);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		System.out.println(3);
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println(4);
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println(5);
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println(6);
	}
}
