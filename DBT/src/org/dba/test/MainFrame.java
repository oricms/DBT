package org.dba.test;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.dba.util.ProjectContext;
import org.dba.util.Util;


public class MainFrame extends JFrame {

	private JDialog SysManage;

	public MainFrame() {
		SysManage = new JDialog(this, true);
		SysManage.getContentPane().setLayout(new BorderLayout());
		SysManage.getContentPane().add(new JLabel("This is SysManage"),
				BorderLayout.CENTER);
		JButton cancel = new JButton("CANCEL");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SysManage.dispose();
			}
		});
		SysManage.getContentPane().add(cancel, BorderLayout.SOUTH);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(new JLabel("This is MainFrame"),
				BorderLayout.CENTER);
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SysManage.pack();
				SysManage.setLocation(SysManage.getParent().getX()+(SysManage.getParent().getWidth() - SysManage.getWidth()) / 2,
						SysManage.getParent().getY()+(SysManage.getParent().getHeight() - SysManage.getHeight()) / 2);
				SysManage.setVisible(true);
			}
		});
		getContentPane().add(ok, BorderLayout.SOUTH);
		setLocation((getToolkit().getScreenSize().width - getWidth()) / 2,
				(getToolkit().getScreenSize().height - getHeight()) / 2);
		setVisible(true);
		pack();
	}

	public static void main(String[] args) {
		File sqlfile = new File(ProjectContext.getSourcePath() + "dll.sql");
		String usrcmd;
		try {
			usrcmd = ProjectContext.getWinExpPath() + "sqlplus.exe BZADMIN/BZADMIN@127.0.0.1/BZPM@" + sqlfile.getCanonicalPath();
			Process process = Runtime.getRuntime().exec(Util.getInstance().getCmd(3,usrcmd));
			InputStreamReader isr = new InputStreamReader(process.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

