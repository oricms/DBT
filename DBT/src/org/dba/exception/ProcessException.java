package org.dba.exception;

public class ProcessException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private Throwable cause = null;

	public ProcessException() {
		super();
	}

	public ProcessException(String message) {
		super(message);
	}

	public ProcessException(Throwable cause) {
		super();
		this.cause = cause;
	}

	public ProcessException(String message, Throwable cause) {
		super(message);
		this.cause = cause;
	}

	public String getMessage() {
		if (super.getMessage() != null) {
			return super.getMessage();
		} else if (cause != null) {
			return cause.toString();
		} else {
			return null;
		}
	}

	public Throwable getThrowable() {
		return cause;
	}
}
