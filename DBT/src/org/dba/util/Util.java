package org.dba.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JLabel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dba.beanunit.DynamicBean;
import org.dba.beanunit.ExportBeanUnit;
import org.dba.dbunit.wf_Db_Connect;
import org.dba.dbunit.wf_Db_Exception;
import org.dba.dbunit.wf_Db_Server;
import org.doomdark.uuid.UUIDGenerator;

public class Util {

	Log log = LogFactory.getLog(getClass());
	private static Integer UUIDLOCK = new Integer(1);
	private static Util du = new Util();
	private String key = "filter";
	private static HashSet<String> tables = new HashSet<String>();
	private static HashSet<TreePath> stables = new HashSet<TreePath>();
	private static HashMap<String, DefaultMutableTreeNode> cacheTreepath = new HashMap<String, DefaultMutableTreeNode>();
	private static HashMap<String, String> keyInput = new HashMap<String, String>();
	public final static String REG_DIGIT = "[0-9]*";
	public final static String REG_CHAR = "[a-zA-Z]*";

	public static Util getInstance() {
		return du;
	}

	public boolean isDigit(String str) {
		return str.matches(REG_DIGIT);
	}

	public boolean isChar(String str) {
		return str.matches(REG_CHAR);
	}

	public String getKeyInput(String value) {
		synchronized (keyInput) {
			if (isDigit(value) || isChar(value)) {
				keyInput.put(key, value);
				return keyInput.get(key);
			}
		}
		return null;
	}

	public void getKeyInput() {
		synchronized (keyInput) {
			keyInput.clear();
			// return keyInput = "";
		}
	}

	public String[] getCmd(int cmd_num, String customCMD) {
		String osName = System.getProperty("os.name");
		String[] cmd = new String[cmd_num];

		if (osName.equals("Windows NT")) {
			cmd[0] = "cmd.exe";
			cmd[1] = "/C";
			cmd[2] = customCMD;
		} else if (osName.equals("Windows XP")) {
			cmd[0] = "cmd.exe";
			cmd[1] = "/C";
			cmd[2] = customCMD;
		} else if (osName.equals("Windows 95")) {
			cmd[0] = "command.com";
			cmd[1] = "/C";
			cmd[2] = customCMD;
		} else if (osName.equals("Windows 7")) {
			cmd[0] = "cmd.exe";
			cmd[1] = "/C";
			cmd[2] = customCMD;
		} else {
			cmd[0] = "cmd.exe";
			cmd[1] = "/C";
			cmd[2] = customCMD;
		}
		return cmd;
	}

	public void dropTable(wf_Db_Connect db_connect, String tName, JLabel pbarinfo) {
		StringBuffer sb = new StringBuffer();
		sb.append("DROP TABLE ");
		if (tName != null || !"".equals(tName))
			sb.append(tName.toUpperCase());
		log.info(sb.toString());
		pbarinfo.setText(sb.toString());
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(sb.toString());
			try {
				int result = stmt.executeUpdate();
				db_connect.Submit();
				log.info("Result:" + result);
				if (result == 0) {
					pbarinfo.setText("执行结果：【" + tName + "】删除成功!");
				} else {
					pbarinfo.setText("执行结果：【" + tName + "】删除失败!");
				}
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void truncateTable(wf_Db_Connect db_connect, String tName, JLabel pbarinfo) {
		StringBuffer sb = new StringBuffer();
		sb.append("TRUNCATE TABLE ");
		if (tName != null || !"".equals(tName)) {
			sb.append(tName.toUpperCase());
			log.info(sb.toString());
			pbarinfo.setText(sb.toString());
		} else {
			return;
		}
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(sb.toString());
			try {
				int result = stmt.executeUpdate();
				db_connect.Submit();
				if (result > 0) {
					pbarinfo.setText("执行结果：【" + tName + "】数据成功清除!");
				} else {
					pbarinfo.setText("执行结果：【" + tName + "】数据清除失败!");
				}
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			pbarinfo.setText(e.getMessage());
		} catch (Exception e) {
			pbarinfo.setText(e.getMessage());
		}
	}

	public void deleteTable(wf_Db_Connect db_connect, String tName, JLabel pbarinfo) {
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM ");
		if (tName != null || !"".equals(tName)) {
			sb.append(tName.toUpperCase());
			log.info(sb.toString());
			pbarinfo.setText(sb.toString());
		} else {
			return;
		}
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(sb.toString());
			try {
				int result = stmt.executeUpdate();
				db_connect.Submit();
				if (result > 0) {
					pbarinfo.setText("执行结果：【" + tName + "】数据成功清除!");
				} else {
					pbarinfo.setText("执行结果：【" + tName + "】数据清除失败!");
				}
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			pbarinfo.setText(e.getMessage());
		} catch (Exception e) {
			pbarinfo.setText(e.getMessage());
		}
	}

	public void add2Array(String value) {
		synchronized (tables) {
			if (value != null)
				tables.add(value);
		}
	}

	public void removeArray(String value) {
		synchronized (tables) {
			if (value != null)
				tables.remove(value);
		}
	}

	public void add2Sarray(TreePath value) {
		synchronized (stables) {
			if (value != null)
				stables.add(value);
		}
	}

	public void removeSarray(TreePath value) {
		synchronized (stables) {
			if (value != null)
				stables.remove(value);
		}
	}

	public int getSize() {
		synchronized (tables) {
			return tables.size();
		}
	}

	public void clearTables() {
		synchronized (tables) {
			tables.clear();
			log.info("DbUtil tables = " + tables.size());
		}
	}

	public HashSet<String> getSelectTables() {
		return tables;
	}

	public HashSet<TreePath> getSelectTreePath() {
		return stables;
	}

	public void addCacheTreePath(String Key, DefaultMutableTreeNode tp) {
		synchronized (cacheTreepath) {
			cacheTreepath.put(Key, tp);
		}
	}

	public DefaultMutableTreeNode getCacheTreePath(String Key) {
		synchronized (cacheTreepath) {
			java.util.Iterator<Entry<String, DefaultMutableTreeNode>> it = cacheTreepath.entrySet().iterator();
			while (it.hasNext()) {
				Key = Key.toUpperCase();
				Entry<String, DefaultMutableTreeNode> entity = it.next();
				String _key = entity.getKey();
				DefaultMutableTreeNode _value = entity.getValue();
				log.info("Key=" + Key);
				log.info("_Key:" + _key + " _Value:" + _value);
				if (_key.toString().trim().startsWith(Key.toString().trim())) {
					return _value;
				}
				// DbUtil.getInstance().getKeyInput();
			}
			return null;
		}
	}

	public String[] readConfig() {
		// 初始100
		String[] result = null;
		String[] temp = new String[100];
		String path = ProjectContext.getSourcePath() + "/connect_config.property";
		FileInputStream fis = null;
		BufferedReader filebuffer = null;
		try {
			fis = new FileInputStream(path);
			filebuffer = new BufferedReader(new InputStreamReader(fis, "GBK"));
			String _t = null;
			int i = 0;
			do {
				// 127.0.0.1@1521@BZPM@BZADMIN@BZADMIN@BZADMIN
				_t = filebuffer.readLine();
				if (_t != null) {
					String[] __s = _t.split("=");
					if (__s[1] != null) {
						temp[i] = __s[0];
					}
				}
				i++;
			} while (_t != null);

			int j = i - 1;
			result = new String[j];

			for (int a = 0; a < j; a++) {
				if (temp[a] != null || !temp[a].equals("")) {
					result[a] = temp[a];
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
				filebuffer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 读取person_role配置文件
	 * 
	 * @param path
	 * @return
	 */
	public List<String[]> readConfig(String path) {
		// 初始100
		List<String[]> tempList = new ArrayList<String[]>();
		FileInputStream fis = null;
		BufferedReader filebuffer = null;
		try {
			fis = new FileInputStream(path);
			filebuffer = new BufferedReader(new InputStreamReader(fis, "GBK"));
			String _t = null;
			do {
				_t = filebuffer.readLine();
				if (_t != null) {
					tempList.add(_t.split(","));
				}
			} while (_t != null);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
				filebuffer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return tempList;
	}

	public boolean build_config_file(DynamicBean db) {
		OutputStreamWriter os = null;
		String path = ProjectContext.getSourcePath() + "/connect_config.property";
		;
		try {
			File file = new File(path);
			if (!file.canRead()) {
				file = new File(file.getAbsolutePath());
			}
			os = new OutputStreamWriter(new FileOutputStream(file, true), "GBK");
			try {
				String key = db.getValue("aliais").toString();
				os.write(key + "=" + db.getValue("ip") + "@" + db.getValue("port") + "@" + db.getValue("sid") + "@" + db.getValue("user") + "@" + db.getValue("msm") + "@" + db.getValue("pwd")
						+ System.getProperty("line.separator"));
				os.flush();
				return true;
			} finally {
				os.close();
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			return false;
		}
	}

	public String getSelectConnectItem(String itemKey) {
		String path = ProjectContext.getSourcePath() + "/connect_config.property";
		FileInputStream fis = null;
		BufferedReader filebuffer = null;
		try {
			fis = new FileInputStream(path);
			filebuffer = new BufferedReader(new InputStreamReader(fis, "GBK"));
			String _t = null;
			do {
				// 127.0.0.1@1521@BZPM@BZADMIN@BZADMIN@BZADMIN
				// 192.168.3.200@BZPM=192.168.3.200@1521@BZPM@BZADMIN@BZADMIN@BZADMIN
				_t = filebuffer.readLine();
				if (_t != null) {
					String[] _s = _t.split("=");
					System.out.println(_s[0] + "   " + _s[1] + "   " + itemKey);
					if ((_s[0].toString()).equals(itemKey)) {
						return _s[1].toString();
					}
				}
			} while (_t != null);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
				filebuffer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public boolean build_config_file(ExportBeanUnit ebu) {
		OutputStreamWriter os = null;
		String path = ProjectContext.getSourcePath() + "/dbconfig.property";
		try {
			File file = new File(path);
			if (!file.canRead()) {
				file = new File(file.getAbsolutePath());
			}
			os = new OutputStreamWriter(new FileOutputStream(file), "GBK");
			try {
				os.write("LOG=off" + System.getProperty("line.separator"));
				os.write("TRACE=off" + System.getProperty("line.separator"));
				os.write("" + System.getProperty("line.separator"));
				os.write("#database properties" + System.getProperty("line.separator"));
				os.write("DATABASE_TYPE=oracle" + System.getProperty("line.separator"));
				os.write("JDBC_DRIVER=oracle.jdbc.OracleDriver" + System.getProperty("line.separator"));
				os.write("CONNECTION_URL=jdbc:oracle:thin:@" + ebu.getSip() + ":1521:" + ebu.getSid() + System.getProperty("line.separator"));
				os.write("LOGIN_ID=" + ebu.getUser() + System.getProperty("line.separator"));
				os.write("LOGIN_PASSWORD=" + ebu.getPsd() + System.getProperty("line.separator"));
				os.write("MIN_CONNECTION=10" + System.getProperty("line.separator"));
				os.write("MAX_CONNECTION=15" + System.getProperty("line.separator"));
				os.write("MAX_CONNECT_TIME=60" + System.getProperty("line.separator"));
				os.write("IDLETIME=2" + System.getProperty("line.separator"));
				os.write("DB_LOG_FILE=bzserver.log" + System.getProperty("line.separator"));
				os.flush();
				return true;
			} finally {
				os.close();
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			return false;
		}
	}

	public String getDateTime(long millis) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dataTime = sdf.format(millis);
		return dataTime;
	}

	public String getNoDateTime(long millis) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String dataTime = sdf.format(millis);
		return dataTime;
	}

	private static String toHexUtil(int n) {
		String rt = "";
		switch (n) {
		case 10:
			rt += "A";
			break;
		case 11:
			rt += "B";
			break;
		case 12:
			rt += "C";
			break;
		case 13:
			rt += "D";
			break;
		case 14:
			rt += "E";
			break;
		case 15:
			rt += "F";
			break;
		default:
			rt += n;
		}
		return rt;
	}

	public static String toHex(int n) {
		StringBuilder sb = new StringBuilder();
		if (n / 16 == 0) {
			return toHexUtil(n);
		} else {
			String t = toHex(n / 16);
			int nn = n % 16;
			sb.append(t).append(toHexUtil(nn));
		}
		return sb.toString();
	}

	public static String parseAscii(String str) {
		StringBuilder sb = new StringBuilder();
		byte[] bs = str.getBytes();
		for (int i = 0; i < bs.length; i++)
			sb.append(toHex(bs[i]));
		return sb.toString();
	}

	public static String StringToAsciiString(String content) {
		String result = "";
		int max = content.length();
		for (int i = 0; i < max; i++) {
			char c = content.charAt(i);
			String b = Integer.toHexString(c);
			result = result + b;
		}
		return result;
	}

	public static boolean changeFile(String fName, String destinfo, int start, int len) {
		// 创建一个随机读写文件对象
		java.io.RandomAccessFile raf = null;
		java.nio.MappedByteBuffer buffer = null;
		java.nio.channels.FileChannel channel = null;
		try {
			if (destinfo != null) {
				raf = new java.io.RandomAccessFile(fName, "rw");
				// 打开一个文件通道
				channel = raf.getChannel();
				// 映射文件中的某一部分数据以读写模式到内存中
				buffer = channel.map(FileChannel.MapMode.READ_WRITE, start, len);
				byte[] dest = destinfo.getBytes();
				buffer.put(dest, 0, len);
				buffer.force();// 强制输出,在buffer中的改动生效到文件
				buffer.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				channel.close();
				raf.close();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	/**
	 * 用户信息
	 * 
	 * @param con
	 * @param UserSet
	 * @param info
	 * @return
	 */
	public void addUserSet(wf_Db_Connect db_connect, final String[] u, final JLabel info) throws Exception {
		String Create_SQL = "INSERT INTO USERSET (PERSONID,PERSONCODE,PERSONNAME,ORGID,SELEID,COMPANYID)VALUES(?,?,?,?,?,?)";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(Create_SQL);
			try {
				db_connect.setHas_update(true);
				String personid = u[0];
				String personcode = u[1];
				String orgid = u[2];
				if (personid != null) {
					stmt.setString(1, personid);
				}
				if (personcode != null) {
					stmt.setString(2, personcode);
					stmt.setString(3, u[3]);
				}
				if (orgid != null) {
					stmt.setString(4, orgid);
				}
				stmt.setString(5, "XGMJ01");
				stmt.setString(6, "0000");
				if (stmt.executeUpdate() != 1) {
					throw new Exception("USERSET插入失败!");
				} else {
					addUserORGSet(db_connect, u, info);
				}
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new Exception(e);
		}
	}

	/**
	 * 用户组织
	 * 
	 * @param con
	 * @param UserORGSet
	 * @return
	 */
	private void addUserORGSet(wf_Db_Connect db_connect, final String[] u, final JLabel info) throws Exception {
		String Create_SQL = "INSERT INTO USERORGSET (PERSONID,PERSONCODE,PERSONNAME,ORGID) VALUES(?,?,?,?)";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(Create_SQL);
			try {
				db_connect.setHas_update(true);
				String personid = u[0];
				String personcode = u[1];
				String orgid = u[2];
				if (personid != null) {
					stmt.setString(1, personid);
				}
				if (personcode != null) {
					stmt.setString(2, personcode);
					stmt.setString(3, u[3]);
				}
				if (orgid != null) {
					stmt.setString(4, orgid);
				}
				if (stmt.executeUpdate() != 1) {
					throw new Exception("USERORGSET插入失败!");
				} else {
					addPersonSet(db_connect, u, info);
				}
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new Exception(e);
		}
	}

	/**
	 * 登录账号信息
	 * 
	 * @param con
	 * @param PersonSet
	 * @return
	 */
	public void addPersonSet(wf_Db_Connect db_connect, final String[] u, final JLabel info) throws Exception {
		String Create_SQL = "INSERT INTO PERSON_SET (PERSONID,PERSONCODE,PERSONNAME,PERSONTYPE,PERSONPASSWORD,ORGID,ISFINALLY,ISUSED) VALUES(?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(Create_SQL);
			try {
				db_connect.setHas_update(true);
				String personid = u[0];
				String personcode = u[1];
				String orgid = u[2];
				if (personid != null) {
					stmt.setString(1, personid);
				}
				if (personcode != null) {
					stmt.setString(2, personcode);
					stmt.setString(3, u[3]);
				}
				stmt.setString(4, "PERSON");// type
				stmt.setString(5, compile(u[4]));// pwd
				if (orgid != null) {
					stmt.setString(6, orgid);
				}
				stmt.setString(7, "Y");
				stmt.setString(8, "Y");
				stmt.executeUpdate();
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new Exception(e);
		}
	}

	/**
	 * 人员与角色 安全员 ROLEID:E791B82F-4823-4CC9-8911-83353D11EFA5
	 * personid:-9223372036854775805 ISFINALLY:Y ISUSED:Y ISAUTH:N ISEXE:Y //安全员
	 * 
	 * @param con
	 * @param PersonRole
	 * @return
	 */
	public void addPersonRole(wf_Db_Connect db_connect, final String[] u, final JLabel info) throws Exception {
		String Create_SQL = "INSERT INTO PERSON_ROLE (ROLEID,PERSONID,ISFINALLY,ISUSED,ISAUTHORIZE,ISEXECUTE) VALUES(?,?,?,?,?,?)";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(Create_SQL);
			try {
				db_connect.setHas_update(true);
				// ROLEID,PERSONID,ISFINALLY,ISUSED,ISAUTHORIZE,ISEXECUTE
				stmt.setString(1, u[0]);// type
				stmt.setString(2, u[1]);// type
				stmt.setString(3, u[2]);// pwd
				stmt.setString(4, u[3]);
				stmt.setString(5, u[4]);
				stmt.setString(6, u[5]);
				stmt.executeUpdate();
			} finally {
				stmt.close();
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 人员组织
	 * @param db_connect
	 * @param u
	 * @param info
	 * @throws Exception
	 */
	public void addPersonOrg(wf_Db_Connect db_connect, final String pid,final String orgid, final JLabel info) throws Exception {
		String Create_SQL = "INSERT INTO PERSON_ORG (PERSONID,ORGID,ISFINALLY,ISUSED) VALUES(?,?,?,?)";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(Create_SQL);
			try {
				db_connect.setHas_update(true);
				stmt.setString(1, pid);
				stmt.setString(2, orgid);
				stmt.setString(3, "Y");
				stmt.setString(4, "Y");
				stmt.executeUpdate();
			} finally {
				stmt.close();
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	/**
	 * 人员岗位设置
	 * 
	 * @param db_connect
	 * @param u
	 * @param info
	 */
	public void addPersonStationOrg(wf_Db_Connect db_connect, final String[] u, final JLabel info) throws Exception {
		String Create_SQL = "INSERT INTO PERSON_STATION_ORG (ORGID,STATIONID,PERSONID,ISUSED,ISFINALLY,ISAUTHORIZE,ISEXECUTE) VALUES(?,?,?,?,?,?,?)";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(Create_SQL);
			try {
				db_connect.setHas_update(true);
				// ROLEID,PERSONID,ISFINALLY,ISUSED,ISAUTHORIZE,ISEXECUTE
				stmt.setString(1, u[0]);// type
				stmt.setString(2, u[1]);// type
				stmt.setString(3, u[2]);// pwd
				stmt.setString(4, u[3]);
				stmt.setString(5, u[4]);
				stmt.setString(6, u[5]);
				stmt.setString(7, u[6]);
				stmt.executeUpdate();
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new Exception(e);
		}
	}

	public void deleteEsbDanweiHttp(wf_Db_Connect db_connect, final JLabel info) throws Exception {
		String delSQL = "DELETE FROM ESB_DANWEIHTTP";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(delSQL);
			try {
				db_connect.setHas_update(true);
				stmt.executeUpdate();
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new Exception(e);
		}
	}

	public void updateEsbDanweiHttp(wf_Db_Connect db_connect, final String[] u, final JLabel info) throws Exception {
		String Create_SQL = "INSERT INTO ESB_DANWEIHTTP (DANWEIID,DANWEINAME,URL) VALUES(?,?,?)";
		try {
			PreparedStatement stmt = db_connect.getConnect().prepareStatement(Create_SQL);
			try {
				db_connect.setHas_update(true);
				stmt.setString(1, u[0]);// type
				stmt.setString(2, u[1]);// type
				stmt.setString(3, u[2]);// pwd
				stmt.executeUpdate();
			} finally {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new Exception(e);
		}
	}

	/**
	 * SoftfanUtil中MD5加密算法
	 * 
	 * @param s
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public String compile(String s) throws NoSuchAlgorithmException {
		char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		byte[] tmp = s.getBytes();
		MessageDigest dig = MessageDigest.getInstance("MD5");
		dig.update(tmp);
		byte[] md = dig.digest();
		int j = md.length;
		char[] str = new char[j * 2];
		int k = 0;
		for (int i = 0; i < j; i++) {
			byte byte0 = md[i];
			str[(k++)] = hex[(byte0 >>> 4 & 0xF)];
			str[(k++)] = hex[(byte0 & 0xF)];
		}
		return new String(str);
	}

	public static void main(String[] args) {
//		String path = "G:/DBT/DBT/source/PERSON_STATION_ORG.property";
//		List<String[]> d = Util.getInstance().readConfig(path);
//		List<String[]> _t = new ArrayList<String[]>();
//		for (String[] a : d) {
//			for (int i = 0; i < a.length; i++) {
//				if (i == 0)
//					a[0] = "0296";
//				if (i == 2)
//					a[2] = "1111";
//			}
//			_t.add(a);
//		}
//		int b = 0;
//		for (String[] a1 : _t) {
//			b++;
//			System.out.println(b + ":" + a1[0] + ":" + a1[1] + ":" + a1[2]);
//		}
		try {
			System.out.println(Util.getInstance().compile("game!@#!A)"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 替换PERSON_ROLE表中的personid
	 * 
	 * @param path
	 * @param personid
	 * @return
	 */
	public List<String[]> replacePersonRole(String path, String personid) {
		List<String[]> d = Util.getInstance().readConfig(path);
		List<String[]> _t = new ArrayList<String[]>();
		for (String[] a : d) {
			for (int i = 0; i < a.length; i++) {
				// i=1时,是personid
				if (i == 1) {
					a[1] = personid;
				}
			}
			_t.add(a);
		}
		return _t;
	}

	/**
	 * 替换Person_Station_Org表中的personid
	 * 
	 * @param path
	 * @param personid
	 * @return
	 */
	public List<String[]> replacePersonStationOrg(String path, String personid, String orgid) {
		List<String[]> d = Util.getInstance().readConfig(path);
		List<String[]> _t = new ArrayList<String[]>();
		for (String[] a : d) {
			for (int i = 0; i < a.length; i++) {
				if (i == 0)
					a[0] = orgid;
				if (i == 2)
					a[2] = personid;
			}
			_t.add(a);
		}
		return _t;
	}

	public static String generate() {
		synchronized (UUIDLOCK) {
			return UUIDGenerator.getInstance().generateRandomBasedUUID().toString();
		}
	}

	public static String generate(String id) {
		if ((id == null) || (id.equals(""))) {
			return UUIDGenerator.getInstance().generateRandomBasedUUID().toString();
		}
		return id;
	}
	
	public static void zip(String sourcesFile,String destFile,JLabel info){
		ZipOutputStream zos = null;
		InputStream is = null;
		if(sourcesFile.equals("")||sourcesFile == null){
			info.setText("源文件不能为空");
			return;
		}
		if(destFile.equals("")||destFile == null){
			info.setText("生成的目标文件不能为空");
			return;
		}
		try {
			zos = new ZipOutputStream(new FileOutputStream(destFile));
			// zos.setMethod(ZipOutputStream.STORED);
			// 实例化一个名称为ab.txt的ZipEntry对象
			File file = new File(sourcesFile);
			ZipEntry entry = new ZipEntry(file.getName());
			// 设置注释
			// zos.setComment(new String("zip测试for单个文件".getBytes(),"GBK"));
			// 把生成的ZipEntry对象加入到压缩文件中，而之后往压缩文件中写入的内容都会放在这个ZipEntry对象里面
			zos.putNextEntry(entry);
			is = new FileInputStream(sourcesFile);
			byte[] b = new byte[1024];
			int len = 0;
			info.setText("正在压缩数据包...");
			while ((len = is.read(b)) != -1) {
				zos.write(b, 0, len);
			}
			info.setText("备份文件压缩完成!");
			is.close();
			zos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
