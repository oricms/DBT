package org.dba.service;

import java.io.File;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.dba.util.DbaXml;
import org.dba.util.FrameManager;
import org.dba.util.ProjectContext;

public class startService {

	private static void init() throws Exception {
		ProjectContext.root = new File(".").getCanonicalPath() + File.separator;
		initLib();
		initConfig();
		initService();
	}

	private static void initService() {
		CoreService.getInstance();
	}

	private static void initLib() throws Exception {
	}

	private static void initConfig() throws Exception {
		DbaXml.read(new File(ProjectContext.getSourcePath() + "dba.xml"));
	}

	public void startup(String[] args, ClassLoader classLoader) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			init();
			FrameManager.getInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void shutdown(String[] args, ClassLoader classLoader) {

	}

}
