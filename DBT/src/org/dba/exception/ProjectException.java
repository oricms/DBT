package org.dba.exception;


public class ProjectException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private Throwable cause = null;
	
	public ProjectException() {
		super();
	}
	
	public ProjectException(String message) {
		super(message);
	}
	
	public ProjectException(Throwable cause) {
		super();
		this.cause = cause;
	}
	
	public ProjectException(String message, Throwable cause) {
		super(message);
		this.cause = cause;
	}
	
	public String getMessage() {
		if (super.getMessage() != null) {
			return super.getMessage();
		}else if ( cause != null ) {
			return cause.toString();
		}else {
			return null;
		}
	}

	public Throwable getThrowable() {
		return cause;
	}

	

}
