package org.dba.test;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;

public class Imgtest extends JFrame {

	Imgtest() {
		super("图片测试窗口");
		this.setSize(500, 500);
		this.setBackground(Color.BLUE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImageIcon img = new ImageIcon("G:\\DBT\\DBT\\image\\database_connect.png");// 创建图片组件，制定图片地址
		final Border emptyBorder = BorderFactory.createEmptyBorder(0, 0, 0, 0);

		final Color roverBorderColor = Color.gray;
		final Border roverBorder = new Border() {

			public boolean isBorderOpaque() {
				return true;
			}

			@Override
			public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
				g.setColor(roverBorderColor);
				g.drawRect(x, y, width - 1, height - 1);
			}

			@Override
			public Insets getBorderInsets(Component c) {
				return new Insets(1, 1, 1, 1);
			}
		};
		
		final JButton imgButton = new JButton(img);// 将图片组建作为参数传给JButton
		
		imgButton.setOpaque(false);
		imgButton.setIcon(img);
		imgButton.setBorder(emptyBorder);  
		imgButton.setContentAreaFilled(false);  
		imgButton.setFocusPainted(false);  
		imgButton.setRolloverEnabled(true);
		imgButton.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseEntered(MouseEvent e) {
				if (isEnabled()) {
					imgButton.setBorder(roverBorder);
					imgButton.setRolloverEnabled(true);
				}
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("11111111111111");
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (isEnabled()) {
					imgButton.setBorder(emptyBorder);
				}
			}
		});
		JPanel panel = new JPanel();
		panel.add(imgButton);

		this.setContentPane(panel);

	}

	public static void main(String[] args) {
		Imgtest i = new Imgtest();
		i.show();
	}
}