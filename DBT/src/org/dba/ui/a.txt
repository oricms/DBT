class ImportInfoPanel extends JDialog implements WindowListener {

        /**
         * 
         */
        private static final long serialVersionUID = 6162899017966256858L;

        JTextArea exportinfo = new JTextArea();

        public ImportInfoPanel(SuperFrame fSuper) {
            super(fSuper);
            initGUI();
        }

        private void initGUI() {
            this.setTitle("数据库导入");
            this.setSize(550, 300);
            this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            Font x = new Font("Serif", 0, 12);
            exportinfo.setFont(x);
            JScrollPane jsp = new JScrollPane(exportinfo);
            getContentPane().add(jsp, "Center");
            this.setLocationRelativeTo(null);
            this.setAlwaysOnTop(true);
            this.addWindowListener((WindowListener) this);
            Thread t = new Thread(new Runnable() {
                public void run() {
                    try {
                        FrameManager.F_SUPER.setEnabled(false);
                        File sqlfile = new File(ProjectContext.getSourcePath() + "user.sql");
                        BufferedWriter bw = null;
                        try {
                            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(sqlfile)));
                            bw.write("drop user " + user + " cascade;\n");
                            bw.write("create user " + user + " identified by " + psd + ";\n");
                            bw.write("grant dba,connect,resource to " + user + ";\n");
                            bw.write("commit;\n");
                            bw.write("exit;");
                        } finally {
                            if (bw != null)
                                bw.close();
                        }
                        
                        String usrcmd = ProjectContext.getWinExpPath()+"sqlplus.exe " + admin + "/" + adminpsd + "@" + sid + " @" + sqlfile.getCanonicalPath();
                        Process process = Runtime.getRuntime().exec(DbUtil.getCmd(usrcmd));
                        InputStreamReader isr = new InputStreamReader(process.getInputStream());
                        BufferedReader br = new BufferedReader(isr);
                        String line = null;
                        try {
                            while ((line = br.readLine()) != null) {
                                if (!line.equals("") || line != null) {
                                    exportinfo.append(line + "\n");
                                    exportinfo.setSelectionStart(exportinfo.getText().length());
                                }
                            }
                        } finally {
                            if (isr != null)
                                isr.close();
                            if (br != null)
                                br.close();
                        }
                        
                        String impcmd =  ProjectContext.getWinExpPath()+"imp " + user + "/" + psd + "@" + sid + " file=" + file + " full=y commit=y ignore=y log=" + ProjectContext.getRoot() + "db.log";
                        process = Runtime.getRuntime().exec(DbUtil.getCmd(impcmd));
                        InputStreamReader isr1 = new InputStreamReader(process.getErrorStream());
                        BufferedReader br1 = new BufferedReader(isr1);
                        String lineinfo = null;
                        try {
                            while ((lineinfo = br1.readLine()) != null) {
                                if (!lineinfo.equals("") || lineinfo != null) {
                                    exportinfo.append(lineinfo + "\n");
                                    exportinfo.setSelectionStart(exportinfo.getText().length());
                                }
                            }
                        } finally {
                            if (isr1 != null)
                                isr1.close();
                            if (br1 != null)
                                br1.close();
                        }
                        if (sqlfile.exists())
                            sqlfile.delete();
                    } catch (Exception exp) {
                        exp.printStackTrace();
                        JOptionPane.showMessageDialog(FrameManager.F_SUPER, "出现错误: " + exp);
                    } finally {
                        FrameManager.F_SUPER.setEnabled(true);
                    }
                }
            });
            t.start();
        }

        @Override
        public void windowOpened(WindowEvent e) {

        }

        @Override
        public void windowClosing(WindowEvent e) {

        }

        @Override
        public void windowClosed(WindowEvent e) {
        }

        @Override
        public void windowIconified(WindowEvent e) {

        }

        @Override
        public void windowDeiconified(WindowEvent e) {
        }

        @Override
        public void windowActivated(WindowEvent e) {

        }

        @Override
        public void windowDeactivated(WindowEvent e) {
            if (!this.isVisible()) {
                startImport.setEnabled(true);
            }

        }
    }