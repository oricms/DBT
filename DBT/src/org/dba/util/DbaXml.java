package org.dba.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.dba.exception.ProcessException;
import org.dba.exception.ProjectException;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class DbaXml{

	public static final ImportDba IMP = new ImportDba();
	public static final ExportDba EXP = new ExportDba();
	public static final CompareDba CDD = new CompareDba();

	public static class ImportDba {
		public String admin = "";
		public String user = "";
		public String sid = "";
		public String file = "";

		private ImportDba() {
		}
	}

	public static class ExportDba {
		public String user = "";
		public String sid = "";
		public String file = "";
		public String sip = "";
		public String logfile = "";
		public String pwd = "";
		public String islog = "";
		public String compress = "";
		public String indexes = "";
		public String usermodel = "";
		public String fullmodel = "";
		public String zl = "";
		public String instanceName = "";

		private ExportDba() {
		}
	}

	public static class CompareDba {
		public String src_server_ip;
		public String src_user;
		public String src_pwd;
		public String src_sid;
		public String src_Instance;
		public String dest_server_ip;
		public String dest_user;
		public String dest_pwd;
		public String dest_sid;
		public String dest_Instance;
		public String indexs;
		public String trigger;
		public String savedata;
		public String buildsql;
		public String imexec;
		public String openlog;
		public String charset;
		public String comparepath;
		public String lxsource;
	}

	public static void read(File file) {
		try {
			SAXReader saxr = new SAXReader();
			parseXml(saxr.read(file));
		} catch (Exception e) {
			throw (e instanceof RuntimeException) ? (RuntimeException) e : new ProjectException(e);
		}
	}

	public static void read(InputStream is) {
		try {
			SAXReader saxr = new SAXReader();
			parseXml(saxr.read(is));
		} catch (Exception e) {
			throw (e instanceof RuntimeException) ? (RuntimeException) e : new ProjectException(e);
		}
	}

	private static void parseXml(Document doc) throws Exception {
		Element root = doc.getRootElement();
		if (root == null)
			return;
		List contentList = root.content();
		for (int i = 0; i < contentList.size(); i++) {
			Object dom = contentList.get(i);
			if (!(dom instanceof Element))
				continue;
			Element el = (Element) dom;
			String elname = el.getName();
			if (elname.equals("import")) {
				parseImport(el.content());
			} else if (elname.equals("export")) {
				parseExport(el.content());
			} else if (elname.equals("compare")) {
				parseCompare(el.content());
			}
		}
	}

	/**
	 * public String src_server_ip; public String src_user; public String
	 * src_pwd; public String src_sid; public String src_Instance; public String
	 * dest_server_ip; public String dest_user; public String dest_pwd; public
	 * String dest_sid; public String dest_Instance; public String indexs;
	 * public String trigger; public String savedata; public String buildsql;
	 * public String imexec; public String openlog; public String charset;
	 * 
	 * @param contentList
	 */
	private static void parseCompare(List contentList) {
		if (contentList == null)
			return;
		for (int i = 0; i < contentList.size(); i++) {
			Object dom = contentList.get(i);
			if (!(dom instanceof Element))
				continue;
			Element el = (Element) dom;
			String name = el.getName();
			String value = el.getText();
			if (name.equals("src_server_ip")) {
				CDD.src_server_ip = value;
			} else if (name.equals("src_user")) {
				CDD.src_user = value;
			} else if (name.equals("src_pwd")) {
				CDD.src_pwd = value;
			} else if (name.equals("src_sid")) {
				CDD.src_sid = value;
			} else if (name.equals("src_Instance")) {
				CDD.src_Instance = value;
			} else if (name.equals("dest_server_ip")) {
				CDD.dest_server_ip = value;
			} else if (name.equals("dest_user")) {
				CDD.dest_user = value;
			} else if (name.equals("dest_pwd")) {
				CDD.dest_pwd = value;
			} else if (name.equals("dest_sid")) {
				CDD.dest_sid = value;
			} else if (name.equals("dest_Instance")) {
				CDD.dest_Instance = value;
			} else if (name.equals("indexs")) {
				CDD.indexs = value;
			} else if (name.equals("trigger")) {
				CDD.trigger = value;
			} else if (name.equals("savedata")) {
				CDD.savedata = value;
			} else if (name.equals("buildsql")) {
				CDD.buildsql = value;
			} else if (name.equals("imexec")) {
				CDD.imexec = value;
			} else if (name.equals("openlog")) {
				CDD.openlog = value;
			} else if (name.equals("charset")) {
				CDD.charset = value;
			}else if(name.equals("comparepath")){
				CDD.comparepath = value;
			}else if(name.equals("lxsource")){
				CDD.lxsource = value;
			}
		}

	}

	private static void parseImport(List contentList) {
		if (contentList == null)
			return;
		for (int i = 0; i < contentList.size(); i++) {
			Object dom = contentList.get(i);
			if (!(dom instanceof Element))
				continue;
			Element el = (Element) dom;
			String name = el.getName();
			String value = el.getText();
			if (name.equals("admin")) {
				IMP.admin = value;
			} else if (name.equals("user")) {
				IMP.user = value;
			} else if (name.equals("sid")) {
				IMP.sid = value;
			} else if (name.equals("file")) {
				IMP.file = value;
			}
		}
	}

	private static void parseExport(List contentList) {
		if (contentList == null)
			return;
		for (int i = 0; i < contentList.size(); i++) {
			Object dom = contentList.get(i);
			if (!(dom instanceof Element))
				continue;
			Element el = (Element) dom;
			String name = el.getName();
			String value = el.getText();
			if (name.equals("user")) {
				EXP.user = value;
			} else if(name.equals("pwd")){
				EXP.pwd = value;
			}else if (name.equals("sid")) {
				EXP.sid = value;
			} else if (name.equals("file")) {
				EXP.file = value;
			} else if (name.equals("sip")) {
				EXP.sip = value;
			} else if (name.equals("logfile")) {
				EXP.logfile = value;
			} else if (name.equals("islog")) {
				EXP.islog = value;
			} else if (name.equals("compress")) {
				EXP.compress = value;
			} else if (name.equals("indexes")) {
				EXP.indexes = value;
			} else if (name.equals("usermodel")) {
				EXP.usermodel = value;
			} else if (name.equals("fullmodel")) {
				EXP.fullmodel = value;
			} else if (name.equals("zl")) {
				EXP.zl = value;
			}else if (name.equals("instanceName")) {
				EXP.instanceName = value;
			}
		}
	}

	public static void write(File file) {
		try {
			write(new FileOutputStream(file));
		} catch (Exception e) {
			throw (e instanceof RuntimeException) ? (RuntimeException) e : new ProjectException(e);
		}
	}

	public static void write(OutputStream os) throws Exception {
		try {
			Document doc = DocumentHelper.createDocument();
			Element root = doc.addElement("root");
			Element impel = root.addElement("import");
			impel.addElement("admin").setText(IMP.admin);
			impel.addElement("user").setText(IMP.user);
			impel.addElement("sid").setText(IMP.sid);
			impel.addElement("file").setText(IMP.file);
			
			Element expel = root.addElement("export");
			expel.addElement("user").setText(EXP.user);
			expel.addElement("pwd").setText(EXP.pwd);
			expel.addElement("sid").setText(EXP.sid);
			expel.addElement("file").setText(EXP.file);
			expel.addElement("logfile").setText(EXP.logfile);
			expel.addElement("sip").setText(EXP.sip);
			expel.addElement("islog").setText(EXP.islog);
			expel.addElement("compress").setText(EXP.compress);
			expel.addElement("indexes").setText(EXP.indexes);
			expel.addElement("usermodel").setText(EXP.usermodel);
			expel.addElement("fullmodel").setText(EXP.fullmodel);
			expel.addElement("zl").setText(EXP.zl);
			expel.addElement("instanceName").setText(EXP.instanceName);
			/*
			 * <src_server_ip></src_server_ip> <src_user></src_user>
			 * <src_pwd></src_pwd> <src_sid></src_sid>
			 * <src_Instance></src_Instance> <dest_server_ip></dest_server_ip>
			 * <dest_user></dest_user> <dest_pwd></dest_pwd>
			 * <dest_sid></dest_sid> <dest_Instance></dest_Instance>
			 * <indexs></indexs> <trigger></trigger> <savedata></savedata>
			 * <buildsql></buildsql> <imexec></imexec> <openlog></openlog>
			 * <charset></charset>
			 */
			Element compare = root.addElement("compare");
			compare.addElement("src_server_ip").setText(CDD.src_server_ip);
			compare.addElement("src_user").setText(CDD.src_user);
			compare.addElement("src_pwd").setText(CDD.src_pwd);
			compare.addElement("src_sid").setText(CDD.src_sid);
			compare.addElement("src_Instance").setText(CDD.src_Instance);
			compare.addElement("dest_server_ip").setText(CDD.dest_server_ip);
			compare.addElement("dest_user").setText(CDD.dest_user);
			compare.addElement("dest_pwd").setText(CDD.dest_pwd);
			compare.addElement("dest_sid").setText(CDD.dest_sid);
			compare.addElement("dest_Instance").setText(CDD.dest_Instance);
			compare.addElement("indexs").setText(CDD.indexs);
			compare.addElement("trigger").setText(CDD.trigger);
			compare.addElement("savedata").setText(CDD.savedata);
			compare.addElement("buildsql").setText(CDD.buildsql);
			compare.addElement("imexec").setText(CDD.imexec);
			compare.addElement("openlog").setText(CDD.openlog);
			compare.addElement("charset").setText(CDD.charset);
			compare.addElement("comparepath").setText(CDD.comparepath);
			compare.addElement("lxsource").setText(CDD.lxsource);
			OutputFormat of = null;
			BufferedOutputStream bos = null;
			XMLWriter xw = null;
			try {
				bos = new BufferedOutputStream(os);
				of = OutputFormat.createPrettyPrint();
				of.setEncoding("GBK");
				xw = new XMLWriter(bos, of);
				xw.write(doc);
			} finally {
				if (xw != null)
					xw.close();
				if (bos != null)
					bos.close();
			}
		} catch (Exception e) {
			throw (e instanceof RuntimeException) ? (RuntimeException) e : new ProjectException(e);
		}
	}

}
