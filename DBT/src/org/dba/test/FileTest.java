package org.dba.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;

import org.dba.util.Util;

public class FileTest {

	public static void main(String[] args) {
		File f = new File("d:/备份/a.dmp");
		// try {
		// FileInputStream fis = new FileInputStream(f);
		// BufferedReader br = new BufferedReader(new InputStreamReader(fis,
		// "GBK"));
		// String version = br.readLine();
		// System.out.println(version);
		// br.close();
		// fis.close();
		//
		// FileOutputStream fos = new FileOutputStream(f);
		// OutputStreamWriter os = new OutputStreamWriter(fos, "GBK");
		// os.write("version 10.0");
		// os.flush();
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// } catch (UnsupportedEncodingException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		try {
			changeFile("d:/备份/a.dmp", "V10.11.01", 10, 9);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(" change OK... ");

	}

	/**
	 * 修改文件中的某一部分的数据测试:将字定位置的字母改为大写
	 * 
	 * @param fName
	 *            :要修改的文件名字
	 * @param start
	 *            :起始字节
	 * @param len
	 *            :要修改多少个字节
	 * @return :是否修改成功
	 * @throws Exception
	 *             :文件读写中可能出的错
	 * @author javaFound
	 */
	public static boolean changeFile(String fName, String destinfo, int start, int len) {
		// 创建一个随机读写文件对象
		java.io.RandomAccessFile raf = null;
		java.nio.MappedByteBuffer buffer = null;
		java.nio.channels.FileChannel channel = null;
		try {
			if (destinfo != null) {
				raf = new java.io.RandomAccessFile(fName, "rw");
				// 打开一个文件通道
				channel = raf.getChannel();
				// 映射文件中的某一部分数据以读写模式到内存中
				buffer = channel.map(FileChannel.MapMode.READ_WRITE, start, len);
				// byte[] src = buffer.array();
				byte[] dest = destinfo.getBytes();
				buffer.put(dest, 0, len);
				buffer.force();// 强制输出,在buffer中的改动生效到文件
				buffer.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				channel.close();
				raf.close();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

}
