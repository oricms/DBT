package org.dba.dbunit;

import java.sql.Connection;
import java.sql.SQLException;

public class wf_Db_Connect {
	private wf_Db_Factory factory;
	private Connection connection;

	public wf_Db_Connect(Connection connection, wf_Db_Factory factory) {
		this.connection = connection;
		this.factory = factory;
	}

	public boolean isIs_close() {
		return this.connection == null;
	}

	public Connection getConnect() {
		return this.connection;
	}

	private void freeLob() {
	}

	public void close() {
		try {
			freeLob();
		} finally {
			try {
				if (this.factory != null)
					this.factory.free(this);
			} finally {
				this.connection = null;
			}
		}
	}

	public void Submit() throws Exception {
		commit();
		freeLob();
	}

	private void commit() throws Exception {
		if (this.connection == null)
			throw new Exception("数据连接已关闭,不能提交");
		try {
			this.connection.commit();
		} catch (SQLException se) {
			throw new Exception(se);
		} catch (Exception e) {
			throw new Exception(e);
		} catch (Throwable e) {
			throw new Exception(e.getMessage());
		}
	}

	public void Shutdown() throws Exception {
		try {
			if (this.connection != null)
				try {
					this.connection.rollback();
				} catch (SQLException e) {
					throw new Exception(e);
				} catch (RuntimeException e) {
					throw new Exception(e);
				} catch (Exception e) {
					throw new Exception(e);
				} catch (Throwable e) {
					throw new Exception(e.getMessage());
				} finally {
					freeLob();
				}
		} finally {
			this.connection = null;
		}
	}

	public wf_Db_Factory getFactory() {
		return this.factory;
	}

	public void setHas_update(boolean b) {
		// TODO Auto-generated method stub
		
	}
}