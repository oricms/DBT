package org.dba.test;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import junit.framework.TestCase;

public class ProcessfileTest extends TestCase {

	Log log = LogFactory.getLog(this.getClass());

	public void testSubstring() {
		String a = ",BZADMIN@P04GTXMJGYSZJBG@0,YSZJBGID@VARCHAR2@64@0@0,YSZJGS@VARCHAR2@2000@0@1,YSZJBG@VARCHAR2@50@0@1,YSZJBGFILENAME@VARCHAR2@50@0@1,JGYSSQID@VARCHAR2@64@0@0,XMXXID@VARCHAR2@50@0@0,SBR@VARCHAR2@50@0@0,SBDW@VARCHAR2@50@0@0,JHYSSJ@VARCHAR2@50@0@1,=end=";
		String a1 = "BZADMIN@GSQLJH@1";
		// System.out.println(a.substring(1, a.length()-5));
		// System.out.println(a1.substring(0,a1.length()-2));
	}

	@Override
	protected void runTest() throws Throwable {
		// Cacl();

		listCompare();
		super.runTest();
	}

	private void listCompare() {
		List<String> l1 = new ArrayList<String>();
		List<String> l2 = new ArrayList<String>();

		l1.add("BZADMIN@ACTIVITY_OWNER_SET@0");
		l1.add("PROCESSID@VARCHAR2@50@0@0");
		l1.add("MPID@VARCHAR2@50@0@0");
		l1.add("MAID@VARCHAR2@50@0@1");
		l1.add("PERSONID@VARCHAR2@2@0@0");
		l1.add("ID@VARCHAR2@2@0@0");

		l2.add("BZADMIN@ACTIVITY_OWNER_SET@0");
		l2.add("PROCESSID@VARCHAR2@50@0@0");
		l2.add("MPID@VARCHAR2@50@0@0");
		l2.add("MAID@VARCHAR2@50@0@0");
		l2.add("PERSONID@NUMBER@2@0@0");

		HashMap<String, String> s = new HashMap<String, String>();
		HashMap<String, String> d = new HashMap<String, String>();

		for (String _sf : l1) {
			if (_sf != null) {
				if (!l2.contains(_sf)) {
					String[] __sf = _sf.split("@");
					s.put(__sf[0], _sf);
				}
			}
		}

		for (String _tg : l2) {
			String[] __tg = _tg.split("@");
			d.put(__tg[0], _tg);

		}
		// 针对不存在的数据进行进一步的细分
		Iterator<Entry<String, String>> it = s.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> next = it.next();
			String key = next.getKey();
			String __svalue = next.getValue();
			String __dvalue = d.get(key);
			if (__dvalue == null) {
				String[] __sf = __svalue.split("@");
				log.info(" src:filed = " + __sf[0]);
			} else {
				String[] __sf = __svalue.split("@");
				String[] __tg = __dvalue.split("@");
				if (!__tg[1].equals(__sf[1])) {
					log.info(__sf[0] + " target:type = " + __tg[1] + " src:type = " + __sf[1]);
				}
				if (!__tg[2].equals(__sf[2])) {
					log.info(__sf[0] + " target:length = " + __tg[2] + " src:length = " + __sf[2]);
				}
				if (!__tg[3].equals(__sf[3])) {
					log.info(__sf[0] + " target:digits = " + __tg[3] + " src:digits = " + __sf[3]);
				}
				if (!__tg[4].equals(__sf[4])) {
					log.info(__sf[0] + " target:nullable = " + __tg[4] + " src:nullable = " + __sf[4]);
				}
			}
		}
		
		d.clear();
		s.clear();

	}

	public void Cacl() {
		//
		List<File[]> lf = new LinkedList<File[]>();
		// // 计算总任务
		// // "E:\\oracl"
		File file = new File("E:\\oracl");
		int totalwork = 0;
		File[] files = null;
		if (file.isDirectory()) {
			files = file.listFiles();
			FilenameFilter srcfilter = new FilenameFilter() {

				public boolean isSrcprefix(String fileName) {
					if (fileName.startsWith("src"))
						return true;
					return false;
				}

				@Override
				public boolean accept(File dir, String name) {
					return isSrcprefix(name);
				}
			};
			File[] list = file.listFiles(srcfilter);
			totalwork = list.length;
		}
		int perThreadProsize = 3;
		// 默认2个线程处理
		// if (totalwork <= threadNum) {
		// threadNum = totalwork;
		// perThreadProsize = totalwork / threadNum;
		// System.out.println("perThreadProsize===:" + perThreadProsize);
		// } else {
		// // 文件数据大于当前分配的线程处理数时 ;
		// if (totalwork % threadNum != 0) {
		// threadNum = threadNum + 1;
		// }else{
		// perThreadProsize = (totalwork / threadNum)+1;
		// }
		//
		// System.out.println("perThreadProsize===:" + perThreadProsize);
		// }

		int a = 0;
		for (; a < totalwork;) {
			File[] _filesList = new File[perThreadProsize];
			for (int perprosize = 0; perprosize < perThreadProsize; perprosize++) {
				_filesList[perprosize] = files[a + perprosize];

				if ((a + 1) + perprosize == totalwork) {
					lf.add(_filesList);
					break;
				} else {
					int _tp = perprosize + 1;
					if (_tp == perThreadProsize) {
						System.out.println(_tp + ":" + perThreadProsize);
						lf.add(_filesList);
						continue;
					}
				}

			}
			a += perThreadProsize;
		}
		System.out.println("SIZE:" + lf.size());
		for (File[] l : lf) {
			for (File _s : l) {
				if (_s != null) {
					System.out.println("==:" + _s.getName());
				} else {
					System.out.println("=空了=");
				}
			}
		}
	}
}
